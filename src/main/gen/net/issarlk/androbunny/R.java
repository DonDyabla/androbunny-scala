/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package net.issarlk.androbunny;

public final class R {
    public static final class array {
        public static final int rating_action_values=0x7f060001;
        public static final int rating_actions=0x7f060000;
        public static final int search_time_range=0x7f060002;
    }
    public static final class attr {
    }
    public static final class drawable {
        public static final int adult=0x7f020000;
        public static final int audio=0x7f020001;
        public static final int button_pause=0x7f020002;
        public static final int button_play=0x7f020003;
        public static final int charsheet=0x7f020004;
        public static final int comic=0x7f020005;
        public static final int digital=0x7f020006;
        public static final int digitalfalse=0x7f020007;
        public static final int general=0x7f020008;
        public static final int mature=0x7f020009;
        public static final int music=0x7f02000a;
        public static final int musicalbum=0x7f02000b;
        public static final int nofile=0x7f02000c;
        public static final int noicon=0x7f02000d;
        public static final int photography=0x7f02000e;
        public static final int picture=0x7f02000f;
        public static final int pictureseries=0x7f020010;
        public static final int portfolio=0x7f020011;
        public static final int print=0x7f020012;
        public static final int printfalse=0x7f020013;
        public static final int shockwave=0x7f020014;
        public static final int sketch=0x7f020015;
        public static final int toast_border=0x7f020016;
        public static final int transparent_background=0x7f02001a;
        public static final int video=0x7f020017;
        public static final int writing=0x7f020018;
        public static final int writing_icon=0x7f020019;
    }
    public static final class id {
        public static final int LinearLayout01=0x7f0a0008;
        public static final int ScrollView01=0x7f0a001b;
        public static final int about=0x7f0a0039;
        public static final int about_text=0x7f0a0000;
        public static final int apache_button=0x7f0a0002;
        public static final int author_icon=0x7f0a0021;
        public static final int button_play=0x7f0a001f;
        public static final int by_author=0x7f0a000a;
        public static final int continue_button=0x7f0a0011;
        public static final int description=0x7f0a0023;
        public static final int favorites_button=0x7f0a0034;
        public static final int files_gallery=0x7f0a0020;
        public static final int gallery_button=0x7f0a0032;
        public static final int gpl_button=0x7f0a0001;
        public static final int home_linearlayout=0x7f0a0004;
        public static final int image=0x7f0a002e;
        public static final int login=0x7f0a000d;
        public static final int login_as_guest=0x7f0a000e;
        public static final int login_continue_layout=0x7f0a000f;
        public static final int login_continue_text=0x7f0a0010;
        public static final int logout=0x7f0a003a;
        public static final int main_layout=0x7f0a0019;
        public static final int mediaplayer=0x7f0a001e;
        public static final int password=0x7f0a000c;
        public static final int root=0x7f0a0003;
        public static final int scraps_button=0x7f0a0033;
        public static final int search=0x7f0a0037;
        public static final int search_button=0x7f0a0018;
        public static final int search_descriptions=0x7f0a0016;
        public static final int search_keywords=0x7f0a0014;
        public static final int search_linearlayout=0x7f0a0012;
        public static final int search_text=0x7f0a0013;
        public static final int search_time_range=0x7f0a0017;
        public static final int search_titles=0x7f0a0015;
        public static final int section_desc=0x7f0a0006;
        public static final int section_name=0x7f0a0005;
        public static final int section_table=0x7f0a0007;
        public static final int settings=0x7f0a0038;
        public static final int submission_details=0x7f0a0025;
        public static final int submission_image_view=0x7f0a001d;
        public static final int submission_keywords=0x7f0a0024;
        public static final int submission_stats=0x7f0a0026;
        public static final int submission_view_on_ib=0x7f0a0027;
        public static final int submission_writing=0x7f0a001c;
        public static final int thumbnail_icon_digital=0x7f0a002b;
        public static final int thumbnail_icon_layout=0x7f0a0028;
        public static final int thumbnail_icon_print=0x7f0a002c;
        public static final int thumbnail_icon_rating=0x7f0a0029;
        public static final int thumbnail_icon_type=0x7f0a002a;
        public static final int thumbnail_image=0x7f0a0009;
        public static final int thumbnail_view=0x7f0a001a;
        public static final int title=0x7f0a0022;
        public static final int toast_layout_root=0x7f0a002d;
        public static final int toast_text=0x7f0a002f;
        public static final int user_linearlayout=0x7f0a0030;
        public static final int user_profile=0x7f0a0036;
        public static final int user_searches_linearlayout=0x7f0a0035;
        public static final int user_top_linearlayout=0x7f0a0031;
        public static final int username=0x7f0a000b;
    }
    public static final class layout {
        public static final int about=0x7f030000;
        public static final int file_thumbnail=0x7f030001;
        public static final int home=0x7f030002;
        public static final int home_search=0x7f030003;
        public static final int homethumbnail=0x7f030004;
        public static final int login=0x7f030005;
        public static final int main=0x7f030006;
        public static final int progress=0x7f030007;
        public static final int search=0x7f030008;
        public static final int searchresults=0x7f030009;
        public static final int submissionactivity=0x7f03000a;
        public static final int thumbnail=0x7f03000b;
        public static final int toast_layout=0x7f03000c;
        public static final int user=0x7f03000d;
    }
    public static final class menu {
        public static final int main_menu=0x7f090000;
    }
    public static final class raw {
        public static final int about=0x7f050000;
        public static final int apache2=0x7f050001;
        public static final int gpl=0x7f050002;
    }
    public static final class string {
        public static final int app_name=0x7f070000;
        public static final int by_author=0x7f070014;
        public static final int crash_dialog_ok_toast=0x7f070027;
        public static final int crash_dialog_text=0x7f070026;
        public static final int crash_dialog_title=0x7f070025;
        public static final int crash_notif_text=0x7f070024;
        /**  ACRA 
         */
        public static final int crash_notif_ticker_text=0x7f070022;
        public static final int crash_notif_title=0x7f070023;
        public static final int details=0x7f070016;
        public static final int favorites_button=0x7f07001a;
        /**  User 
         */
        public static final int gallery_button=0x7f070018;
        public static final int keywords=0x7f070015;
        /**  Login 
         */
        public static final int log_in=0x7f070001;
        public static final int login=0x7f070004;
        public static final int login_as_guest=0x7f070005;
        public static final int login_as_guest_button=0x7f070006;
        /**  Menu 
         */
        public static final int menu_about=0x7f07001b;
        public static final int menu_login=0x7f07001c;
        public static final int menu_logout=0x7f07001d;
        public static final int menu_search=0x7f07001e;
        public static final int menu_settings=0x7f07001f;
        public static final int music=0x7f07000a;
        public static final int password=0x7f070002;
        public static final int popular=0x7f070008;
        public static final int pref_acra_disabled=0x7f07002a;
        public static final int pref_acra_enabled=0x7f070029;
        public static final int pref_cache_size=0x7f07002b;
        public static final int pref_enable_acra=0x7f070028;
        public static final int recent=0x7f070009;
        public static final int scraps_button=0x7f070019;
        public static final int search_button=0x7f070012;
        public static final int search_descriptions=0x7f070010;
        public static final int search_in=0x7f07000d;
        public static final int search_keywords=0x7f07000e;
        /**  Search 
         */
        public static final int search_text=0x7f07000c;
        public static final int search_time_range=0x7f070011;
        public static final int search_titles=0x7f07000f;
        /**  Home 
         */
        public static final int see_more=0x7f070007;
        public static final int show_apache=0x7f070021;
        /**  About 
         */
        public static final int show_gpl=0x7f070020;
        public static final int stats=0x7f070017;
        /**  Upgrade 
         */
        public static final int upgrade_prompt=0x7f07002c;
        public static final int username=0x7f070003;
        /**  Submission 
         */
        public static final int view_on_inkbunny=0x7f070013;
        public static final int writing=0x7f07000b;
    }
    public static final class style {
        public static final int AboutLicenseButton=0x7f080026;
        /**  About 
         */
        public static final int AboutTextView=0x7f080025;
        public static final int Androbunny=0x7f080004;
        public static final int AndrobunnyHome=0x7f080003;
        public static final int HomeLinearLayout=0x7f080009;
        /**   Home 
         */
        public static final int HomeScrollView=0x7f080008;
        public static final int HomeSectionDescription=0x7f08000e;
        public static final int HomeSectionHeaderLinearLayout=0x7f08000c;
        public static final int HomeSectionLinearLayout=0x7f08000a;
        public static final int HomeSectionLinearLayout_Odd=0x7f08000b;
        public static final int HomeSectionName=0x7f08000d;
        public static final int HomeSeeMore=0x7f08000f;
        public static final int HomeThumbnailTable=0x7f080010;
        public static final int LoginButton=0x7f080007;
        public static final int LoginEditText=0x7f080006;
        /**  Login 
         */
        public static final int LoginText=0x7f080005;
        /**  Mediaplayer 
         */
        public static final int MediaplayerLayout=0x7f080027;
        public static final int MediaplayerPlay=0x7f080028;
        /**  Progress 
         */
        public static final int Progress=0x7f080029;
        public static final int RootLayout=0x7f080000;
        public static final int RootLinearLayout=0x7f080001;
        public static final int SearchButton=0x7f08002e;
        public static final int SearchCheckbox=0x7f08002f;
        public static final int SearchDaysCheckbox=0x7f080033;
        public static final int SearchDescriptionsCheckbox=0x7f080032;
        public static final int SearchKeywordsCheckbox=0x7f080030;
        public static final int SearchLabel=0x7f08002c;
        public static final int SearchLinearLayout=0x7f08002b;
        /**   Search 
         */
        public static final int SearchScrollView=0x7f08002a;
        public static final int SearchText=0x7f08002d;
        public static final int SearchTimeRangeSpinner=0x7f080034;
        public static final int SearchTitlesCheckbox=0x7f080031;
        public static final int SubmissionAuthorIcon=0x7f08001c;
        public static final int SubmissionDescription=0x7f08001e;
        public static final int SubmissionDetails=0x7f08001f;
        public static final int SubmissionDetailsHeader=0x7f080021;
        public static final int SubmissionDetailsHeaderRow=0x7f080020;
        public static final int SubmissionDetailsLayout=0x7f080022;
        public static final int SubmissionDetailsTextView=0x7f080023;
        public static final int SubmissionFilesGallery=0x7f08001a;
        public static final int SubmissionImageView=0x7f080019;
        public static final int SubmissionTitle=0x7f08001d;
        public static final int SubmissionTitleLayout=0x7f08001b;
        public static final int SubmissionViewOnIBButton=0x7f080024;
        /**   Submission Activity 
         */
        public static final int SubmissionWriting=0x7f080018;
        /**  Thumbnails 
         */
        public static final int ThumbnailGridView=0x7f080035;
        public static final int ThumbnailIcon=0x7f08003a;
        public static final int ThumbnailIconLayout=0x7f080039;
        public static final int ThumbnailImageView=0x7f080037;
        public static final int ThumbnailLinearLayout=0x7f080036;
        public static final int ThumbnailTextView=0x7f080038;
        public static final int TitleBar=0x7f080002;
        public static final int UserAuthorIcon=0x7f080015;
        public static final int UserButton=0x7f080016;
        public static final int UserLinearLayout=0x7f080012;
        public static final int UserProfile=0x7f080017;
        /**   User Activity 
         */
        public static final int UserScrollView=0x7f080011;
        public static final int UserSearchesLinearLayout=0x7f080014;
        public static final int UserTopLinearLayout=0x7f080013;
    }
    public static final class xml {
        public static final int preference=0x7f040000;
    }
    public static final class styleable {
        /** Attributes that can be used with a GalleryTheme.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #GalleryTheme_android_galleryItemBackground net.issarlk.androbunny:android_galleryItemBackground}</code></td><td></td></tr>
           </table>
           @see #GalleryTheme_android_galleryItemBackground
         */
        public static final int[] GalleryTheme = {
            0x0101004c
        };
        /**
          <p>This symbol is the offset where the {@link net.issarlk.androbunny.R.attr#android_galleryItemBackground}
          attribute's value can be found in the {@link #GalleryTheme} array.
          @attr name android:android_galleryItemBackground
        */
        public static final int GalleryTheme_android_galleryItemBackground = 0;
    };
}
