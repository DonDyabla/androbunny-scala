/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny;

import java.util.HashMap;

import net.issarlk.androbunny.R;
import net.issarlk.androbunny.inkbunny.Search;
import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

public class ThumbnailTable extends TableLayout implements View.OnClickListener {
	private ThumbnailAdapter adapter;
	private OnItemClickListener itemclickListener;
	private ThumbnailablesObserver observer;
	private final HashMap<View, Integer> view2Position = new HashMap<View, Integer>(4);;
	private OnSeeMoreListener seeMoreListener;
	private int nb_columns;
	private int nb_rows;
	
	public ThumbnailTable(Context context) {
		super(context);
		this.observer = new ThumbnailablesObserver();
	}
	
	public ThumbnailTable(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.observer = new ThumbnailablesObserver();
	}
	
	public void onClick(View v) {
		//Retrieve position
		Integer position = (Integer)this.view2Position.get(v);
		if (position != null && this.itemclickListener != null) {
			this.itemclickListener.onItemClick(this.adapter, v, position.intValue(), 0L);						
		}
	}
	
	public void setAdapter(ThumbnailAdapter argThumbnailAdapter) {
		if (this.adapter != null) {
			this.adapter.unregisterDataSetObserver(this.observer);
		}
		this.adapter = argThumbnailAdapter;
		this.adapter.registerDataSetObserver(this.observer);
		this.observer.onChanged();
	}
	
	public void cleanup() {
		if (this.adapter != null) {
			this.adapter.unregisterDataSetObserver(this.observer);
		}
	}
	
	public void setOnItemClickListener(OnItemClickListener argOnItemClickListener) {
		this.itemclickListener = argOnItemClickListener;
	}
	
	public void setOnSeeMoreListener(OnSeeMoreListener argOnSeeMoreListener) {
		this.seeMoreListener = argOnSeeMoreListener;
	}
	
	public interface OnItemClickListener {
		public abstract void onItemClick(BaseAdapter adapter, View view, int position, long id);
	}
	
	public interface OnSeeMoreListener {
		public abstract void onSeeMore(Search search);
	}
	
	public void fillRows() {
		//Cleanup old submissions
		this.view2Position.clear();    			
		//Display thumbnails			
		this.removeAllViews();
		int count = this.adapter.getCount();
		for (int r = 0 ; r < this.nb_rows ; r++) {
			if (r * this.nb_columns >= count) {
				break;
			}
			TableRow tr = new TableRow(this.getContext());
			for (int c = 0 ; c < this.nb_columns ; c++) {
				if (r * this.nb_columns + c >= count) {
					break;
				}
				if (r * this.nb_columns + c < this.nb_columns * this.nb_rows - 1) {
					View v = this.adapter.getView(r * this.nb_columns + c, null, null);
					if (v != null) {
						ImageView i = (ImageView)v.findViewById(R.id.thumbnail_image);
						i.setOnClickListener(this);    			 			
						tr.addView(v, new TableRow.LayoutParams());
						view2Position.put(i, new Integer(r * this.nb_columns + c));	
					}
				} else {
					//Last item, display a "see more" button
					Button seeMore = new Button(this.getContext());
					seeMore.setText("See\nmore");
					seeMore.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							OnSeeMoreListener tmp = ThumbnailTable.this.seeMoreListener;
							if (tmp != null) {
								tmp.onSeeMore(ThumbnailTable.this.adapter.search);
							}
						}		
					});
					tr.addView(seeMore);
				}
				
			}    				
			ThumbnailTable.this.addView(tr, new TableLayout.LayoutParams());
		}
	}
	
	class ThumbnailablesObserver extends DataSetObserver {	    	    	
		public void onChanged () {
    		super.onChanged();   
    		ThumbnailTable.this.fillRows();
    	}
    }

	public void setNumberOfColumns(int c) {
		this.nb_columns = c;		
	}

	public void setNumberOfRows(int r) {
		this.nb_rows = r;		
	}
}
