/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.acra.*;
import org.acra.annotation.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import net.issarlk.androbunny.inkbunny.API;
import net.issarlk.androbunny.inkbunny.InkbunnyAPIException;
import net.issarlk.androbunny.inkbunny.Rating;
import net.issarlk.androbunny.inkbunny.SubmissionType;
import net.issarlk.androbunny.inkbunny.Thumbnailable;
import net.issarlk.androbunny.utils.ABToast;
import net.issarlk.androbunny.utils.AbHttpClient;
import net.issarlk.androbunny.utils.BitmapCache;
import net.issarlk.androbunny.utils.CacheCleaner;
import net.issarlk.androbunny.utils.SimpleObservable;
import net.issarlk.androbunny.R;
import net.issarlk.androbunny.ThumbnailAdapter.ViewHolder;
import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.Toast;


@ReportsCrashes(formKey = "dEdsYXdwdk1mQjRCYi1aSmRxazBQeFE6MQ")
public final class AndrobunnyApp extends Application {
	public final BitmapCache bitmapCache = new BitmapCache();
	private final static String TAG = "InkbunnyApp";
	public final Handler handler = new Handler();
    //Its Worker
    public final ExecutorService worker = Executors.newFixedThreadPool(2);  
    public final ExecutorService urgentWorker = Executors.newCachedThreadPool();
	//Reference to UI Thread
	public Thread uiThread;	
	//Current adapter
	//public ThumbnailAdapter currentAdapter;
	//Layout inflater
	public LayoutInflater layoutInflater;	
	//Upgrade
	public boolean upgrade_available;
	public boolean refused_upgrade;
	public String upgrade_changes = null;
	
	/*
	 * 
	 * Initialisation
	 * 
	 */	
	public AndrobunnyApp() {
		super();

		//Put this object into the singleton
		AndrobunnyAppSingleton.androbunnyapp = this;
        ABIO.it().setHTTPClient(new AbHttpClient(this));
        //Start Androbunny actor
        Androbunny.it().start();
	}

	public void onCreate() {
		// Initialization of ACRA
        ACRA.init(this);
        this.uiThread = Thread.currentThread();
        //Get layout inflater
        layoutInflater = LayoutInflater.from(this);        
		super.onCreate();
		//Get density
        Androbunny.it().setScale(this.getResources().getDisplayMetrics().density);
		Log.d(TAG, "Starting Inkbunny APP");
		//Load default values from the xml into preferences.
		PreferenceManager.setDefaultValues(this, R.xml.preference, false);
		//Get preference manager for further use.
        Preference.it().setSharedPreferences(PreferenceManager.getDefaultSharedPreferences(this));

		//Set SID in API if saved in preferences
		if (Preference.it().has("sid")) {
			API.it().getSID().set(Preference.it().getString("sid"));
		}
		if (Preference.it().has("ratingsmask")) {
            API.it().setRatingsmask(Preference.it().getString("ratingsmask"));
		} else if (API.it().getSID().get() != null) {
			//The user is logged in, but we don't have her ratingsmask.
			//She needs to log in again... log her out
            API.it().logout();
		}
		//Check for update
		this.worker.execute(new Updater());
		//Start cache cleaner thread
		new CacheCleaner().run();
		//Check in the background whether we are logged in or not.
		final String username = Preference.it().getString("username");
		worker.execute(new Runnable() {
            @Override
            public void run() {
                ABUser.it().checkLoggedInAs(username);
            }
        });
	}


	/*
	 * 
	 *  Icons
	 * 
	 */
	
	public int getDefaultResourceForSubmissionType(SubmissionType st) {
		final int[] ressources = {
			R.drawable.nofile, //Picture pinup
			R.drawable.nofile, //Sketch
			R.drawable.nofile, //Picture series
			R.drawable.nofile, //Comic
			R.drawable.nofile, //Portfolio
			R.drawable.shockwave, //Flash animation
			R.drawable.shockwave, //Interactive Flash
			R.drawable.nofile, //Video
			R.drawable.nofile, //Video animation/3D/CGI
			R.drawable.audio, //Music - single track
			R.drawable.audio, //Music - album
			R.drawable.writing, //Writing
			R.drawable.nofile, //Character sheet
			R.drawable.nofile, //Photography - Fursuit/Sculpture/Jewelry/etc
		};
		return(ressources[(int)st.id - 1]);
	}
	
	public int getResourceForRating(Rating r) {
		return this.getResourceForRating(r.id);
	}
	
	public int getResourceForRating(long id) {
		final int[] ressources = {
			R.drawable.general, //General
			R.drawable.mature, //Mature
			R.drawable.adult //Adult
		};
		return(ressources[(int)id]);
	}
	
	public int getIconResourceForSubmissionType(SubmissionType st) {
		final int[] ressources = {
				R.drawable.picture, //Picture pinup
				R.drawable.sketch, //Sketch
				R.drawable.pictureseries, //Picture series
				R.drawable.comic, //Comic
				R.drawable.portfolio, //Portfolio
				R.drawable.video, //Flash animation
				R.drawable.video, //Interactive Flash
				R.drawable.video, //Video
				R.drawable.video, //Video animation/3D/CGI
				R.drawable.music, //Music - single track
				R.drawable.musicalbum, //Music - album
				R.drawable.writing_icon, //Writing
				R.drawable.charsheet, //Character sheet
				R.drawable.photography, //Photography - Fursuit/Sculpture/Jewelry/etc
			};
			return(ressources[(int)st.id - 1]);
	}
	
	public int getIconResourceForDigitalSales(int s) {
		return (s==0)?R.drawable.digitalfalse:R.drawable.digital;
	}
	
	public int getIconResourceForPrintSales(int s) {
		return (s==0)?R.drawable.printfalse:R.drawable.print;
	}

}
