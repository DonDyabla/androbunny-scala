package net.issarlk.androbunny

import inkbunny.Thumbnailable
import java.util.Observable
import android.graphics.Bitmap

/**
 * Created by IntelliJ IDEA.
 * User: issarlk
 * Date: 12/25/11
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */

object Rating extends Observable {

  def getBitmapFilterFor(thumbnailable: Thumbnailable): BitmapFilter = {
    var rating: Int = thumbnailable.getRating.id.asInstanceOf[Int]
    assert((rating >= 0 && rating <= 2))
    var action: Int = Preference.it.getRating(rating)
    action match {
      case 1 =>
        return HideBitmapFilter
      case 2 =>
        return MosaicBitmapFilter
      case 3 =>
        return UnitBitmapFilter
    }
    return null
  }
}

abstract trait BitmapFilter {
  def filter(in: Bitmap): Bitmap
}

object HideBitmapFilter extends BitmapFilter {
  def filter(in: Bitmap): Bitmap = {
    return null
  }
}

object UnitBitmapFilter extends BitmapFilter {
  def filter(in: Bitmap): Bitmap = {
    return in
  }
}

object MosaicBitmapFilter extends BitmapFilter {
  var squares = 10

  def filter(in: Bitmap): Bitmap = {
    val w: Int = in.getWidth
    val h: Int = in.getHeight
    if (squares <= 0) {
      squares = 10
    }
    var w2: Int = this.squares
    if (w2 <= 0) w2 = 1
    var h2: Int = this.squares
    if (h2 <= 0) h2 = 1
    var small: Bitmap = Bitmap.createScaledBitmap(in, w2, h2, false)
    in.recycle
    var result: Bitmap = Bitmap.createScaledBitmap(small, w, h, false)
    small.recycle
    return result
  }
}