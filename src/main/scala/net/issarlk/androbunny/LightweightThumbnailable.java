/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/

package net.issarlk.androbunny;

import android.os.Parcel;
import android.os.Parcelable;
import net.issarlk.androbunny.inkbunny.Rating;
import net.issarlk.androbunny.inkbunny.SubmissionType;
import net.issarlk.androbunny.inkbunny.Thumbnail;
import net.issarlk.androbunny.inkbunny.Thumbnailable;

public final class LightweightThumbnailable implements Thumbnailable, Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6147583928723632039L;
	private final Thumbnail original_thumbnail;
	private final Thumbnail custom_thumbnail;
	private final String author;
	private final SubmissionType type;
	private final Rating rating;
	private final int digitalSales;
	private final int printSales;
	private final int id;
	
	public LightweightThumbnailable(Thumbnailable argThumbnailable, int w, int h) {
		this.original_thumbnail = argThumbnailable.getLargerThumbnail(Thumbnailable.ORIGINAL, w, h);
		this.custom_thumbnail = argThumbnailable.getLargerThumbnail(Thumbnailable.CUSTOM, w, h);
		this.author = argThumbnailable.getAuthor();
		this.type = argThumbnailable.getSubmissionType();
		this.rating = argThumbnailable.getRating();
		this.digitalSales = argThumbnailable.getDigitalSales();
		this.printSales = argThumbnailable.getPrintSales();
		this.id = argThumbnailable.getID();
	}
	
	public Thumbnail getLargerThumbnail(int type, int w, int h) {
		return (type == Thumbnailable.ORIGINAL)?this.original_thumbnail:this.custom_thumbnail;
	}

	public String getAuthor() {
		return this.author;
	}

	public SubmissionType getSubmissionType() {
		return this.type;
	}

	public Rating getRating() {
		return this.rating;
	}

	public int getDigitalSales() {
		return this.digitalSales;
	}

	public int getPrintSales() {
		return this.printSales;
	}

	public int getID() {
		return this.id;
	}

	
	//Parcellable
	public LightweightThumbnailable(Parcel in) {		
		this.original_thumbnail = Thumbnail.CREATOR.createFromParcel(in);
		this.custom_thumbnail = Thumbnail.CREATOR.createFromParcel(in);
		this.author = in.readString();
		this.type = SubmissionType.CREATOR.createFromParcel(in);
		this.rating = Rating.CREATOR.createFromParcel(in);
		this.digitalSales = in.readInt();
		this.printSales = in.readInt();
		this.id = in.readInt();
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		this.original_thumbnail.writeToParcel(out, 0);
		this.custom_thumbnail.writeToParcel(out, 0);
		out.writeString(this.author);
		this.type.writeToParcel(out, 0);
		this.rating.writeToParcel(out, 0);
		out.writeInt(this.digitalSales);
		out.writeInt(this.printSales);
		out.writeInt(this.id);
	}
	
	public static final Parcelable.Creator<LightweightThumbnailable> CREATOR = 
		new Parcelable.Creator<LightweightThumbnailable>() {
			public LightweightThumbnailable createFromParcel(Parcel in) {
				return new LightweightThumbnailable(in);
			}
			
			public LightweightThumbnailable[] newArray(int size) {
				return new LightweightThumbnailable[size];
			}
	};
}
