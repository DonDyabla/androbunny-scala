package net.issarlk.androbunny

/**
 * Created by IntelliJ IDEA.
 * User: issarlk
 * Date: 12/12/11
 * Time: 1:34 PM
 * To change this template use File | Settings | File Templates.
 */

import java.util.regex.{Pattern, Matcher}

final object ABTextProcessor {
  private final val TAG = "ABTextProcessor"
  private final val REMOVE_STARTDIVS: Pattern = Pattern.compile("<div [^>]*>", Pattern.CASE_INSENSITIVE)
  private final val REMOVE_ENDDIVS: Pattern = Pattern.compile("</div>", Pattern.CASE_INSENSITIVE)
  private final val REMOVE_WS: Pattern = Pattern.compile("\n|\r|\t", Pattern.CASE_INSENSITIVE)
  private final val USER_LINK: Pattern = Pattern.compile("['\"](https://inkbunny.net/)?([0-9a-z]+)['\"]", Pattern.CASE_INSENSITIVE)
  private final val SUBMISSION_LINK: Pattern = Pattern.compile("['\"](https://inkbunny.net/)?submissionview\\.php\\?id=([0-9]+)['\"]", Pattern.CASE_INSENSITIVE)
  private final val SUBMISSION_ID: Pattern = Pattern.compile("net.issarlk.androbunny.submission://([0-9]+)", Pattern.CASE_INSENSITIVE)
  private final val POOL_LINK: Pattern = Pattern.compile("['\"](https://inkbunny.net/)?poolview_process\\.php\\?pool_id=([0-9]+)['\"]", Pattern.CASE_INSENSITIVE)

  def process(text: String):String = {
    var matches: Matcher = REMOVE_STARTDIVS.matcher(text)
    var tmp: String = matches.replaceAll("<ignore>")
    matches = REMOVE_ENDDIVS.matcher(tmp)
    tmp = matches.replaceAll("</ignore>")
    matches = REMOVE_WS.matcher(tmp)
    tmp = matches.replaceAll("")
    Log.d(TAG, "Description: " + tmp)
    matches = USER_LINK.matcher(tmp)
    tmp = matches.replaceAll("'net.issarlk.androbunny.user://$2'")
    matches = SUBMISSION_LINK.matcher(tmp)
    tmp = matches.replaceAll("'net.issarlk.androbunny.submission://$2'")
    matches = POOL_LINK.matcher(tmp)
    tmp = matches.replaceAll("'net.issarlk.androbunny.pool://$2'")
    return tmp
  }
}