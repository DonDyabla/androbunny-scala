package net.issarlk.androbunny

import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.SharedPreferences
import utils.SimpleObservable
import scala.collection.mutable.MutableList

/**
 * Created by IntelliJ IDEA.
 * User: issarlk
 * Date: 12/18/11
 * Time: 8:21 PM
 * To change this template use File | Settings | File Templates.
 */

object Preference extends OnSharedPreferenceChangeListener {
  private val TAG = "Preference"
  var sharedPreferences: SharedPreferences = null
  def setSharedPreferences(arg: SharedPreferences):Unit = {
    sharedPreferences = arg
    //Receive notifications when preferences change
    this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    //Read rating actions
    readRatingAction(0, sharedPreferences.getString("general", "0"));
    readRatingAction(1, sharedPreferences.getString("mature", "0"));
    readRatingAction(2, sharedPreferences.getString("adult", "0"));
    //Read mosaic size
    mosaic_squares = Integer.parseInt(sharedPreferences.getString("mosaic_squares", "32"))
  }
  //Observable to update interest objects when the ratings change.
  val ratings: SimpleObservable[Array[Int]] = new SimpleObservable[Array[Int]](Array(0, 0, 0))
  def getRating(index: Int): Int = ratings.get()(index)
  val RATING_HIDE = 1;
  val RATING_MOSAIC = 2;
  val RATING_SHOW = 3;
  val customThumbnails: SimpleObservable[Boolean] = new SimpleObservable[Boolean](true)
  def useCustomThumbnails: Boolean = customThumbnails.get()
  var mosaic_squares = 32

  def it = this

  def onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String): Unit = {
    try {
      if ("general" == key) {
        readRatingAction(0, sharedPreferences.getString(key, "0"))
      }
      else if ("mature" == key) {
        readRatingAction(1, sharedPreferences.getString(key, "0"))
      }
      else if ("adult" == key) {
        readRatingAction(2, sharedPreferences.getString(key, "0"))
      }
      else if ("mosaic_squares" == key) {
        MosaicBitmapFilter.squares = Integer.parseInt(sharedPreferences.getString("mosaic_squares", "32"))
      }
      else if ("show_custom" == key) {
        this.customThumbnails.set(sharedPreferences.getBoolean("show_custom", true))
      }
    }
    catch {
      case e: Exception => {
        Log.e(TAG, e.toString)
      }
    }
  }  

  private def getEditor = sharedPreferences.edit
  
  def has(key: String): Boolean = {
    return sharedPreferences.contains(key)
  }

  def getBoolean(key: String): Boolean = {
    return sharedPreferences.getBoolean(key, false)
  }

  def getString(key: String): String = {
    return sharedPreferences.getString(key, "")
  }
  
  def setString(key: String, value: String): Unit = {
    val editor = getEditor
    editor.putString(key, value)
    editor.commit()
  }

  def readRatingAction(rating:Int, action:String):Boolean = {
    val oldaction = getRating(rating)
    val newaction = action.toInt
    if (oldaction != newaction) {
      ratings.get().update(rating, newaction);
      ratings.changed();
    }
    return ((oldaction == RATING_HIDE && newaction != RATING_HIDE)
      ||(newaction == RATING_HIDE && oldaction != RATING_HIDE));
  }
}