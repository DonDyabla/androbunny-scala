/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/

package net.issarlk.androbunny.inkbunny;

public interface Thumbnailable {
	public static final int ORIGINAL = 1;
	public static final int CUSTOM = 2;
	public static final int FULL = 0;
	public static final int SCREEN = 1;
	public static final int PREVIEW = 2;
	abstract public Thumbnail getLargerThumbnail(int type, int w, int h);
	abstract public String getAuthor();
	abstract public SubmissionType getSubmissionType();
	abstract public Rating getRating();
	abstract public int getDigitalSales();
	abstract public int getPrintSales();
	abstract public int getID();
}
