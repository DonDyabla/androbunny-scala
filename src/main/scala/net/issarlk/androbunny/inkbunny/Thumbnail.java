package net.issarlk.androbunny.inkbunny;

import java.net.URI;
import java.net.URISyntaxException;
import net.issarlk.androbunny.Log;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public final class Thumbnail implements Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1493891514534887546L;
	//private static final String TAG = "Thumbnail";
	public static final int LARGE = 0;
	public static final int MEDIUM = 1;
	public static final int SMALL = 2;
	public static final int LARGE_NONCUSTOM = 3;
	public static final int MEDIUM_NONCUSTOM = 4;
	public static final int SMALL_NONCUSTOM = 5;
	private static final String TAG = "Thumbnail";
	private static final String[] type_names = {"large",
	                                      "medium",
	                                      "small",
	                                      "large_noncustom",
	                                      "medium_noncustom",
	                                      "small_noncustom"};
	public final int type;
	public final URI url;
	public final int x;
	public final int y;
	
	public Thumbnail(int argtype, URI argurl, int argx, int argy) {
		this.type = argtype;
		this.url = argurl;
		this.x = argx;
		this.y = argy;
	}
	
	public Thumbnail(int argtype, JSONObject json) throws URISyntaxException, JSONException {
		this(
				argtype,
				new URI(json.getString("thumbnail_url_" + type_names[argtype])),
				json.getInt("thumb_" + type_names[argtype] + "_x"),
				json.getInt("thumb_" + type_names[argtype] + "_y")
				);
	}
	
	public static Thumbnail[] extractFrom(JSONObject json) throws URISyntaxException, JSONException {
		Thumbnail[] result = new Thumbnail[6];
		int i;
		for (i =  type_names.length - 1 ; i >= 0 ; i--) {
		try {
				if (json.has("thumbnail_url_" + type_names[i])) {
					result[i] = new Thumbnail(i, json);
				}
			} catch (JSONException e) {
				//Log.e("Thumbnail", "Mauvais json: " + json.toString() + "\n" + e.toString());
			}
		}
		return result;
	}
	
	public boolean larger(int w, int h) {
		return this.x >= w && this.y >= h;	
	}
	
	public boolean larger(Thumbnail other) {
		if (other == null) {
			return true;
		} else {
			return this.x * this.y >= other.x * other.y;	
		}
	}
	
	//Parcellable
	public Thumbnail(Parcel in) {		
		this.type = in.readInt();
		URI tmpURI = null;
		String tmp = in.readString();
		if (tmp != null) {
			try {
				tmpURI = new URI(tmp);
			} catch (URISyntaxException e) {
				Log.e(TAG, e.toString());
			}			
		}
		this.url = tmpURI;
		this.x = in.readInt();
		this.y = in.readInt();
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.type);
		out.writeString(this.url.toString());
		out.writeInt(this.x);
		out.writeInt(this.y);
	}
	
	public static final Parcelable.Creator<Thumbnail> CREATOR = 
		new Parcelable.Creator<Thumbnail>() {
			public Thumbnail createFromParcel(Parcel in) {
				return new Thumbnail(in);
			}
			
			public Thumbnail[] newArray(int size) {
				return new Thumbnail[size];
			}
	};
}
