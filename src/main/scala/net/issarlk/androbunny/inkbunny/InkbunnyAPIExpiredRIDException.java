package net.issarlk.androbunny.inkbunny;

public class InkbunnyAPIExpiredRIDException extends InkbunnyAPIException {

	public InkbunnyAPIExpiredRIDException(String s) {
		super(s);
	}

}
