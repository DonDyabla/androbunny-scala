package net.issarlk.androbunny.inkbunny;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class Rating implements Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -509514011772354560L;
	private static final HashMap<Integer, Rating> ratings = new HashMap<Integer, Rating>();
	public int id;
	public String name;
	
	public static Rating make(int argid, String argname) {		
		Integer key = new Integer(argid);
		Rating result = ratings.get(key);
		if (result == null) {
			//Rating not found, create it
			result = new Rating(argid, argname);
			ratings.put(key, result);
		}
		return result;
	}
	
	private Rating(int argid, String argname) {
		id = argid;
		name = argname;
	}
	
	//Parcellable
	public static Rating make(Parcel in) {		
		int id = in.readInt();
		String name = in.readString();
		Integer key = new Integer(id);
		Rating result = ratings.get(key);
		if (result == null) {
			//Rating not found, create it
			result = new Rating(id, name);
			ratings.put(key, result);
		}
		return result;
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.id);
		out.writeString(this.name);
	}
	
	public static final Parcelable.Creator<Rating> CREATOR = 
		new Parcelable.Creator<Rating>() {
			public Rating createFromParcel(Parcel in) {
				return Rating.make(in);
			}
			
			public Rating[] newArray(int size) {
				return new Rating[size];
			}
	};
}
