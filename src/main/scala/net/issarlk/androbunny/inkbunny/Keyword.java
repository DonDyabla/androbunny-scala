package net.issarlk.androbunny.inkbunny;

import java.net.URISyntaxException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class Keyword implements Parcelable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2061822838411170389L;
	public int id;
	public String name;
	public int submissions_count;
	
	public Keyword(Submission argsubmission, JSONObject json) throws JSONException, URISyntaxException {
		this.id = json.getInt("keyword_id");
		this.name = json.getString("keyword_name");
		this.submissions_count = json.getInt("submissions_count");
	}
	
	public static Keyword[] readArray(Submission argsubmission, JSONArray array) throws JSONException, URISyntaxException {
		Keyword[] result;
		result = new Keyword[array.length()];
		for (int i =  array.length() - 1 ; i >= 0 ; i-- ) {
			result[i] = new Keyword(argsubmission, array.getJSONObject(i));
		}
		return result;
	}
	
	//Parcellable
	public Keyword(Parcel in) {
		this.id = in.readInt();
		this.name = in.readString();
		this.submissions_count = in.readInt();
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.id);
		out.writeString(this.name);
		out.writeInt(this.submissions_count);
	}
	
	public static final Parcelable.Creator<Keyword> CREATOR = 
		new Parcelable.Creator<Keyword>() {
			public Keyword createFromParcel(Parcel in) {
				return new Keyword(in);
			}
			
			public Keyword[] newArray(int size) {
				return new Keyword[size];
			}
	};
}
