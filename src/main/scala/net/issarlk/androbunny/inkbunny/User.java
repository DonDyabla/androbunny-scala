/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.inkbunny;


import java.net.URI;
import java.net.URISyntaxException;

import net.issarlk.androbunny.Androbunny;
import net.issarlk.androbunny.AndrobunnyAppSingleton;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7026149452891028517L;
	@SuppressWarnings("unused")
	private static final String TAG = "User";
	public static final int CONTACT_AIM = 0;
	public static final int CONTACT_DA = 1;
	public static final int CONTACT_EMAIL = 2;
	public static final int CONTACT_FB = 3;
	public static final int CONTACT_FLAYRAH = 4;
	public static final int CONTACT_FLIST = 5;
	public static final int CONTACT_FA = 6;
	public static final int CONTACT_GOOGLETALK = 7;
	public static final int CONTACT_ICQ = 8;
	public static final int CONTACT_LJ = 9;
	public static final int CONTACT_MSN = 10;
	public static final int CONTACT_OTHER = 11;
	public static final int CONTACT_PS = 12;
	public static final int CONTACT_SL = 13;
	public static final int CONTACT_SHEEZYART = 14;
	public static final int CONTACT_SKYPE = 15;
	public static final int CONTACT_SOFURRY = 16;
	public static final int CONTACT_TWITTER = 17;
	public static final int CONTACT_WEB = 18;
	public static final int CONTACT_WIKIFUR = 19;
	public static final int CONTACT_XBOX = 20;
	public static final int CONTACT_YAHOO = 21;
	public static final int CONTACT_YOUTUBE = 22;
	public int id;
	public String name;
	public String icon_file_name;
	public Icon[] icons;
	public final String[] contacts = new String[CONTACT_YOUTUBE + 1];
	public String profile;
	
	
	public User(JSONObject json) throws URISyntaxException, JSONException {
		id = json.getInt("user_id");
		name = json.getString("username");
		icons = new Icon[3];
		if (! json.isNull("user_icon_file_name")) {
			//Full info
			icon_file_name = json.getString("user_icon_file_name");
			icons[0] = new Icon(Icon.LARGE, new URI(json.getString("user_icon_url_large")));
			icons[1] = new Icon(Icon.MEDIUM, new URI(json.getString("user_icon_url_medium")));
			icons[2] = new Icon(Icon.SMALL, new URI(json.getString("user_icon_url_small")));
		}
	}
	
	public User(int argId, String argName) {
		id = argId;
		name = argName;
		icons = new Icon[3];
	}
	
	public Icon getLargestIcon() {
		int i;
		if (this.icons == null || this.icons.length < 1) {
			return null;
		}
		for (i = this.icons.length - 1 ; i >= 0 ; i--) {
			if (this.icons[i] != null && this.icons[i].url != null) {
				return (this.icons[i]);
			}
		}
		return null;
	}
	
	//Parcellable
	public User(Parcel in) {		
		this.id = in.readInt();
		this.name = in.readString();
		if (in.readInt() == 1) {
			//Full info
			icon_file_name = in.readString();
			icons = new Icon[3];			
			icons[0] = Icon.CREATOR.createFromParcel(in);
			icons[1] = Icon.CREATOR.createFromParcel(in);
			icons[2] = Icon.CREATOR.createFromParcel(in);				
		}
		if (in.readInt() == 1) {
			//Profile present
			this.profile = in.readString();
		}
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.id);
		out.writeString(this.name);
		if (this.icon_file_name != null) {
			out.writeInt(1);
			out.writeString(this.icon_file_name);
			this.icons[0].writeToParcel(out, 0);
			this.icons[1].writeToParcel(out, 0);
			this.icons[2].writeToParcel(out, 0);
		} else {
			out.writeInt(0);
		}
		if (this.profile != null) {
			out.writeInt(1);
			out.writeString(this.profile);
		} else {
			out.writeInt(0);
		}
	}
	
	public static final Parcelable.Creator<User> CREATOR = 
		new Parcelable.Creator<User>() {
			public User createFromParcel(Parcel in) {
				return new User(in);
			}
			
			public User[] newArray(int size) {
				return new User[size];
			}
	};

	public Search getGallery() throws InkbunnyAPIException {
		return API.it().getUserGallery(this);
	}
	
	public Search getScraps() throws InkbunnyAPIException {
		return API.it().getUserScraps(this);
	}
	
	public Search getFavorites() throws InkbunnyAPIException {
		return API.it().getUserFavorites(this);
	}
	
	public String getProfile() throws InkbunnyAPIException {
		if (this.profile != null) {
			return this.profile;
		}
		try {
            API.it().scrapeUserPage(this);
			return this.profile;
		} catch (Exception e) {
			throw new InkbunnyAPIException(e);
		}
	}
}
