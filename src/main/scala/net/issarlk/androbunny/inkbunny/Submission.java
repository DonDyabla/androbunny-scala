/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/

package net.issarlk.androbunny.inkbunny;
import java.net.URI;
import java.net.URISyntaxException;
import net.issarlk.androbunny.Log;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class Submission implements Parcelable, Thumbnailable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1641464884399255676L;
	private static final String TAG = "Submission";
	public static final int FULL = 0;
	public static final int SCREEN = 1;
	public static final int PREVIEW = 2;
	public int id;
	public int hidden;
	public User user;
	public String create_datetime;
	public String create_datetime_usertime;
	public String file_name;
	public Thumbnail[] thumbnails;
	public URI[] file_urls;
	public String title;
	public int deleted;
	public int s_public;
	public String mimetype;
	public Rating rating;
	public SubmissionType submission_type;
	public int digitalsales;
	public int printsales;
	public int guest_block;
	public int scraps;
	public int favorite;
	public int favorites_count;
	public int comments_count;;
	public File[] files;
	public String description;
	public String writing;
	public Keyword[] keywords;
	
	public int getID() {
		return this.id;
	}
	
	public String getAuthor() {
		return this.user.name;
	}
	
	public SubmissionType getSubmissionType() {
		return(this.submission_type);
    }

	public Rating getRating() {
		return(this.rating);
	}
	
	public int getDigitalSales() {
		return(this.digitalsales);
	}
	
	public int getPrintSales() {
		return(this.printsales);
	}	
	public Submission(JSONObject json) {
		try {
			this.id = json.getInt("submission_id");
			this.hidden = API.getBool(json, "hidden");
			this.user = new User(json);
			this.create_datetime = json.getString("create_datetime");
			this.create_datetime_usertime = json.getString("create_datetime_usertime");
			this.file_name = json.getString("file_name");
			this.thumbnails = Thumbnail.extractFrom(json);
			this.title = json.getString("title");
			this.deleted = API.getBool(json, "deleted");
			this.s_public = API.getBool(json, "public");
			this.mimetype = json.getString("mimetype");
			this.rating = Rating.make(json.getInt("rating_id"), json.getString("rating_name"));
			this.submission_type = new SubmissionType(json.getInt("submission_type_id"), json.getString("type_name"));
			this.digitalsales = API.getBool(json, "digitalsales");
			this.printsales = API.getBool(json, "printsales");
			this.guest_block = API.getBool(json, "guest_block");
			
			// Full info
			if (json.has("favorite")) {
				this.keywords = Keyword.readArray(this, json.getJSONArray("keywords"));
				this.scraps = API.getBool(json, "scraps");
				this.favorite = API.getBool(json, "favorite");
				this.favorites_count = json.getInt("favorites_count");
				this.comments_count = json.getInt("comments_count");
				this.files = File.readArray(this, json.getJSONArray("files"));
				this.file_urls = new URI[3];
				this.file_urls[FULL] = new URI(json.getString("file_url_full"));
				this.file_urls[SCREEN] = new URI(json.getString("file_url_screen"));
				this.file_urls[PREVIEW] = new URI(json.getString("file_url_preview"));
			}
			
			//Description
			if (! json.isNull("description_bbcode_parsed")) {
				this.description = json.getString("description_bbcode_parsed");					
			}
			
			//Writing
			if (! json.isNull("writing_bbcode_parsed")) {
				this.writing = json.getString("writing_bbcode_parsed");					
			}
				
		} catch (JSONException e) {
			Log.e(TAG, e.toString());
		} catch (URISyntaxException e) {
			Log.e(TAG, e.toString());
		}
	}
	
	public Thumbnail getLargerThumbnail(int type, int w, int h) {
		if (this.thumbnails == null || this.thumbnails.length < 1) {
			return null;
		}
		int i, start, end;
		if (type == Thumbnailable.CUSTOM) {
			start = 0;
			end = 2;				
		} else {
			start = 3;
			end = 5;				
		}
		Thumbnail result = this.thumbnails[start];
		for (i = end ; i >= start ; i--) {
			if (result == null || !this.thumbnails[i].larger(result) && this.thumbnails[i].larger(w, h)) {
				result = this.thumbnails[i];
			}
		}
		if (result != null && result.larger(w, h)) {
			//Found one
			return result;
		} else {
			//None are large enough... return the biggest
			return this.getLargestThumbnail(type);
		}			
	}
	public Thumbnail getLargestThumbnail(int type) {
		if (this.thumbnails == null || this.thumbnails.length < 1) {
			return null;
		}
		int i, start, end;
		if (type == Thumbnailable.CUSTOM) {
			start = 0;
			end = 2;				
		} else {
			start = 3;
			end = 5;				
		}
		Thumbnail result = this.thumbnails[start];
		for (i = end ; i >= start ; i--) {
			if (result == null || this.thumbnails[i] != null && this.thumbnails[i].larger(result)) {
				result = this.thumbnails[i];
			}
		}
		return result;
	}
	
	//Parcellable
	public Submission(Parcel in) {		
		if (in.readLong() != Submission.serialVersionUID) {
			throw new RuntimeException("Wrong UID");
		}
		this.id = in.readInt();
		this.hidden = in.readInt();
		this.user = User.CREATOR.createFromParcel(in);
		this.create_datetime = in.readString();
		this.create_datetime_usertime = in.readString();
		this.file_name = in.readString();
		this.thumbnails = new Thumbnail[in.readInt()];
		for (int i = this.thumbnails.length - 1 ; i >= 0 ; i--) {
			if (in.readInt() == 1) {
				this.thumbnails[i] = Thumbnail.CREATOR.createFromParcel(in);
			}
		}		
		this.title = in.readString();
		this.deleted = in.readInt();
		this.s_public = in.readInt();
		this.mimetype = in.readString();
		this.rating = Rating.CREATOR.createFromParcel(in);
		this.submission_type = SubmissionType.CREATOR.createFromParcel(in);
		this.digitalsales = in.readInt();
		this.printsales = in.readInt();
		this.guest_block = in.readInt();
		
		// Full info
		if (in.readInt() == 1) {
			this.keywords = new Keyword[in.readInt()];
			for (int i = this.keywords.length - 1 ; i >= 0 ; i--) {
				this.keywords[i] = Keyword.CREATOR.createFromParcel(in);
			}
			this.scraps = in.readInt();
			this.favorite = in.readInt();
			this.favorites_count = in.readInt();
			this.comments_count = in.readInt();
			this.files = new File[in.readInt()];
			for (int i = this.files.length - 1 ; i >= 0 ; i--) {
				this.files[i] = File.CREATOR.createFromParcel(in);
			}			
			this.file_urls = new URI[3];
			for (int i = 0 ; i < 3 ; i++) {
				String tmp = in.readString();
				if (tmp != null) {
					try {
						this.file_urls[FULL] = new URI(tmp);					
					} catch (URISyntaxException e) {
						Log.e(TAG, e.toString());
					}		
				}
			}		
		}
		
		//Description		
		if (in.readInt() == 1) {
			this.description = in.readString();					
		}
		
		//Writing
		if (in.readInt() == 1) {
			this.writing = in.readString();					
		}
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeLong(Submission.serialVersionUID);
		out.writeInt(this.id);
		out.writeInt(this.hidden);
		this.user.writeToParcel(out, 0);
		out.writeString(this.create_datetime);
		out.writeString(this.create_datetime_usertime);
		out.writeString(this.file_name);
		out.writeInt(this.thumbnails.length);
		for (int i = this.thumbnails.length - 1 ; i >= 0 ; i--) {
			if (this.thumbnails[i] != null) {
				out.writeInt(1);
				this.thumbnails[i].writeToParcel(out, 0);
			} else {
				out.writeInt(0);
			}
		}
		out.writeString(this.title);
		out.writeInt(this.deleted);
		out.writeInt(this.s_public);
		out.writeString(this.mimetype);
		this.rating.writeToParcel(out, 0);
		this.submission_type.writeToParcel(out, 0);
		out.writeInt(this.digitalsales);
		out.writeInt(this.printsales);
		out.writeInt(this.guest_block);
		
		// Full info
		if (this.keywords != null) {
			out.writeInt(1);
			out.writeInt(this.keywords.length);
			for (int i = this.keywords.length - 1 ; i >= 0 ; i--) {
				this.keywords[i].writeToParcel(out, 0);
			}
			out.writeInt(this.scraps);
			out.writeInt(this.favorite);
			out.writeInt(this.favorites_count);
			out.writeInt(this.comments_count);
			out.writeInt(this.files.length);
			for (int i = this.files.length - 1 ; i >= 0 ; i--) {
				this.files[i].writeToParcel(out, 0);
			}		
			out.writeString(this.file_urls[FULL].toString());
			out.writeString(this.file_urls[SCREEN].toString());
			out.writeString(this.file_urls[PREVIEW].toString());
		} else {
			out.writeInt(0);
		}
		
		//Description		
		if (this.description != null) {
			out.writeInt(1);
			out.writeString(this.description);					
		} else {
			out.writeInt(0);
		}
		
		//Writing
		if (this.writing != null) {
			out.writeInt(1);
			out.writeString(this.writing);					
		} else {
			out.writeInt(0);
		}
	}
	
	public static final Parcelable.Creator<Submission> CREATOR = 
		new Parcelable.Creator<Submission>() {
			public Submission createFromParcel(Parcel in) {
				return new Submission(in);
			}
			
			public Submission[] newArray(int size) {
				return new Submission[size];
			}
	};
}
