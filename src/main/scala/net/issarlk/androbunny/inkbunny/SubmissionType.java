package net.issarlk.androbunny.inkbunny;

import android.os.Parcel;
import android.os.Parcelable;

public class SubmissionType implements Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4892781337148938885L;
	public int id;
	public String name;
	
	public SubmissionType(int argid, String argname) {
		this.id = argid;
		this.name = argname;
	} 
	
	//Parcellable
	public SubmissionType(Parcel in) {		
		this.id = in.readInt();
		this.name = in.readString();
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.id);
		out.writeString(this.name);
	}
	
	public static final Parcelable.Creator<SubmissionType> CREATOR = 
		new Parcelable.Creator<SubmissionType>() {
			public SubmissionType createFromParcel(Parcel in) {
				return new SubmissionType(in);
			}
			
			public SubmissionType[] newArray(int size) {
				return new SubmissionType[size];
			}
	};
}
