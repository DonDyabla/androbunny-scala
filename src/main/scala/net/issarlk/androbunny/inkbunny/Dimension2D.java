package net.issarlk.androbunny.inkbunny;

import android.os.Parcel;
import android.os.Parcelable;

public class Dimension2D implements Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4505102101129253634L;
	public int width;
	public int height;
	
	public Dimension2D(int argwidth, int argheight) {
		this.width = argwidth;
		this.height = argheight;
	}
	
	//Parcellable
	public Dimension2D(Parcel in) {
		this.width = in.readInt();
		this.height = in.readInt();
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeInt(this.width);
		arg0.writeInt(this.height);
	}
	
	public static final Parcelable.Creator<Dimension2D> CREATOR = 
		new Parcelable.Creator<Dimension2D>() {
			public Dimension2D createFromParcel(Parcel in) {
				return new Dimension2D(in);
			}
			
			public Dimension2D[] newArray(int size) {
				return new Dimension2D[size];
			}
	};
}
