/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.inkbunny;

import org.json.JSONObject;

public class InkbunnyAPIException extends java.lang.Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5647943402097759495L;

	public InkbunnyAPIException(Throwable t) {
		super(t);
	}
	
	public InkbunnyAPIException(String s) {
		super(s);
	}

	public static InkbunnyAPIException from(JSONObject json) {
		int code = Integer.parseInt(json.optString("error_code", "-1"));
		String message = json.optString("error_message", "An error occured while communication with Inkbunny.");
		switch(code) {
		case 0: return new InkbunnyAPIInvalidLoginException(message);
		case 2: return new InkbunnyAPIInvalidSessionException(message);
		case 4: return new InkbunnyAPIExpiredRIDException(message);
		default: return new InkbunnyAPIException(message);
		}
	}
}
