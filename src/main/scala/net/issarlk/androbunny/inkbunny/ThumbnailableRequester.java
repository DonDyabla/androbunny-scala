package net.issarlk.androbunny.inkbunny;

public interface ThumbnailableRequester {
	public abstract void receive(int position, Thumbnailable argThumbnailable);
}
