package net.issarlk.androbunny.inkbunny;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import net.issarlk.androbunny.*;
import net.issarlk.androbunny.utils.ABToast;
import net.issarlk.androbunny.utils.SimpleObservable;
import net.issarlk.androbunny.utils.Utils;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Toast;


public final class Search extends Observable implements Parcelable, Observer, Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -632047142863153471L;
	private static final String TAG = "Search";
	public Object[] results;
	private Boolean pageRequested;
	private Object[] requesters;
	private int highestRequested;
	private final ReentrantLock lock = new ReentrantLock();
	private final Condition number_of_submissions_available = lock.newCondition();
	private int sourceLastKnownPosition = -1;
	private int nextPage;
	public String rid;
	public String query;
	private volatile boolean gotInfo = false;
	private long queryDate;
	private int submissions_per_page = 30;		
	private SimpleObservable<String> sid;
	private boolean need_rid = false;
	private boolean got_rid = false;
	public int pages_count = 0;
	public int nb_of_submissions = 0;	
	public String description = "Search result";
	public static final int PAGES = 1;
	public static final int NO_PAGES = 2;
	private final ConcurrentLinkedQueue<Getter> toRuns = new ConcurrentLinkedQueue<Getter>();
	private final Set<Getter> running = Collections.synchronizedSet(new HashSet<Getter>());
		
	/*
	 * Constructors
	 */
	public Search(SimpleObservable<String> argsid, String argquery) {
		this(argsid, argquery, Search.PAGES);
	}
	
	public Search(SimpleObservable<String> argsid, String argquery, int argdopage) {
		this.sid = argsid;
		//Follow sid changes to rerun the Search
		this.sid.addObserver(this);
		this.need_rid = (argdopage == PAGES)?true:false;
		this.pageRequested = Boolean.FALSE;
		this.newQuery(argquery);
		//Observe rating
		Preference.it().ratings().addObserver(this);
		//Observe custom thumbnails
		Preference.it().customThumbnails().addObserver(this);
		//Create a new runnable to get number of submissions asap
		this.cancelToRuns();
		this.toRuns.add(new InfoGetter());
		AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(this);	
	}
	
	/*
	 * Cleanup
	 */
	
	public void freeMemory() {
		this.results = new Object[0];
		this.requesters = new Object[10];
		this.highestRequested = -1;
		this.nextPage = 0;		
		synchronized(this.pageRequested) {
			this.pageRequested = Boolean.FALSE;
		}
	}
	
	public void cleanup() {
		Preference.it().ratings().deleteObserver(this);
		Preference.it().customThumbnails().deleteObserver(this);
		this.sid.deleteObserver(this);
	}
	
	private void cancelToRuns() {
		this.toRuns.clear();
		for (Getter g: this.running) {
			g.cancel(true);
		}
		this.running.clear();
	}
	
	public void clearCache() {
		//Erase cache file for each pages until there are no more
		int p = 0;
		while (true) {
			File f = cacheFileForPage(p);
			if (f.exists()) {
				//Erase file
				Log.d(TAG, "Deleting file " + f.getName());
				f.delete();
			} else {
				//No more files
				break;
			}
			p++;
		}
	}
	
	/*
	 * Info retrieval
	 */
		
	public boolean isLoaded() {
		return this.gotInfo;
	}
	
	public int getNbOfItems() {
		this.lock.lock();
		try {
			while (!this.gotInfo) {
				this.number_of_submissions_available.await();
			}
			return this.nb_of_submissions;
		} catch (InterruptedException e) {
			Log.e(TAG, e.toString());
			return 0;
		} finally {
			this.lock.unlock();
		}
	}

	/*
	 * Observer interface
	 */
	
	public void update(Observable arg0, Object arg1) {
		if (arg0 == Preference.it().ratings()) {
			//The ratings have changed, rebuild results
			AndrobunnyAppSingleton.androbunnyapp.worker.execute(new Runnable() {
				public void run() {
					Search.this.reload();
				}
			});
		} else if (arg0 == Preference.it().customThumbnails()) {
			//The customThumbnails setting has changed, just notify observers
			AndrobunnyAppSingleton.androbunnyapp.worker.execute(new Runnable() {
				public void run() {
					Search.this.setChanged();
					Search.this.notifyObservers();
				}
			});
		} else if (arg0 == this.sid) {
			Log.d(TAG, "sid has changed");
			this.cancelToRuns();
			AndrobunnyAppSingleton.androbunnyapp.worker.execute(new Runnable() {
				public void run() {				
					//The sid has changed, cleanup cache
					Search.this.clearCache();
					//If the sid is not null, rerun query
					if (Search.this.sid.get() != null) {
						Search.this.newQuery(Search.this.query);
					} else {
						//Otherwise, empty the search
						Search.this.empty();
					}
				}
			});
		} else {
			throw new RuntimeException("Unknown observable: " + arg0.toString());
		}
	}
	
	/*
	 * Runnable interface
	 */
	
	public void run() {
		//Run one of the toRun runnables
		Getter toRun = this.toRuns.poll();
		this.running.add(toRun);
		try {
			if (toRun != null) {
				//Run it
				toRun.run();
				if (this.toRuns.peek() != null) {
					//Request another run
					AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(this);
				}
			}
		} finally {
			this.running.remove(toRun);
		}
	}
	
	/*
	 * Data
	 */
	
	public void empty() {
		//Make the search results empty
		this.requesters = new Object[30];
		this.highestRequested = -1;
		this.gotInfo = true;
		this.got_rid = true;
		this.need_rid = false;
		this.nextPage = 0;
		this.submissions_per_page = 0;
		this.nb_of_submissions = 0;
		this.pages_count = 0;
		this.results = new Thumbnailable[0];
		synchronized(this.pageRequested) {
			this.pageRequested = Boolean.FALSE;
		}
		this.queryDate = System.currentTimeMillis();
		this.lock.lock();
		try {
			this.number_of_submissions_available.signalAll();
		} finally {
			this.lock.unlock();
		}
		//Notify observers of change
		this.setChanged();
		this.notifyObservers();		
	}	
	
	public synchronized void reload() {
		//Rebuild results
		Log.d(TAG, "Reloading search");
		this.results = new Object[0];
		this.requesters = new Object[10];
		this.highestRequested = -1;
		for (int page = 0 ; page < this.nextPage ; page++) {
			Object[] tmp;
			try {
				tmp = filterByRating(this.readPageFromCache(page));				
			} catch (IOException e) {
				Log.e(TAG, e.toString());
				// Fail! do a new search
				this.newQuery(this.query);
				return;
			}
			int appendToPos = this.results.length;
			this.results = Utils.growArray(this.results, this.results.length + tmp.length);
			System.arraycopy(tmp, 0, this.results, appendToPos, tmp.length);
		}
		Log.d(TAG, "" + this.results.length + " found.");
		if (this.nextPage > this.pages_count - 1) {
			//Last page reached.
			if (this.results.length != this.nb_of_submissions) {
				//The actual number of results is different than nb of submission
				//That's because of the filter on rating. 
				this.nb_of_submissions = this.results.length;
			}
		}
		//Notify observers of change
		this.setChanged();
		this.notifyObservers();
	}
	
	public void newQuery(String argquery) {
		this.requesters = new Object[30];
		this.highestRequested = -1;
		this.gotInfo = false;
		this.got_rid = false;
		this.query = argquery;
		this.nextPage = 0;
		this.submissions_per_page = 0;
		this.nb_of_submissions = 0;
		this.pages_count = 0;
		this.results = new Thumbnailable[0];
		synchronized(this.pageRequested) {
			this.pageRequested = Boolean.FALSE;
		}
		//Create a new runnable to get number of submissions asap
		this.cancelToRuns();
		this.toRuns.add(new InfoGetter());
		AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(this);
		//Notify observers of change
		this.setChanged();
		this.notifyObservers();
	}
	
	private void sendSourceTo(ThumbnailableRequester requester, int position) {
		//Log.d(TAG, "Send " + position + " to " + requester.toString());
		requester.receive(position, (Thumbnailable) this.results[position]);
		//Log.d(TAG, "...sent " + position + " to " + requester.toString());
		if (position < this.requesters.length) {
			this.requesters[position] = null;
		}
		//Free up memory
		//this.results[position] = null;
	}
	
	private boolean hasSourcePosition(int position) {
		return (position < this.results.length);
	}
	
	private boolean isSourcePosLoadingRequested(int position) {
		synchronized(this.pageRequested) {
			return this.pageRequested.booleanValue();
		}
	}
	
	private void requestSourcePosLoading(int position) {
		synchronized(this.pageRequested) {
			if (!this.pageRequested) {
				//Create a new runnable to load a new page
				this.pageRequested = Boolean.TRUE;
				if (this.nextPage < this.pages_count) {
					this.cancelToRuns();
					this.toRuns.add(new PageGetter());
					AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(this);					
				}
			}
		}
	}
		
	public void request(ThumbnailableRequester requester, int position) {
		if (position > this.highestRequested) {
			this.highestRequested = position;
		}
		if (this.hasSourcePosition(position)) {
			//send
			//Log.d(TAG, "Sending " + position + " in response to request for " + position);
			this.sendSourceTo(requester, position);
		} else {
			//Log.d(TAG, "Requester " + requester + " position " + position);
			if (position >= this.requesters.length) {
    			this.requesters = Utils.growArray(this.requesters, (int)((float)position * 1.2));
    		}
			this.requesters[position] = new WeakReference<ThumbnailableRequester>(requester);
			if (!this.isSourcePosLoadingRequested(position)) {
				//Request loading of the position
				//Log.d(TAG, "Requesting loading of " + position);
				this.requestSourcePosLoading(position);
			} else {
				//we just need to wait for results
				//Log.d(TAG, "Waiting for " + position + " to be loaded.");
			}
		}
	}
	
	public void cancelRequest(ThumbnailableRequester requester, int position) {
		//Remove request
		if (position < this.requesters.length) {
			synchronized(this.requesters) {
				//Log.d(TAG, "Canceling request for " + position);
				this.requesters[position] = null;
			}
		}
	}
	
	private interface Getter extends Future<Boolean> {
		public void run();
	}
	
	private class PageGetter implements Getter {
		private int tries = 5;
		private int page;
		private volatile boolean canceled = false;
		private volatile boolean done = false;
		private volatile Thread runThread = null;
		
		public String toString() {
			return "PageGetter(" + this.page + ")";
		}
		
		public PageGetter() {
			this.page = Search.this.nextPage;			
			Log.d(TAG, "PageGetter(" + this.page + ") created");
		}
		
		public void run() {
			if (this.canceled) {
				return;
			}
			Log.d(TAG, "PageGetter(" + this.page + ") running");
			try {
				Search.this.getPage(this.page, this);
				this.done = true;
				Log.d(TAG, "PageGetter(" + this.page + ") ran. nextPage is now: " + Search.this.nextPage);
			} catch (IOException e) {
				this.tries -= 1;
				if (this.tries > 0) {
					Log.e(TAG, "Network problem, trying again.");
					Search.this.toRuns.add(this);
					AndrobunnyAppSingleton.androbunnyapp.worker.execute(Search.this);
					return;
				} else {
					Log.e(TAG, "Maximum number of tries reached, giving up.");
					this.done = true;
				}
			}
		}

		public boolean cancel(boolean mayInterruptIfRunning) {
			this.canceled = true;
			Log.d(TAG, this.toString() + " canceling");
			if (this.runThread != null && mayInterruptIfRunning) {
				this.runThread.interrupt();
			}
			return true;
		}

		public Boolean get() throws InterruptedException, ExecutionException {
			return null;
		}

		public Boolean get(long timeout, TimeUnit unit)
				throws InterruptedException, ExecutionException,
				TimeoutException {
			return null;
		}

		public boolean isCancelled() {
			Log.d(TAG, this.toString() + " isCanceled: " + this.canceled);
			return this.canceled;
		}

		public boolean isDone() {
			return this.done;
		}
	}
	
	private class InfoGetter implements Getter {	
		int tries = 5;
		private volatile boolean canceled = false;
		private volatile boolean isDone = false;
		private volatile Thread runThread = null;
		
		public void run() {
			if (this.canceled) {
				return;
			}
			try {
				Search.this.getInfo();
				this.isDone = true;
				return;
			} catch (ClientProtocolException e) {
				Log.e(TAG, e.toString());
			} catch (URISyntaxException e) {
				Log.e(TAG, e.toString());
			} catch (IOException e) {
				//Network error. Try again
				Log.e(TAG, "Network error, trying again");
				Search.this.toRuns.add(this);
				AndrobunnyAppSingleton.androbunnyapp.worker.execute(Search.this);
				return;
			} catch (JSONException e) {
				AndrobunnyAppSingleton.androbunnyapp.handler.post(new Runnable() {
					public void run() {
						ABToast.makeText("Error while reading data.", Toast.LENGTH_SHORT).show();
					}
				});
			} catch (InterruptedException e) {
				// interrupted, do nothing
				Log.e(TAG, "Interrupted");
				return;
			}
			// try again
			this.tries -= 1;
			if (this.tries > 0) {
				if (this.canceled) {
					return;
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Log.e(TAG, e.toString());
				}
				this.run();
			} else {
				AndrobunnyAppSingleton.androbunnyapp.handler.post(new Runnable() {
					public void run() {
						ABToast.makeText("Could not read information from Inkbunny. Please try again.", Toast.LENGTH_SHORT).show();
					}
				});				
			}
		}

		public boolean cancel(boolean mayInterruptIfRunning) {
			this.canceled = true;
			if (this.runThread != null && mayInterruptIfRunning) {
				//Interrupt thread
				this.runThread.interrupt();
			}
			return true;
		}

		public Boolean get() throws InterruptedException, ExecutionException {
			return null;
		}

		public Boolean get(long arg0, TimeUnit arg1)
				throws InterruptedException, ExecutionException,
				TimeoutException {
			return null;
		}

		public boolean isCancelled() {
			return this.canceled;
		}

		public boolean isDone() {
			return this.isDone;
		}
	}
	
	private void getInfo() throws URISyntaxException, ClientProtocolException, IOException, JSONException, InterruptedException {
		//Perform the search without getting submissions, in order
		//To get the submission count
		URI uri;
		this.lock.lock();
		try {	
			if (this.sid.get() == null) {
				//nothing to get
				this.empty();
				return;
			}
			if (Thread.interrupted()) {
				throw new InterruptedException();
			}
			uri = new URI("https://inkbunny.net/api_search.php?sid=" + this.sid.get() +"&get_rid=" + (this.need_rid?"yes":"no") + "&page=0&no_submissions=yes&" + this.query);			
			Log.d(TAG, "Getting number of submissions: " + uri.toString());
			String tmps = ABIO.it().readUri(uri);
			JSONObject json_response = (JSONObject) new JSONTokener(tmps).nextValue();
			if (Thread.interrupted()) {
				throw new InterruptedException();
			}
			this.submissions_per_page = json_response.getInt("results_count_thispage");
			this.pages_count = json_response.getInt("pages_count");
			this.nb_of_submissions = json_response.getInt("results_count_all");
			if (this.need_rid) {
				this.rid = json_response.getString("rid");
			}
			this.gotInfo = true;
			this.queryDate = System.currentTimeMillis();			
			this.number_of_submissions_available.signalAll();
			//Notify observers of change
			this.setChanged();
			this.notifyObservers();
		} finally {
			this.lock.unlock();
		}
	}
	
	private File cacheFileForPage(int page) {
		File cachedir = AndrobunnyAppSingleton.androbunnyapp.getCacheDir();
		if (this.need_rid && this.rid != null) {
			return new File(cachedir, "search-" + this.rid + "-page-" + page);
		} else {
			return new File(cachedir, "search-" + this.hashCode() + "-page-" + page);
		}
	}
	
	private void writePageToCache(Object[] items, int page) {
		File cachefile = cacheFileForPage(page);		
		Parcel parcel = Parcel.obtain();
		parcel.writeString("CachedPage:");
		parcel.writeString(this.rid);
		parcel.writeInt(this.submissions_per_page);
		parcel.writeInt(this.pages_count);
		parcel.writeInt(this.nb_of_submissions);
		parcel.writeInt(items.length);
		for (int i = items.length - 1 ; i >= 0 ; i--) {
			if (items[i] instanceof Submission) {
				parcel.writeInt(0);
				((Submission)(items[i])).writeToParcel(parcel, 0);
			} else if (items[i] instanceof LightweightThumbnailable) {
				parcel.writeInt(1);
				((LightweightThumbnailable)(items[i])).writeToParcel(parcel, 0);
			} else {
				throw new RuntimeException("Unknown class: " + items[i].getClass());
			}
		}
		
		FileOutputStream fo;
		try {
			fo = new FileOutputStream(cachefile);
			fo.write(parcel.marshall());
			fo.close();	
		} catch (FileNotFoundException e) {
			Log.e(TAG, e.toString());
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		}
	}
	
	private Object[] readPageFromCache(int page) throws IOException {
		File cachefile = cacheFileForPage(page);		
		FileInputStream fi;
		Object[] items = null;
		fi = new FileInputStream(cachefile);
		try {
			Parcel parcel = Parcel.obtain();		
			long length = cachefile.length();
			byte[] data = new byte[(int) length];
			BufferedInputStream in = new BufferedInputStream(fi);
			
			int offset = 0;
		    int numRead = 0;
		    while (offset < length
		           && (numRead=in.read(data, offset, data.length-offset)) >= 0) {
		        offset += numRead;
		    }
			
			parcel.unmarshall(data, 0, (int)length);
			parcel.setDataPosition(0);
			if (!"CachedPage:".equals(parcel.readString())) {
				//Corrupted file
				Log.e(TAG, "Couldnt load file " + cachefile.toString() + " ... deleting it.");
				cachefile.delete();
				throw new IOException("Corrupted file");
			}
			this.rid = parcel.readString();
			this.submissions_per_page = parcel.readInt();
			this.pages_count = parcel.readInt();
			this.nb_of_submissions = parcel.readInt();
			items = new Object[parcel.readInt()]; 
			for (int i = items.length - 1 ; i >= 0 ; i--) {
				int type = parcel.readInt();
				if (type == 0) {
					items[i] = Submission.CREATOR.createFromParcel(parcel);
				} else if (type == 1) {
					items[i] = LightweightThumbnailable.CREATOR.createFromParcel(parcel);
				} else {
					throw new RuntimeException("Unknown type: " + type);
				}
			}	
		} finally {
			fi.close();
		}
	    return items;
	}
	
	private Object[] processPageFromJSonPage(JSONObject data) throws JSONException {
		Object[] tmp;
		JSONArray submissions = data.getJSONArray("submissions");
		int l = submissions.length();			
		Log.d(TAG, "Nb of submissions found: " + submissions.length());
		tmp = new Submission[l];
		for (int i = 0 ; i < l ; i++) {
			JSONObject json_submission = submissions.getJSONObject(i);
			tmp[i] = new Submission(json_submission);
		}
		return tmp;
	}
	
	private Object[] getPageData(int pageNumber) throws IOException {
		try {
			Object[] items = filterByRating(this.readPageFromCache(pageNumber));
			Log.d(TAG, "Got page " + pageNumber + " from cache.");
			return items;
		} catch (IOException e) {
			//not in cache
		}
		// Get from network
		if (!this.got_rid) {
			//it is the first search.
			//Perform initial search
			URI uri;
			try {
				uri = new URI("https://inkbunny.net/api_search.php?sid=" + this.sid.get() +"&get_rid=" + (this.need_rid?"yes":"no") + "&page=" + (pageNumber + 1) + "&" + this.query);
				Log.d(TAG, "Performing initial search: " + uri.toString());
				String tmps = ABIO.it().readUri(uri);
				JSONObject json_response = (JSONObject) new JSONTokener(tmps).nextValue();
				
				Object[] items = this.processPageFromJSonPage(json_response);
				this.writePageToCache(items, pageNumber);
				//Filter results
				items = filterByRating(items);
				Log.d(TAG, "Got page " + pageNumber + " from network.");
				return items;
			} catch (URISyntaxException e) {
				Log.e(TAG, e.toString());
			} catch (ClientProtocolException e) {
				Log.e(TAG, e.toString());
			} catch (JSONException e) {
				Log.e(TAG, e.toString());
			}
		} else {
			if (!this.need_rid) {
				Log.d(TAG, "This search doesn't have pages.");
				//This search doesn't handle pages of results
				return null;					
			} else {						
				//Query page of result and return it
				try {    	
					//Perform a search
					URI uri = new URI("https://inkbunny.net/api_search.php?sid=" + this.sid.get() +"&rid=" + this.rid + "&page=" + (pageNumber + 1) + "&submissions_per_page=" + this.submissions_per_page);
					Log.d(TAG, "Fetching  page " + (pageNumber + 1) + ": " + uri.toString());
					String tmps = ABIO.it().readUri(uri);
					JSONObject json_response = (JSONObject) new JSONTokener(tmps).nextValue();				
					Object[] items = this.processPageFromJSonPage(json_response);					
					this.writePageToCache(items, pageNumber);
					//Filter results
					items = filterByRating(items);
					return items;
				} catch (URISyntaxException e) {
					Log.e(TAG, e.toString());
				} catch (ClientProtocolException e) {
					Log.e(TAG, e.toString());
				} catch (JSONException e) {
					Log.e(TAG, e.toString());
				}
			}
		}
		return null;
	}
	
	private void getPage(int pageNumber, Getter getter) throws IOException {
		if (getter.isCancelled()) {
			return;
		}
		//Is it the first search?
		Object[] tmp;
		int old_result_length = this.results.length;
		try {
			tmp = this.getPageData(pageNumber);					
			if (this.need_rid && (! this.got_rid) && (this.rid != null)) {
				this.got_rid = true;
			}	
			int appendToPos = this.results.length;
			this.results = Utils.growArray(this.results, this.results.length + tmp.length);
			System.arraycopy(tmp, 0, this.results, appendToPos, tmp.length);
			Log.d(TAG, "Found " + tmp.length + " submissions.");
		} finally {
			synchronized(this.pageRequested) {
				this.pageRequested = Boolean.FALSE;
			}
		}
		if (getter.isCancelled()) {
			return;
		}
		//Page has been loaded
		this.nextPage = pageNumber + 1;
		int start = old_result_length;
		int stop = this.results.length;
		int i;
		for (i = start ; i < stop ; i++) {
			if (getter.isCancelled()) {
				return;
			}
			//Log.d(TAG, "i >= this.requesters.length ? : " + i + " >= " + this.requesters.length + " ?");
			if (i > this.highestRequested) {
				//No more requesters after this, stop
				break;
			}
			@SuppressWarnings("unchecked")
			WeakReference<ThumbnailableRequester> weakRequester = (WeakReference<ThumbnailableRequester>) this.requesters[i];
			if (weakRequester != null) {
				ThumbnailableRequester requester = weakRequester.get();
				//known position, send
				if (requester != null) {
					this.requesters[i] = null;
					this.sendSourceTo(requester, i);
				} else {
					//Log.d(TAG, "Request for pos " + i + "... requester has been GCed.");						
				}
			} else {
				//Log.d(TAG, "Request for pos " + pos + "... null weak reference (shouldnt happen)");
			}
		}
		if (getter.isCancelled()) {
			return;
		}
		Log.d(TAG, "i: " + i + " stop: " + stop);
		if (pageNumber >= this.pages_count - 1) {
			//Last page reached.
			if (this.results.length != this.nb_of_submissions) {
				//The actual number of results is different than nb of submission
				//That's because of the filter on rating. Notify observers that
				//the search changed significantly.
				this.nb_of_submissions = this.results.length;
				//Remove requesters for positions after the end
				for (int j = this.results.length ; j < this.requesters.length ; j++) {
					this.requesters[j] = null;
				}
				this.setChanged();
				this.notifyObservers();
			}
		}
		if (getter.isCancelled()) {
			return;
		}
		if (i == stop) {
			//There are requesters for position higher than what the page added
			Log.d(TAG, "Requesting loading of more, i = " + i + " stop = " + stop);
			this.requestSourcePosLoading(Search.this.sourceLastKnownPosition + 1);
		}
		return;
	}
	
	private Object[] filterByRating(Object[] arg) {
		Vector<Thumbnailable> result = new Vector<Thumbnailable>();
		for (int i = 0 ; i < arg.length ; i++) {
			Object obj = arg[i];
			if (obj instanceof Thumbnailable) {
				Thumbnailable thumb = (Thumbnailable)obj;
				//Should we keep it ?
				int rating_action = Preference.it().getRating(thumb.getRating().id);
				if (rating_action != Preference.it().RATING_HIDE()) {
					//Keep it
					result.add(thumb);
				}
			}
		}
		//Return result
		return result.toArray();
	}

	public String getDescription() {
		return this.description;
	}
	
	//Parcellable
	public Search(Parcel in) {		
		this.results = new Object[in.readInt()];
		for (int i = this.results.length - 1 ; i >= 0 ; i--) {
			if (in.readInt() == 0) {
				this.results[i] = Submission.CREATOR.createFromParcel(in);
			} else {
				this.results[i] = LightweightThumbnailable.CREATOR.createFromParcel(in);
			}
		}
		this.pageRequested = Boolean.FALSE;
		this.requesters = new Object[30];
		this.highestRequested = in.readInt();
		this.sourceLastKnownPosition = in.readInt();
		this.nextPage = in.readInt();
		this.rid = in.readString();
		this.query = in.readString();
		this.gotInfo = (in.readInt() == 1 ? true : false);
		this.queryDate = in.readLong();
		this.submissions_per_page = in.readInt();
		this.sid = new SimpleObservable<String>(in.readString());
		this.need_rid = (in.readInt() == 1 ? true : false);
		this.got_rid = (in.readInt() == 1 ? true : false);
		this.pages_count = in.readInt();
		this.nb_of_submissions = in.readInt();	
		this.description = in.readString();

		
		//Observe rating
		Preference.it().ratings().addObserver(this);
		//Observe custom thumbnails
		Preference.it().customThumbnails().addObserver(this);
		//Compare the read SID with the API's current SID
		if (this.sid.get() != API.it().getSID().get()) {
			//The differ, this search is obsolete. Rerun it
			//Set the sid to the API's object
			this.sid = API.it().getSID();
			//Observe it
			this.sid.addObserver(this);
			//Rerun search
			this.newQuery(this.query);
		} else {
			//Set the sid to the API's object
			this.sid = API.it().getSID();
			//Observe it
			this.sid.addObserver(this);
			//Create a new runnable to get number of submissions asap
			this.cancelToRuns();
			this.toRuns.add(new InfoGetter());
			AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(this);
		}
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.results.length);
		for (int i = this.results.length - 1 ; i >= 0 ; i--) {
			Parcelable obj = (Parcelable)(this.results[i]);
			if (obj instanceof Submission) {
				out.writeInt(0);
			} else if (obj instanceof LightweightThumbnailable) {
				out.writeInt(1);
			} else {
				throw new RuntimeException("Class not known: " + obj.getClass().toString());
			}
			obj.writeToParcel(out, 0);
		}
		out.writeInt(this.highestRequested);
		out.writeInt(this.sourceLastKnownPosition);
		out.writeInt(this.nextPage);
		out.writeString(this.rid);
		out.writeString(this.query);
		out.writeInt(this.gotInfo ? 1 : 0);
		out.writeLong(this.queryDate);
		out.writeInt(this.submissions_per_page);
		out.writeString(this.sid.get());
		out.writeInt(this.need_rid ? 1 : 0);
		out.writeInt(this.got_rid ? 1 : 0);
		out.writeInt(this.pages_count);
		out.writeInt(this.nb_of_submissions);	
		out.writeString(this.description);
		
		
	}
	
	public static final Parcelable.Creator<Search> CREATOR = 
		new Parcelable.Creator<Search>() {
			public Search createFromParcel(Parcel in) {
				return new Search(in);
			}
			
			public Search[] newArray(int size) {
				return new Search[size];
			}
	};
}	
