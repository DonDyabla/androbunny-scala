package net.issarlk.androbunny.inkbunny;

public class InkbunnyAPIInvalidSessionException extends InkbunnyAPIException {

	public InkbunnyAPIInvalidSessionException(String s) {
		super(s);
	}

}
