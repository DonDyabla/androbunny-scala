/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.inkbunny;

import java.net.URI;
import java.net.URISyntaxException;

import net.issarlk.androbunny.Log;

import android.os.Parcel;
import android.os.Parcelable;

public class Icon implements Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2332918129173657507L;
	private static final String TAG = "Icon"; 
	public static final int LARGE = 0;
	public static final int MEDIUM = 1;
	public static final int SMALL = 2;
	
	public int type;
	public URI url;
	
	public Icon(int argtype, URI argurl) {
		this.type = argtype;
		this.url = argurl;
	}
	
	//Parcellable
	public Icon(Parcel in) {
		this.type = in.readInt();
		String tmp = in.readString();
		if (tmp != null) {
			try {
				this.url = new URI(tmp);
			} catch (URISyntaxException e) {
				Log.e(TAG, e.toString());
			}
		}
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.type);
		out.writeString(this.url.toString());
	}
	
	public static final Parcelable.Creator<Icon> CREATOR = 
		new Parcelable.Creator<Icon>() {
			public Icon createFromParcel(Parcel in) {
				return new Icon(in);
			}
			
			public Icon[] newArray(int size) {
				return new Icon[size];
			}
	};
}
