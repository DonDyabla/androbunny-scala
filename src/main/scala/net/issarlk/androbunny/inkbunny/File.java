/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.inkbunny;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.issarlk.androbunny.AndrobunnyAppSingleton;
import net.issarlk.androbunny.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public final class File implements Parcelable, Thumbnailable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6845949140894407267L;
	private static final String TAG = "File";
	public static final int FULL = 0;
	public static final int SCREEN = 1;
	public static final int PREVIEW = 2;
	public static final int THUMBNAIL = 3;
	public static final int INITIAL = 4;
	public int id;
	public String name;
	public Thumbnail[] thumbnails;
	public URI[] file_urls;
	public String mimetype;
	public Submission submission;
	public int submission_id;
	public int user_id;
	public int submission_file_order;
	public Dimension2D[] dimensions;
	public String[] md5s;
	public int deleted;
	public String create_datetime;
	public String create_datetime_usertime;
	
	public File(Submission argsubmission, JSONObject json) throws JSONException, URISyntaxException {
		this.id = json.getInt("file_id");
		this.name = json.getString("file_name");
		this.thumbnails = Thumbnail.extractFrom(json);
		this.mimetype = json.getString("mimetype");
		this.submission_id = json.getInt("submission_id");
		this.user_id = json.getInt("user_id");
		this.submission_file_order = json.getInt("submission_file_order");
		this.dimensions = new Dimension2D[3];
		try {
			this.dimensions[File.FULL] = new Dimension2D(
				json.getInt("full_size_x"),
				json.getInt("full_size_y"));
			this.dimensions[File.SCREEN] = new Dimension2D(
				json.getInt("screen_size_x"),
				json.getInt("screen_size_y"));
			this.dimensions[File.PREVIEW] = new Dimension2D(
				json.getInt("preview_size_x"),
				json.getInt("preview_size_y"));
		} catch(JSONException e) {
			//This file has no dimensions
		}
		this.md5s = new String[5];
		this.md5s[File.FULL] = json.getString("full_file_md5");
		this.md5s[File.SCREEN] = json.getString("large_file_md5");
		this.md5s[File.PREVIEW] = json.getString("small_file_md5");
		this.md5s[File.THUMBNAIL] = json.getString("thumbnail_md5");
		this.md5s[File.INITIAL] = json.getString("initial_file_md5");
		this.file_urls = new URI[3];
		this.file_urls[FULL] = new URI(json.getString("file_url_full"));
		this.file_urls[SCREEN] = new URI(json.getString("file_url_screen"));
		this.file_urls[PREVIEW] = new URI(json.getString("file_url_preview"));
		this.deleted = API.getBool(json, "deleted");
		this.create_datetime = json.getString("create_datetime");
		this.create_datetime_usertime = json.getString("create_datetime_usertime");
		this.submission = argsubmission;
	}
	
	public static File[] readArray(Submission argsubmission, JSONArray array) throws JSONException, URISyntaxException {
		File[] result;
		result = new File[array.length()];
		for (int i =  array.length() - 1 ; i >= 0 ; i-- ) {
			result[i] = new File(argsubmission, array.getJSONObject(i));
		}
		return result;
	}

    public boolean isImage() {
        return this.mimetype.startsWith("image/");
    }

    public boolean isAudio() {
        return this.mimetype.startsWith("audio/");
    }

	public int getID() {
		return this.id;
	}
	
	public String getAuthor() {
		return(this.submission.getAuthor());
	}
	
	public SubmissionType getSubmissionType() {
		return(this.submission.getSubmissionType());
	}
	
	public Rating getRating() {
		return(this.submission.getRating());
	}
	
	public int getDigitalSales() {
		return(this.submission.getDigitalSales());
	}
	
	public int getPrintSales() {
		return(this.submission.getDigitalSales());
	}
	
	//Returns the smallest thumbnail larger than w and h
	public Thumbnail getLargerThumbnail(int type, int w, int h) {
		if (this.thumbnails == null || this.thumbnails.length < 1) {
			return null;
		}
		int i, start, end;
		if (type == Thumbnailable.CUSTOM) {
			start = 0;
			end = 2;				
		} else {
			start = 3;
			end = 5;				
		}
		Thumbnail result = this.thumbnails[start];
		for (i = end ; i >= start ; i--) {
			if (!this.thumbnails[i].larger(result) && this.thumbnails[i].larger(w, h)) {
				result = this.thumbnails[i];
			}
		}
		if (result.larger(w, h)) {
			//Found one
			return result;
		} else {
			//None are large enough... return the biggest
			return this.getLargestThumbnail(type);
		}			
	}
	
	public Thumbnail getLargestThumbnail(int type) {			
		if (this.thumbnails == null || this.thumbnails.length < 1) {
			return null;
		}
		int i, start, end;
		if (type == Thumbnailable.CUSTOM) {
			start = 0;
			end = 2;				
		} else {
			start = 3;
			end = 5;				
		}
		Thumbnail result = this.thumbnails[start];
		for (i = end ; i >= start ; i--) {
			if (this.thumbnails[i].larger(result)) {
				result = this.thumbnails[i];
			}
		}
		return result;
	}
	
	//Returns the smallest image larger than w and h
	public URI getLargerImage(int w, int h) {
		Dimension2D wanted = new Dimension2D(w, h);
		if (this.file_urls == null || this.file_urls.length < 1) {
			return null;
		}
		URI result = this.getLargerCachedImage(w, h);
		if (result != null) {
			return result;
		}
		int i;		
		result = this.file_urls[0];
		Dimension2D resultDim = this.dimensions[0];
		if (resultDim == null) {
			//Not an image
			return null;
		}
		for (i = 2 ; i >= 0 ; i--) {			
			if (!larger(this.dimensions[i], resultDim) && larger(this.dimensions[i], wanted)) {
				result = this.file_urls[i];
				resultDim = this.dimensions[i];
			}
		}
		Log.d(TAG, "getLargerImage(" + w + ", " + h + ") returning " + result.toString());
		return result;
	}
	
	public URI getLargerCachedImage(int w, int h) {
		Dimension2D wanted = new Dimension2D(w, h);
		if (this.file_urls == null || this.file_urls.length < 1) {
			return null;
		}
		int i;		
		URI result = null;
		Dimension2D resultDim = null;
		java.io.File cachedir = AndrobunnyAppSingleton.androbunnyapp.getCacheDir();
		for (i = 2 ; i >= 0 ; i--) {
			byte[] bytesOfMessage = this.file_urls[i].toString().getBytes();
			MessageDigest md;
			try {
				md = MessageDigest.getInstance("MD5");
				byte[] thedigest = md.digest(bytesOfMessage);
				StringBuffer hexString = new StringBuffer();
				for (int j=0;j < thedigest.length;j++) {
					hexString.append(Integer.toHexString(0xFF & thedigest[j]));
				}	
				java.io.File cachefile = new java.io.File(cachedir, hexString.toString());
				if (cachefile.exists()) {
					if (resultDim == null || ( !larger(this.dimensions[i], resultDim) && larger(this.dimensions[i], wanted) )) {
						result = this.file_urls[i];
						resultDim = this.dimensions[i];
					}
					
				}	
				Log.d(TAG, "getLargerCachedImage(" + w + ", " + h + ") returning " + result);
			} catch (NoSuchAlgorithmException e) {
				Log.e(TAG, e.toString());
			}
		}
		return result;
	}
	
	private static boolean larger(Dimension2D a, Dimension2D b) {
		return a.width > b.width;
	}
	
	//Parcellable
	public File(Parcel in) {
		this.id = in.readInt();
		this.name = in.readString();
		this.thumbnails = new Thumbnail[in.readInt()];
		for (int i = this.thumbnails.length - 1 ; i >= 0 ; i--) {
			if (in.readInt() == 1) {
				this.thumbnails[i] = Thumbnail.CREATOR.createFromParcel(in);
			}
		}
		this.mimetype = in.readString();
		this.submission_id = in.readInt();
		this.user_id = in.readInt();
		this.submission_file_order = in.readInt();
		this.dimensions = new Dimension2D[in.readInt()];
		for (int i = this.dimensions.length - 1 ; i >= 0 ; i--) {
			if (in.readInt() == 1) {
				this.dimensions[i] = Dimension2D.CREATOR.createFromParcel(in);
			}
		}
		this.md5s = new String[5];
		in.readStringArray(this.md5s);
		this.file_urls = new URI[3];
		for (int i = 0 ; i < 3 ; i++) {
			String tmp = in.readString();
			if (tmp != null) {
				try {
					this.file_urls[i] = new URI(tmp);
				} catch (URISyntaxException e) {
					Log.e(TAG, e.toString());
				}		
			}
		}		
		this.deleted = in.readInt();		
		this.create_datetime = in.readString();
		this.create_datetime_usertime = in.readString();
	}

	public int describeContents() {		
		return 0;
	}

	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(this.id);
		out.writeString(this.name);
		out.writeInt(this.thumbnails.length);
		for (int i = this.thumbnails.length - 1 ; i >= 0 ; i--) {
			if (this.thumbnails[i] != null) {
				out.writeInt(1);
				this.thumbnails[i].writeToParcel(out, 0);
			} else {
				out.writeInt(0);
			}
			
		}
		out.writeString(this.mimetype);
		out.writeInt(this.submission_id);
		out.writeInt(this.user_id);
		out.writeInt(this.submission_file_order);
		out.writeInt(this.dimensions.length);
		for (int i = this.dimensions.length - 1 ; i >= 0 ; i--) {
			if (this.dimensions[i] != null) {
				out.writeInt(1);
				this.dimensions[i].writeToParcel(out, 0);	
			} else {
				out.writeInt(0);
			}			
		}
		out.writeStringArray(this.md5s);
		out.writeString(this.file_urls[0].toString());
		out.writeString(this.file_urls[1].toString());
		out.writeString(this.file_urls[2].toString());
		out.writeInt(this.deleted);		
		out.writeString(this.create_datetime);
		out.writeString(this.create_datetime_usertime);
	}
	
	public static final Parcelable.Creator<File> CREATOR = 
		new Parcelable.Creator<File>() {
			public File createFromParcel(Parcel in) {
				return new File(in);
			}
			
			public File[] newArray(int size) {
				return new File[size];
			}
	};
}
