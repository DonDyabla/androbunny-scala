/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.inkbunny

import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URI
import java.net.URISyntaxException
import java.net.URLEncoder
import java.text.CharacterIterator
import java.text.StringCharacterIterator
import java.util.HashMap
import java.util.regex.Matcher
import java.util.regex.Pattern
import net.issarlk.androbunny.utils.SimpleObservable
import org.apache.http.client.ClientProtocolException
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import android.os.Parcel
import android.util.Log
import net.issarlk.androbunny.{ABIO, AndrobunnyAppSingleton}

/**
 * @author g-roulot
 *
 */
object API {
  private val serialVersionUID: Long = -5706275778291461485L
  private val TAG: String = "InkbunnyAPI"
  val RATING_GENERAL: Int = 1
  val RATING_MATURE: Int = 2
  val RATING_ADULT: Int = 4
  val SEARCH_KEYWORDS: Int = 1
  val SEARCH_TITLES: Int = 2
  val SEARCH_DESCRIPTIONS: Int = 4
  val sid: SimpleObservable[String] = new SimpleObservable[String](null)
  def getSID = sid
  var ratingsmask: String = null
  def getRatingsmask = ratingsmask
  def setRatingsmask(arg: String) = ratingsmask = arg

  def it() = this

  def extractTree(pattern: Pattern, argdata: String): String = {
    var matches: Matcher = pattern.matcher(argdata)
    if (matches.matches) {
      var data = matches.group(1)
      var result: StringBuilder = new StringBuilder
      var taglevel: Int = 0
      var it: CharacterIterator = new StringCharacterIterator(data)
      var mode: Int = 0
      var inparamname: Int = 0
      var inparamvalue: Int = 0
      var outsidetags: Int = 0
      var intagname: Int = 0
      var paramvaluechar: Char = '"'
      var params: HashMap[String, String] = new HashMap[String, String]
      var tagname: StringBuilder = new StringBuilder
      var paramname: StringBuilder = new StringBuilder
      var paramvalue: StringBuilder = new StringBuilder
      var text: StringBuilder = new StringBuilder
      var c: Char = it.first
      while (c != CharacterIterator.DONE) {
        {
          var cont = false
          if (c == '<') {
            c = it.next
            params.clear
            outsidetags = 0
            result.append(text)
            if (c == '/') {
              mode = 3
              intagname = 1
              c = it.next
            }
            else {
              mode = 2
              intagname = 1
            }
            tagname = new StringBuilder
          }
          if (intagname == 1) {
            if (c < 'a' || c > 'z') {
              intagname = 0
            }
            else {
              tagname.append(c)
            }
          }
          if ((mode == 2 || mode == 3) && intagname == 0 && inparamname == 0 && inparamvalue == 0 && c >= 'a' && c <= 'z') {
            inparamname = 1
            paramname = new StringBuilder
          }
          if (inparamname == 1) {
            if (c == '=') {
              inparamname = 0
              inparamvalue = 1
              paramvaluechar = it.next
              paramvalue = new StringBuilder
              cont = true
            }
            else if (c != ' ') {
              paramname.append(c)
            }
          }
          if (!cont && inparamvalue == 1) {
            if (c == paramvaluechar) {
              inparamvalue = 0
              params.put(paramname.toString, paramvalue.toString)
            }
            else {
              paramvalue.append(c)
            }
          }
          if (!cont && mode == 2 && inparamvalue == 0 && c == '/') {
            c = it.next
            if (c == '>') {
              mode = 4
              if (tagname.toString == "br") {
                result.append("<br/>")
              }
            }
            else {
              c = it.previous
            }
          }
          if (!cont && c == '>') {
            if (mode == 2) {
              taglevel += 1
              outsidetags = 1
              text = new StringBuilder
              var paramstr: String = ""
              import scala.collection.JavaConversions._
              for (key <- params.keySet) {
                paramstr += " " + key + "=\"" + params.get(key) + "\""
              }
              result.append("<" + tagname.toString + paramstr + ">")
              mode = 0
              cont = true
            }
            else if (mode == 3) {
              taglevel -= 1
              outsidetags = 1
              text = new StringBuilder
              result.append("</" + tagname.toString + ">\n")
              mode = 0
              if (taglevel <= 0) {
                return result.toString
              }
              cont = true
            }
            else if (mode == 4) {
              outsidetags = 1
              text = new StringBuilder
              result.append("<" + tagname.toString + "/>\n")
              mode = 0
              cont = true
            }
          }
          if (!cont && outsidetags == 1) {
            text.append(c)
          }
        }
        c = it.next
      }

      var res: String = result.toString
      return res
    }
    return null
  }

  /*def repeat(c: Char, i: Int): String = {
    var tst: StringBuilder = new StringBuilder
    var j: Int = 0
    while (j < i) {
      {
        tst.append(c)
      }
      ({
        j += 1; j
       })
    }
    return tst.toString
  }    */

  def getBool(json: JSONObject, field: String): Int = {
    var tmp: String = json.getString(field)
    if (tmp == "f") {
      return 0
    }
    else {
      return 1
    }
  }

  @throws(classOf[InkbunnyAPIException])
  def login(username: String, password: String): Unit = {
    var json: JSONObject = null
    try {
      var tmps: String = ABIO.readUri(new URI("https://inkbunny.net/api_login.php?username=" + username + "&password=" + password))
      json = new JSONTokener(tmps).nextValue.asInstanceOf[JSONObject]
      this.sid.set(json.getString("sid"))
      this.ratingsmask = json.getString("ratingsmask")
    }
    catch {
      case e: JSONException => {
        if (json != null && json.has("error_code")) {
          throw InkbunnyAPIException.from(json)
        }
        else {
          throw (new InkbunnyAPIException(e))
        }
      }
      case e: URISyntaxException => {
        throw (new InkbunnyAPIException(e))
      }
      case e: ClientProtocolException => {
        throw (new InkbunnyAPIException(e))
      }
    }
  }

  def logout: Unit = {
    this.sid.set(null)
    this.ratingsmask = null
  }

  def getLatest: Search = {
    var result: Search = new Search(this.sid, "")
    result.description = "Recent submissions"
    return result
  }

  def getPopular: Search = {
    var result: Search = new Search(this.sid, "dayslimit=3&orderby=views&count_limit=100&submissions_per_page=100&random=yes&scraps=no", Search.NO_PAGES)
    result.description = "Recent popular submissions"
    return result
  }

  def getLatestWritings: Search = {
    var result: Search = new Search(this.sid, "type=12")
    result.description = "Recent written submissions"
    return result
  }

  def getLatestMusics: Search = {
    var result: Search = new Search(this.sid, "type=10,11")
    result.description = "Recent musics"
    return result
  }

  def getNew: Search = {
    var result: Search = new Search(this.sid, "unread_submissions=yes")
    result.description = "New submissions"
    return result
  }

  def getUserGallery(argUser: User): Search = {
    var result: Search = new Search(this.sid, "user_id=" + argUser.id + "&scraps=no")
    result.description = argUser.name + "'s gallery"
    return result
  }

  def getUserScraps(argUser: User): Search = {
    var result: Search = new Search(this.sid, "user_id=" + argUser.id + "&scraps=only")
    result.description = argUser.name + "'s scraps"
    return result
  }

  def getUserFavorites(argUser: User): Search = {
    var result: Search = new Search(this.sid, "favs_user_id=" + argUser.id)
    result.description = argUser.name + "'s favorites"
    return result
  }

  def getPool(id: Int): Search = {
    var result: Search = new Search(this.sid, "pool_id=" + id + "&orderby=pool_order")
    result.description = "Pool"
    return result
  }

  def searchText(text: String, fields: Int, dayslimit: Int): Search = {
    var result: Search = null
    try {
      result = new Search(this.sid, "text=" + URLEncoder.encode(text, "UTF-8") + "&keywords=" + (if (((fields & SEARCH_KEYWORDS) != 0)) "yes" else "no") + "&title=" + (if (((fields & SEARCH_TITLES) != 0)) "yes" else "no") + "&description=" + (if (((fields & SEARCH_DESCRIPTIONS) != 0)) "yes" else "no") + (if ((dayslimit > 0)) "&dayslimit=" + dayslimit else ""))
      result.description = "Search results for " + text
      return result
    }
    catch {
      case e: UnsupportedEncodingException => {
        Log.e(TAG, e.toString)
        return null
      }
    }
  }

  def scrapeUserPage(argUser: User): Unit = {
    argUser.profile = "Couldn't read user profile"
    var data: String = ""
    try {
      data = ABIO.readUri(new URI("https://inkbunny.net/" + argUser.name))
    }
    catch {
      case e2: URISyntaxException => {
        Log.e(TAG, e2.toString)
      }
    }
    var pattern: Pattern = Pattern.compile(".*<div class='title'>Profile</div>.*?(<.*)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL)
    argUser.profile = extractTree(pattern, data)
    pattern = Pattern.compile("<img [^>]*? src='([^']*)' [^>]*? alt='" + argUser.name + "' title='" + argUser.name + "'[^>]*?/>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL)
    var matcher: Matcher = pattern.matcher(data)
    if (matcher.find) {
      try {
        argUser.icons(0) = new Icon(Icon.LARGE, new URI(matcher.group(1)))
      }
      catch {
        case e: URISyntaxException => {
          Log.e(TAG, e.toString)
        }
      }
    }
  }

  def getUserFromName(username: String): User = {
    var tmps: String = ""
    try {
      tmps = ABIO.readUri(new URI("https://inkbunny.net/api_username_autosuggest.php?username=" + URLEncoder.encode(username, "UTF-8")))
    }
    catch {
      case e: ClientProtocolException => {
        Log.e(TAG, e.toString)
        return null
      }
      case e: UnsupportedEncodingException => {
        Log.e(TAG, e.toString)
        return null
      }
      case e: IOException => {
        Log.e(TAG, e.toString)
        return null
      }
      case e: URISyntaxException => {
        Log.e(TAG, e.toString)
        return null
      }
    }
    try {
      var json_response: JSONObject = null
      json_response = new JSONTokener(tmps).nextValue.asInstanceOf[JSONObject]
      var suggestions: JSONArray = json_response.getJSONArray("results")
      var suggestion: JSONObject = suggestions.getJSONObject(0)
      return new User(suggestion.getInt("id"), suggestion.getString("singleword"))
    }
    catch {
      case e: JSONException => {
        Log.e(TAG, e.toString)
        return null
      }
    }
  }

  def getSubmissionByID(id: Int): Submission = {
    try {
      var tmps: String = ABIO.readUri(new URI("https://inkbunny.net/api_submissions.php?sid=" + this.sid.get + "&show_description_bbcode_parsed=yes&show_writing_bbcode_parsed=yes&submission_ids=" + id))
      var json: JSONObject = new JSONTokener(tmps).nextValue.asInstanceOf[JSONObject]
      var submissions: JSONArray = json.getJSONArray("submissions")
      json = submissions.getJSONObject(0)
      return new Submission(json)
    }
    catch {
      case e: ClientProtocolException => {
        Log.e(TAG, "Exception: " + e.toString)
        return null
      }
      case e: URISyntaxException => {
        Log.e(TAG, "Exception: " + e.toString)
        return null
      }
      case e: JSONException => {
        Log.e(TAG, "Exception: " + e.toString)
        return null
      }
    }
  }

  def getHeavySubmission(light_submission: Submission): Submission = {
    return this.getSubmissionByID(light_submission.id)
  }

  def isLoggedIn: Boolean = {
    if (this.sid.get == null) {
      return false
    }
    try {
      var tmps: String = ABIO.readUri(new URI("https://inkbunny.net/api_search.php?sid=" + this.sid.get + "&dayslimit=1&count_limit=1&no_submissions=yes"))
      var json_response: JSONObject = new JSONTokener(tmps).nextValue.asInstanceOf[JSONObject]
      json_response.get("sid")
      return true
    }
    catch {
      case e: ClientProtocolException => {
        throw new InkbunnyAPIException(e)
      }
      case e: URISyntaxException => {
        throw new InkbunnyAPIException(e)
      }
      case e: JSONException => {
        Log.e(TAG, e.toString)
        return false
      }
      case e: IOException => {
        throw new InkbunnyAPIException(e)
      }
    }
  }

  def getKeywords(text: String): Array[String] = {
    var tmps: String = null
    try {
      tmps = ABIO.readUri(new URI("https://inkbunny.net/api_search_autosuggest.php?ratingsmask=" + this.ratingsmask + "&keyword=" + URLEncoder.encode(text, "UTF-8")))
    }
    catch {
      case e: ClientProtocolException => {
        Log.e(TAG, e.toString)
        return null
      }
      case e: UnsupportedEncodingException => {
        Log.e(TAG, e.toString)
        return null
      }
      case e: IOException => {
        Log.e(TAG, e.toString)
        return null
      }
      case e: URISyntaxException => {
        Log.e(TAG, e.toString)
        return null
      }
    }
    try {
      var json_response: JSONObject = null
      json_response = new JSONTokener(tmps).nextValue.asInstanceOf[JSONObject]
      var suggestions: JSONArray = json_response.getJSONArray("results")
      var l: Int = suggestions.length
      var result: Array[String] = new Array[String](l)
      var i: Int = 0
      while (i < l) {
        {
          var suggestion: JSONObject = null
          suggestion = suggestions.getJSONObject(i)
          result(i) = suggestion.getString("singleword")
        }
        ({
          i += 1; i
        })
      }
      return result
    }
    catch {
      case e: JSONException => {
        Log.e(TAG, e.toString)
        return null
      }
    }
  }

  def describeContents: Int = {
    return 0
  }

  def writeToParcel(arg0: Parcel, arg1: Int): Unit = {
  }
}

