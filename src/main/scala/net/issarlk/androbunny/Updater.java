package net.issarlk.androbunny;

import java.io.IOException;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.ClientProtocolException;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

public final class Updater implements Runnable {
	private static final String TAG = "Updater";
	private static final Pattern versionPattern = Pattern.compile("^Version: (.*)$", Pattern.MULTILINE);
	private static final Pattern changesPattern = Pattern.compile("^Changes: (.*)$", Pattern.MULTILINE);
	
	public void run() {
		//Retrieve informations on the last version available
		String version_info = null;
		try {
			version_info = ABIO.it().readUri(URI.create("http://www.issarlk.net/androbunny_version.txt"));
		} catch (ClientProtocolException e) {
			Log.e(TAG, "Couldn't get updated version info: " + e.toString());
			return;
		} catch (IOException e) {
			Log.e(TAG, "Couldn't get updated version info: " + e.toString());
			return;
		}
		//Extract info from it
		int version = 0;
		Matcher match = versionPattern.matcher(version_info);
		if (match.find()) {
			version = Integer.parseInt(match.group(1));
		}
		String changes = "";
		match = changesPattern.matcher(version_info);
		if (match.find()) {
			changes = match.group(1);
		}
		//Compare to version code
		PackageInfo pinfo;
		try {
			pinfo = AndrobunnyAppSingleton.androbunnyapp.getPackageManager().getPackageInfo(AndrobunnyAppSingleton.androbunnyapp.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			Log.e(TAG, "Couldn't read version code. Error: " + e.toString());
			return;
		}
		int versionNumber = pinfo.versionCode;
		if (versionNumber < version) {
			//An update is available
			AndrobunnyAppSingleton.androbunnyapp.upgrade_available = true;
			AndrobunnyAppSingleton.androbunnyapp.upgrade_changes = changes;
		}
	}
}
