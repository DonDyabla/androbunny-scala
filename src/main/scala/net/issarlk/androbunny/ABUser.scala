package net.issarlk.androbunny

import android.content.SharedPreferences
import android.preference.PreferenceManager
import inkbunny.{InkbunnyAPIException, API}
import java.util.Observable

/**
 * Created by IntelliJ IDEA.
 * User: issarlk
 * Date: 12/24/11
 * Time: 9:06 PM
 * To change this template use File | Settings | File Templates.
 */

object ABUser extends Observable {
  private val TAG = "ABUser"
  var name: Option[String] = None
  def setName(value: String) {
    name = Some(value)
    setChanged()
    notifyObservers(value)
  }
  def noName() {
    name = None
    setChanged()
    notifyObservers()
  }
  
  def it = this

  def isLoggedIn(): Boolean = name != None

  def isGuest(): Boolean = name == Some("guest")

  @throws(classOf[InkbunnyAPIException])
  def loginAsGuest() = login("guest", "")

  @throws(classOf[InkbunnyAPIException])
  def login(username: String, password: String): Unit = {
    API.login(username, password)
    Preference.setString("username", username)
    Preference.setString("sid", API.sid.get)
    Preference.setString("ratingsmask", API.ratingsmask)
    setName(username)
  }

  @throws(classOf[InkbunnyAPIException])
  def logout: Unit = {
    Log.d(TAG, "Logging in as guest")
    try {
      this.login("guest", "")
    }
    catch {
      case e: InkbunnyAPIException => {
        API.logout
        throw e
      }
    }
  }

  def checkLoggedInAs(userName: String) {
    //Check whether we are logged in or not.
    if (userName != "") {
      try {
        if (API.isLoggedIn) {
          //Logged in
          setName(userName)
        } else {
          noName()
          //Try to login as guest
          ABUser.it.loginAsGuest
        }
      } catch {
        case e: InkbunnyAPIException => {
          Log.e(TAG, e.toString)
        }
      }
    } else {
      noName()
      try {
        ABUser.it.loginAsGuest
      } catch {
        case e: InkbunnyAPIException => {
          Log.e(TAG, e.toString)
        }
      }
    }
  }
}