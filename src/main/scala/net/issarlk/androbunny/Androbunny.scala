/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.

*/

package net.issarlk.androbunny

import activity.{UserActivity, UserActivityModel}
import activity.ProgressActivity
import activity.{SubmissionActivity, SubmissionActivityModel}
import activity.{ListActivity, ListActivityModel}
import inkbunny.{API, User, Submission, Search}
import scala.actors.Actor
import scala.actors.Actor._
import scala.Some
import java.net.URI
import android.content.{SharedPreferences, Context}
import android.widget.ImageView
import utils.SimpleObservable
import java.util.Vector
import android.content.SharedPreferences.OnSharedPreferenceChangeListener


case class DoShowSubmission(context: Context, id: Int)
case class DoShowUserPage(context: Context,  user: User)
case class DoShowListPage(context: Context,  search: Search)

object Androbunny extends Actor {
  private var submission_activity_model: SubmissionActivityModel = null
  private var user_activity_model: UserActivityModel = null
  private var list_activity_model: ListActivityModel = null
  val BASE_URI: URI = new URI("https://inkbunny.net/")  
  var scale = 1.0
  def setScale(arg: Double):Unit = scale = arg

  def maybe[T](item: T): Option[T] = {
    item match {
      case null => None
      case i: T =>
        Some[T](i)
    }
  }

  def get_submission_activity_model: Option[SubmissionActivityModel] = {
    submission_activity_model match {
      case null => None
      case s =>
        submission_activity_model = null
        Some[SubmissionActivityModel](s)
    }
  }

  def get_user_activity_model: Option[UserActivityModel] = {
    user_activity_model match {
      case null => None
      case u =>
        user_activity_model = null
        Some[UserActivityModel](u)
    }
  }

  def get_list_activity_model: Option[ListActivityModel] = {
    list_activity_model match {
      case null => None
      case s =>
        list_activity_model = null
        Some[ListActivityModel](s)
    }
  }

  def it = this

  def handler = AndrobunnyAppSingleton.androbunnyapp.handler

  def runInBackground(task: () => Unit, urgent: Boolean = false) {
    val worker = urgent match {
      case true  =>
        AndrobunnyAppSingleton.androbunnyapp.urgentWorker
      case false =>
        AndrobunnyAppSingleton.androbunnyapp.worker
    }
    AndrobunnyAppSingleton.androbunnyapp.worker.execute(new Runnable {
      def run: Unit = {
        task()
      }
    })
  }

  def dp_to_pixel(dp: Int):Int = (dp.toDouble * scale).toInt

  //Download an image into an Imageview
  def createDownloadIntoImageViewTask(i: ImageView, argid: Int, url: URI, argw: Int, argh: Int, docache: Boolean, cacheScaled: Boolean, filter: BitmapFilter): Runnable = {
    return (new ABIO.DownloadIntoImageViewTask(i, argid, url, argw, argh, docache, cacheScaled, filter))
  }

  def createDownloadIntoImageViewTask(i: ImageView, url: URI, argw: Int, argh: Int, docache: Boolean, cacheScaled: Boolean, filter: BitmapFilter): Runnable = {
    return (new ABIO.DownloadIntoImageViewTask(i, -1, url, argw, argh, docache, cacheScaled, filter))
  }

  def act() {
    loop {
      react {
        case DoShowSubmission(context, id) =>
          Log.d("Androbunny", "Showing submission " + id)
          ProgressActivity.start(context)
          //Load submission
          val submission = API.getSubmissionByID(id)
          submission_activity_model = new SubmissionActivityModel(submission)
          submission_activity_model.load
          //Show submission activity
          SubmissionActivity.start(context)
          //Dismiss progress dialog
          ProgressActivity.finish()
        case DoShowUserPage(context, user) =>
          Log.d("Androbunny", "Showing user page of " + user.name)
          ProgressActivity.start(context)
          user_activity_model = new UserActivityModel(user)
          user_activity_model.load
          //Show user activity
          UserActivity.start(context)
          //Dismiss progress dialog
          ProgressActivity.finish()
        case DoShowListPage(context, search) =>
          Log.d("Androbunny", "Showing list page for " + search.description)
          ProgressActivity.start(context)
          list_activity_model = new ListActivityModel(search)
          list_activity_model.load
          //Show user activity
          ListActivity.start(context)
          //Dismiss progress dialog
          ProgressActivity.finish()
      }
    }
  }
}