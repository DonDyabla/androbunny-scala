package net.issarlk.androbunny

import android.graphics.drawable.{BitmapDrawable, Drawable}
import android.text.Html
import java.io.IOException
import org.apache.http.client.ClientProtocolException

/**
 * Created by IntelliJ IDEA.
 * User: issarlk
 * Date: 12/9/11
 * Time: 11:36 PM
 * To change this template use File | Settings | File Templates.
 */

object ABImageGetter extends Html.ImageGetter {
  private val TAG = "ImgGetter"

  def getDrawable(source: String): Drawable = {
    if (source == null) {
      Log.e(TAG, "null source");
      return null
    }
    var tmp: BitmapDrawable = null
    try {
      val uri = Androbunny.BASE_URI.resolve(source)
      val tmp = new BitmapDrawable(ABIO.readUriIntoBitmap(uri, 200, 200))
      tmp.setBounds(0, 0, Androbunny.dp_to_pixel(50), Androbunny.dp_to_pixel(50))
      return tmp
    }
    catch {
      case e: ClientProtocolException => {
        Log.e(TAG, e.toString)
        return null
      }
      case e: IOException => {
        Log.e(TAG, e.toString)
        return null
      }
    }
  }
}