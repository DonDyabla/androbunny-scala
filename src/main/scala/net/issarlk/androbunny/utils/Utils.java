package net.issarlk.androbunny.utils;

public final class Utils {
	public static <T> T[] growArray(T[] in, int newSize) {
		@SuppressWarnings("unchecked")
		T[] result = (T[])new Object[newSize];
		System.arraycopy(in, 0, result, 0, in.length);
		return result;
	}
}
