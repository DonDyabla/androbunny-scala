package net.issarlk.androbunny.utils;

import java.util.Observable;

public class SimpleObservable<T> extends Observable {
	private T value;
	
	public SimpleObservable(T arg) {
		super();
		this.value = arg;
	}
	
	public void set(T newValue) {
		this.value = newValue;
		this.changed();
	}
	
	public T get() {
		return this.value;
	}
	
	public void changed() {
		this.setChanged();
		this.notifyObservers();
	}
}
