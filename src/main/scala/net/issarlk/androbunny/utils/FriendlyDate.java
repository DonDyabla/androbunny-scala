/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.util.Log;

public class FriendlyDate {
	static final String TAG = "FriendlyDate";
	public static String friendly(String arg) {
		//Note: using SSSSSS to parse milliseconds makes java
		//bug and add up to several minutes to the date because it
		//can only handle 3 digit SSS correctly, not 6 digit like IB returns.
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date argDate = f.parse(arg);
			Calendar gmt = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			long now = gmt.getTimeInMillis();
			long then = argDate.getTime();
			//Get the difference in seconds
			long difference = (now - then) / 1000;
			if (difference < 60) {
				return "a moment ago";
			} 
			difference /= 60;
			if (difference < 60) {
				return "" + difference + " minute" + ((difference>1)?"s":"") + " ago";
			}
			difference /= 60;
			if (difference < 24) {
				return "" + difference + " hour" + ((difference>1)?"s":"") + " ago";
			}
			difference /= 24;
			if (difference < 31) {
				return "" + difference + " day" + ((difference>1)?"s":"") + " ago";
			}
			if ((difference / 30) < 12) {
				return "" + difference + " month" + ((difference>1)?"es":"") + " ago";
			}
			return "" + (difference / 365) + " year" + ((difference / 365 > 1)?"s":"") + " ago";
		} catch (ParseException e) {
			Log.e(TAG, "Error parsing date: " + arg + ". Error: " + e.toString());
			return arg;
		}
	}
}
