/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.utils;

public class BBDeCoder {
	public static synchronized String decode(String bbcode) {
		//First escape HTML
		bbcode = bbcode.replaceAll("<", "&lt;");
		bbcode = bbcode.replaceAll(">", "&gt;");
		//BBCode
		bbcode = bbcode.replaceAll("\n", "<br/>");
		bbcode = bbcode.replaceAll("\\[b\\]", "<b>");
		bbcode = bbcode.replaceAll("\\[/b\\]", "</b>");
		bbcode = bbcode.replaceAll("\\[u\\]", "<u>");
		bbcode = bbcode.replaceAll("\\[/u\\]", "</u>");
		bbcode = bbcode.replaceAll("\\[i\\]", "<i>");
		bbcode = bbcode.replaceAll("\\[/i\\]", "</i>");
		bbcode = bbcode.replaceAll("\\[center\\]", "<div align=\"center\">");
		bbcode = bbcode.replaceAll("\\[/center\\]", "</div>");
		bbcode = bbcode.replaceAll("\\[left\\]", "<div align=\"left\">");
		bbcode = bbcode.replaceAll("\\[/left\\]", "</div>");
		bbcode = bbcode.replaceAll("\\[right\\]", "<div align=\"right\">");
		bbcode = bbcode.replaceAll("\\[/right\\]", "</right>");
		bbcode = bbcode.replaceAll("\\[q\\]", "<blockquote>");
		bbcode = bbcode.replaceAll("\\[/q\\]", "</blockquote>");
		bbcode = bbcode.replaceAll("\\[url\\](.*)\\[/url\\]", "<a href=\"$1\">$1</a>");
		bbcode = bbcode.replaceAll("\\[url=(.*)\\](.*)\\[/url\\]", "<a href=\"$1\">$2</a>");
		bbcode = bbcode.replaceAll("\\[color=(.*)\\]", "<font color=\"$1\">");
		bbcode = bbcode.replaceAll("\\[/color\\]", "</font>");
		bbcode = bbcode.replaceAll("\\[code\\](.*)\\[/code\\]", "$1");
		bbcode = bbcode.replaceAll("\\[color=(.*)\\]", "<font color=\"$1\">");
		bbcode = bbcode.replaceAll("\\[icon\\]", "");
		bbcode = bbcode.replaceAll("\\[/icon\\]", "");
		bbcode = bbcode.replaceAll("\\[iconname\\]", "");
		bbcode = bbcode.replaceAll("\\[/iconname\\]", "");
		bbcode = bbcode.replaceAll("@([^ ]+)", "$1");
		bbcode = bbcode.replaceAll("\\[name\\]", "");
		bbcode = bbcode.replaceAll("\\[/name\\]", "");
		return bbcode;
	}
}
