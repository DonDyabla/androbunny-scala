/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/

package net.issarlk.androbunny.utils;

import java.net.URI;
import android.graphics.Bitmap;

public class BitmapCache extends Cache<URIAndSize, Bitmap>{

	public void put(URI argURI, int w, int h, Bitmap value) {
		super.put(new URIAndSize(argURI, w, h), value);
	}
	
	public Bitmap get(URI argURI, int w, int h) {
		return super.get(new URIAndSize(argURI, w, h));
	}
}

