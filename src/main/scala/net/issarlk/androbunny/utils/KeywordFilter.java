package net.issarlk.androbunny.utils;

import android.widget.Filter;
import net.issarlk.androbunny.Androbunny;
import net.issarlk.androbunny.AndrobunnyAppSingleton;
import net.issarlk.androbunny.activity.SearchActivity;
import net.issarlk.androbunny.inkbunny.API;

/**
 * Created by IntelliJ IDEA.
 * User: issarlk
 * Date: 12/23/11
 * Time: 10:08 PM
 * To change this template use File | Settings | File Templates.
 */

public class KeywordFilter extends Filter {
    private SearchActivity.KeywordAdapter adapter;

    public KeywordFilter(SearchActivity.KeywordAdapter arg) {
        adapter = arg;
    }

    @Override
    protected FilterResults performFiltering(CharSequence arg0) {
        //Get keyword suggestions from the API.
        String[] tmp = API.it().getKeywords(arg0.toString());
        FilterResults results = new FilterResults();
        results.values = tmp;
        results.count = tmp.length;
        return results;
    }

    @Override
    protected void publishResults(CharSequence arg0, FilterResults arg1) {
        adapter.setKeywords((String[]) arg1.values);
    }
}
