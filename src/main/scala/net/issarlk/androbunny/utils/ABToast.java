/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.utils;

import net.issarlk.androbunny.AndrobunnyAppSingleton;
import android.widget.Toast;

public class ABToast {
	
	// Make a Toast with the Application's context, to avoid the Toast
	// using the styles of our activities and become a mess with white backgound
	// and black round border
	public static Toast makeText (CharSequence argtext, int duration) {
		return Toast.makeText(AndrobunnyAppSingleton.androbunnyapp, argtext, duration);
	}
}
	
