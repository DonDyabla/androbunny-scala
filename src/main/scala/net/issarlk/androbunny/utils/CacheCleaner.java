package net.issarlk.androbunny.utils;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import net.issarlk.androbunny.Preference;
import net.issarlk.androbunny.AndrobunnyAppSingleton;
import android.util.Log;

public class CacheCleaner extends Thread {
	public volatile boolean stopped = false; 
	private static final String TAG = "CacheCleaner";
	private static class FileTimeComparator implements Comparator<File> {			
		public int compare(File objecta, File objectb) {
			long a, b;
			a = objecta.lastModified();
			b = objectb.lastModified();
			if (a == b) return 0;
			return (a > b)?1:-1;
		}
	}
	
	public void run() {
		//Acces cache directory
		File cachedir = AndrobunnyAppSingleton.androbunnyapp.getCacheDir();
		int maxSize = 10 * 1024 * 1024;
        if (Preference.it().has("cache_size")) {
          maxSize = Integer.parseInt(Preference.it().getString("cache_size")) * 1024 * 1024;
        }
		//List files
		File[] files = cachedir.listFiles();
		HashMap<File, Long> times = new HashMap<File, Long>();
		long total_length = 0;
		for (File file: files) {
			total_length += file.length();
			times.put(file, new Long(file.lastModified()));
		}
		//Sort by last modification time
		Arrays.sort(files, new FileTimeComparator());
		//Remove old files to make space
		int j;
		for (j = files.length - 1 ; j >= 0 ; j--) {
			if (total_length < maxSize) {
				break;
			}
			Log.d(TAG, "Deleting file " + files[j].getName() + " last modified " + files[j].lastModified());
			files[j].delete();
		}
	}
}
