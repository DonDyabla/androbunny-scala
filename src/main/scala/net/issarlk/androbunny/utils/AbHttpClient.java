/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.utils;

import android.content.Context;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;

public class AbHttpClient extends DefaultHttpClient {
	final Context context;
	
	public AbHttpClient(Context context) {
		this.context = context;
	}

  	protected ClientConnectionManager createClientConnectionManager() {
	  	SchemeRegistry registry = new SchemeRegistry();
	  	registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	  	registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
	  	return new ThreadSafeClientConnManager(getParams(), registry);
  	}
}