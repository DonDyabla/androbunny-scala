/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/

package net.issarlk.androbunny.utils;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.WeakHashMap;

public class Cache<K, V> {
	private HashMap<K,SoftReference<V>> map;
	private WeakHashMap<V, K> vtok_map;
	private ReferenceQueue<V> refQueue;
	
	private void deQueue() {
		Reference<? extends V> tmp;
		while ((tmp = this.refQueue.poll()) != null) {
			K tmp_key = this.vtok_map.get(tmp);
			if (tmp_key != null) {
				//Remove key
				this.map.remove(tmp_key);
				this.vtok_map.remove(tmp);
			}
		}
	}
	
	public Cache() {
		this.map = new HashMap<K, SoftReference<V>>();
		this.vtok_map = new WeakHashMap<V, K>();
		this.refQueue = new ReferenceQueue<V>();
	}
	
	public void put(K key, V value) {
		this.map.put(key, new SoftReference<V>(value));
		this.vtok_map.put(value, key);
	}
	
	public V get(K key) {
		SoftReference<V> tmpref = this.map.get(key);
		//Use this call to clean old references
		deQueue();
		if (tmpref != null) {
			return tmpref.get();
		} else {
			return null;
		}
	}
}
