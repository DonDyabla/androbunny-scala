/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import android.os.Bundle
import android.app.{Activity, ProgressDialog, Dialog}
import net.issarlk.androbunny.{TR, R}
import android.content.{DialogInterface, Context, Intent}
import ref.WeakReference

final object ProgressActivity {
  var current: WeakReference[ProgressActivity] = null
  
  def start(context: Context): Unit = {
    var i: Intent = new Intent(context, classOf[ProgressActivity])
    context.startActivity(i)
  }

  def finish() {
    //Finish old dialog if any
    if (current != null) {
      current.get match {
        case Some(tmp) =>
          tmp.finish()
          current = null
        case None =>
      }
    }
  }

  private[activity] final val TAG: String = "ProgressActivity"
  private[activity] final val PROGRESS_ID: Int = 1
}

final class ProgressActivity extends AndrobunnyActivity with DialogInterface.OnCancelListener {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    //Finish old dialog if any
    ProgressActivity.finish();
    //Record oneself in singleton
    ProgressActivity.current = new WeakReference[ProgressActivity](this)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.progress)
    showDialog(ProgressActivity.PROGRESS_ID)
  }
  
  override def onPause() {
    super.onPause();
    this.finish();
  }
  
  override def finish() {
    super.finish()
  }
  
  def onCancel(dialog: DialogInterface): Unit = {
    this.finish()
  }
  
  override def onCreateDialog(id: Int): Dialog = {
    if (id == ProgressActivity.PROGRESS_ID) {
      val tmpDialog: ProgressDialog = new ProgressDialog(this);
      tmpDialog.setMessage("Please wait...");
      tmpDialog.setIndeterminate(true);
      tmpDialog.setCancelable(true);
      tmpDialog.setOnCancelListener(this)
      return tmpDialog;
    }
    return super.onCreateDialog(id)
  }
}

