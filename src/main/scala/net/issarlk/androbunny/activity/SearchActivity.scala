/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import java.util.Collections
import java.util.HashSet
import java.util.Set
import org.acra.ErrorReporter
import net.issarlk.androbunny.inkbunny.API
import net.issarlk.androbunny.inkbunny.InkbunnyAPIException
import net.issarlk.androbunny.inkbunny.Search
import net.issarlk.androbunny.TR
import net.issarlk.androbunny.TypedResource._
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.database.DataSetObserver
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.Filter
import android.widget.Filter.FilterResults
import android.widget.Filterable
import android.widget.ListAdapter
import android.widget.MultiAutoCompleteTextView
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import net.issarlk.androbunny.activity.SearchActivity.KeywordAdapter
import net.issarlk.androbunny.utils.{KeywordFilter, ABToast}
import net.issarlk.androbunny._

final object SearchActivity {
  def start(context: Activity): Unit = {
    var intent: Intent = new Intent(context, classOf[SearchActivity])
    context.startActivity(intent)
  }

  private final val TAG: String = "SearchActivity"

  class KeywordAdapter(context: Context, val layout: Int) extends ArrayAdapter[String](context, layout) with ListAdapter with Filterable {

    override def getCount: Int = keywords.length

    override def getItem(arg0: Int): String = {
      var tmp: Array[String] = keywords
      if (arg0 < tmp.length) {
        return tmp(arg0)
      }
      else {
        return null
      }
    }

    override def getItemId(arg0: Int): Long = arg0

    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      var v: TextView = convertView.asInstanceOf[TextView]
      if (v == null) {
        v = new TextView(getContext)
      }
      v.setText(keywords(position))
      return v
    }

    override def hasStableIds: Boolean = false

    override def isEmpty: Boolean = this.keywords.length == 0

    override def registerDataSetObserver(arg0: DataSetObserver): Unit = {
      observers.add(arg0)
    }

    override def unregisterDataSetObserver(arg0: DataSetObserver): Unit = {
      observers.remove(arg0)
    }

    override def getFilter: Filter = filter

    override def areAllItemsEnabled: Boolean = true

    override def isEnabled(arg0: Int): Boolean = true

    private final val filter: KeywordFilter = new KeywordFilter(this)
    var keywords: Array[String] = new Array[String](0)
    def setKeywords(arg: Array[String]): Unit = {
      keywords = arg
      notifyDataSetChanged()
    }
    private[activity] var observers: Set[DataSetObserver] = Collections.synchronizedSet(new HashSet[DataSetObserver])
  }
}

final class SearchActivity extends AndrobunnyActivity with android.view.View.OnClickListener with android.widget.CompoundButton.OnCheckedChangeListener {
  /**Called when the activity is first created. */
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS)
    setupUI
    if (savedInstanceState != null) {
      this.searchTextView.setText(savedInstanceState.getString("search_text"))
      this.search_keywords.setChecked(savedInstanceState.getBoolean("search_keywords"))
      this.search_titles.setChecked(savedInstanceState.getBoolean("search_titles"))
      this.search_descriptions.setChecked(savedInstanceState.getBoolean("search_descriptions"))
      this.time_range.onRestoreInstanceState(savedInstanceState.getParcelable("time_range"))
    }
    this.onCheckedChanged(null, true)
  }

  override def onSaveInstanceState(savedInstanceState: Bundle): Unit = {
    super.onSaveInstanceState(savedInstanceState)
    savedInstanceState.putString("search_text", this.searchTextView.getText.toString)
    savedInstanceState.putBoolean("search_keywords", this.search_keywords.isChecked)
    savedInstanceState.putBoolean("search_titles", this.search_titles.isChecked)
    savedInstanceState.putBoolean("search_descriptions", this.search_descriptions.isChecked)
    savedInstanceState.putParcelable("time_range", this.time_range.onSaveInstanceState)
  }

  override def onConfigurationChanged(newConfig: Configuration): Unit = {
    Log.d(SearchActivity.TAG, "Config change")
    super.onConfigurationChanged(newConfig)
    setupUI
  }

  private def setupUI: Unit = {
    setContentView(R.layout.search)
    var adapter: ArrayAdapter[String] = new SearchActivity.KeywordAdapter(this, android.R.layout.simple_dropdown_item_1line)
    searchTextView = this.findView(TR.search_text)
    searchTextView.setAdapter(adapter)
    searchTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer)
    search_button = this.findView(TR.search_button)
    search_button.setOnClickListener(this)
    search_keywords = this.findView(TR.search_keywords)
    search_keywords.setOnCheckedChangeListener(this)
    search_titles = this.findView(TR.search_titles)
    search_titles.setOnCheckedChangeListener(this)
    search_descriptions = this.findView(TR.search_descriptions)
    search_descriptions.setOnCheckedChangeListener(this)
    time_range = this.findView(TR.search_time_range)
    time_range.setVisibility(View.GONE)
  }

  def onClick(arg0: View): Unit = {
    if (arg0 eq search_button) {
      try {
        var fields: Int = 0
        if (search_keywords.isChecked) {
          fields |= API.SEARCH_KEYWORDS
        }
        if (search_titles.isChecked) {
          fields |= API.SEARCH_TITLES
        }
        if (search_descriptions.isChecked) {
          fields |= API.SEARCH_DESCRIPTIONS
        }
        var dayslimit: Int = -1
        val time_range_selected: Int = time_range.getSelectedItemPosition
        if (time_range_selected != AdapterView.INVALID_POSITION && time_range_selected != 0) {
          time_range_selected match {
            case 1 =>
              dayslimit = 1
            case 2 =>
              dayslimit = 3
            case 3 =>
              dayslimit = 7
            case 4 =>
              dayslimit = 14
            case 5 =>
              dayslimit = 30
            case 6 =>
              dayslimit = 90
            case 7 =>
              dayslimit = 270
            case _ =>
              ErrorReporter.getInstance.handleException(new Exception("Unknown time range position: " + time_range_selected))
          }
        }
        var search: Search = API.searchText(this.searchTextView.getText.toString, fields, dayslimit)
        Androbunny ! DoShowListPage(this, search)
      }
      catch {
        case e: InkbunnyAPIException => {
          ABToast.makeText("Couldn't search.", Toast.LENGTH_LONG)
          return
        }
      }
    }
  }

  def onCheckedChanged(arg0: CompoundButton, arg1: Boolean): Unit = {
    if (!(this.search_descriptions.isChecked || this.search_titles.isChecked)) {
      this.search_keywords.setChecked(true)
      this.search_keywords.setEnabled(false)
    }
    else {
      this.search_keywords.setEnabled(true)
    }
  }

  private var searchTextView: MultiAutoCompleteTextView = null
  private var search_keywords: CheckBox = null
  private var search_titles: CheckBox = null
  private var search_descriptions: CheckBox = null
  private var search_button: Button = null
  private var time_range: Spinner = null
}


