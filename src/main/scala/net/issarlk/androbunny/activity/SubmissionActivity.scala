/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import java.io.IOException
import java.net.URI
import java.util.concurrent.RejectedExecutionException
import org.acra.ErrorReporter
import com.pocketjourney.media.StreamingMediaPlayer
import net.issarlk.androbunny.inkbunny.File
import net.issarlk.androbunny.inkbunny.Icon
import net.issarlk.androbunny.inkbunny.Submission
import net.issarlk.androbunny.inkbunny.Thumbnailable
import net.issarlk.androbunny.utils.ABToast
import net.issarlk.androbunny.utils.FriendlyDate
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.text.Spanned
import android.util.DisplayMetrics
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.Gallery
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import net.issarlk.androbunny._

final object SubmissionActivity {
  private final val TAG: String = "SubmissionActivity"

  def start(context: Context): Unit = {
    val intent = new Intent(context, classOf[SubmissionActivity])
    context.startActivity(intent)
  }
}

final class SubmissionActivity extends AndrobunnyActivity with View.OnClickListener {
  private var model: SubmissionActivityModel = null

  /**Called when the activity is first created. */
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS)
    setupUI
    //Androbunny has the model
    Androbunny.get_submission_activity_model match {
      case None =>
        ABToast.makeText("No submission provided!", Toast.LENGTH_LONG)
        this.finish
        return
      case Some(m) => model = m
    }    
    ErrorReporter.getInstance.putCustomData("submission", "" + model.submission.id)
    if (model.isEmpty) {
      ABToast.makeText("This submission is empty!", Toast.LENGTH_LONG).show  
    }
  }

  override def onStart: Unit = {
    super.onStart
    setupContent
    refreshFileDisplay
  }

  protected override def onStop: Unit = {
    super.onStop
    if (this.audioStreamer != null) {
      this.audioStreamer.interrupt
    }
    Preference.ratings.deleteObserver(this)
    /*if (this.imageBitmap != null) {
      this.imageBitmap.recycle
    }*/
    setupUI
  }

  override def onConfigurationChanged(newConfig: Configuration): Unit = {
    Log.d(SubmissionActivity.TAG, "Config change")
    super.onConfigurationChanged(newConfig)
    setupUI
    setupContent
    refreshFileDisplay
  }

  /*override def update(observable: Observable, value: AnyRef): Unit = {
    super.update(observable, value)
    if (observable eq AndrobunnyAppSingleton.androbunnyapp.ratings) {
      refreshFileDisplay
    }
  }    */

  private def refreshFileDisplay: Unit = {
    if (model.isEmpty) {
      return
    }
    var file: File = model.getCurrentFile
    if (file.isImage) {              
      //if (!file.mimetype.startsWith("audio/")) {
      setupImage
      this.image.setVisibility(View.VISIBLE)
    }
    if (model.hasWriting) {
      this.writing.setText(model.writing)
      this.writing.setVisibility(View.VISIBLE)
    }
    /*if (model.submission.files.length > 1) {
      this.imageAdapter = new SubmissionActivity.ImageAdapter(this)
      this.gallery.setAdapter(this.imageAdapter)
      this.gallery.setSelection(this.currentFile)
      this.gallery.setVisibility(View.VISIBLE)
      gallery.setOnItemClickListener(this)
    } */
  }

  private def setupUI: Unit = {
    setContentView(R.layout.submissionactivity)
    this.gallery = findViewById(R.id.files_gallery).asInstanceOf[Gallery]
    this.image = findViewById(R.id.submission_image_view).asInstanceOf[ImageView]
    this.mediaplayerlayout = findViewById(R.id.mediaplayer).asInstanceOf[LinearLayout]
    this.writing = findViewById(R.id.submission_writing).asInstanceOf[TextView]
    val viewOnIBButton: Button = findViewById(R.id.submission_view_on_ib).asInstanceOf[Button]
    viewOnIBButton.setOnClickListener(this)
  }

  private def setupContent: Unit = {
    this.setupAudio
    this.setupAuthorIcon
    var t: TextView = findViewById(R.id.title).asInstanceOf[TextView]
    t.setText(model.submission.title + " " + this.getResources.getText(R.string.by_author) + " " + model.submission.user.name)
    var description: TextView = findViewById(R.id.description).asInstanceOf[TextView]
    description.setText(Html.fromHtml(model.description, ABImageGetter, null))
    description.setMovementMethod(LinkMovementMethod.getInstance)
    var details: TextView = findViewById(R.id.submission_details).asInstanceOf[TextView]
    details.setText(model.details)
    var keywords: TextView = findViewById(R.id.submission_keywords).asInstanceOf[TextView]
    keywords.setText(model.keywords)
    var stats: TextView = findViewById(R.id.submission_stats).asInstanceOf[TextView]
    stats.setText(model.statistics)
  }

  private def setupAuthorIcon: Unit = {
    var authorIcon: ImageView = findViewById(R.id.author_icon).asInstanceOf[ImageView]
    try {
      var ic: Icon = model.submission.user.getLargestIcon
      if (ic != null) {
        Androbunny.runInBackground(
          task = () => {
            Androbunny.createDownloadIntoImageViewTask(
              authorIcon,
              ic.url,
              Androbunny.dp_to_pixel(90),
              Androbunny.dp_to_pixel(90),
              true,
              true,
              UnitBitmapFilter).run
          }
        )
      }
      else {
        authorIcon.setImageResource(R.drawable.noicon)
      }
    }
    catch {
      case ex: RejectedExecutionException => {
        authorIcon.setImageResource(R.drawable.noicon)
      }
    }
    authorIcon.setOnClickListener(this)
  }

  private def setupAudio: Unit = {
    var file: File = model.getCurrentFile
    if (!file.isAudio) {
      return
    }
    button_play = findViewById(R.id.button_play).asInstanceOf[ImageButton]
    audioStreamer = new StreamingMediaPlayer(this, button_play)
    button_play.setOnClickListener(this)
    mediaplayerlayout.setVisibility(View.VISIBLE)
    audioStreamer.startStreaming(model.getCurrentFile.file_urls(Thumbnailable.FULL).toString, 1, 1)
  }

  private def setupImage: Unit = {
    try {
      var metrics: DisplayMetrics = new DisplayMetrics
      getWindowManager.getDefaultDisplay.getMetrics(metrics)
      Log.d(SubmissionActivity.TAG, "Width: " + metrics.widthPixels)
      setProgressBarIndeterminateVisibility(true)
      if (model.getCurrentFile().getLargerImage(metrics.widthPixels, 0) != null) {
        AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(new Runnable {
          def run: Unit = {
            var metrics: DisplayMetrics = new DisplayMetrics
            getWindowManager.getDefaultDisplay.getMetrics(metrics)
            Log.d(SubmissionActivity.TAG, "Width2: " + metrics.widthPixels)
            System.gc
            try {
              var url: URI = SubmissionActivity.this.model.getCurrentFile.getLargerImage(metrics.widthPixels, 0)
              if (url == null) {
                SubmissionActivity.this.runOnUiThread(new Runnable {
                  def run: Unit = {
                    ABToast.makeText("Sorry, no picture was found in the submission.", Toast.LENGTH_LONG).show
                    SubmissionActivity.this.setProgressBarIndeterminateVisibility(false)
                  }
                })
                ErrorReporter.getInstance.handleSilentException(new Exception("No picture in submission " + SubmissionActivity.this.model.submission.id))
                return
              }
              val tmpBitmap = ABIO.downloadBitmap(url, metrics.widthPixels, 0, true, false)
              SubmissionActivity.this.runOnUiThread(new Runnable {
                def run: Unit = {
                  SubmissionActivity.this.image.setImageBitmap(tmpBitmap)
                  SubmissionActivity.this.setProgressBarIndeterminateVisibility(false)
                }
              })
            }
            catch {
              case e: OutOfMemoryError => {
                System.gc
                SubmissionActivity.this.runOnUiThread(new Runnable {
                  def run: Unit = {
                    ABToast.makeText("Sorry, there is not enough memory to display picture.", Toast.LENGTH_LONG).show
                  }
                })
                SubmissionActivity.this.finish
              }
              case e: IOException => {
                ABToast.makeText("An error occured while getting picture from Inkbunny. Please try again.", Toast.LENGTH_SHORT).show
                SubmissionActivity.this.finish
              }
            }
          }
        })
      }
      else {
        this.image.setImageResource(R.drawable.noicon)
      }
    }
    catch {
      case ex: RejectedExecutionException => {
        this.image.setImageResource(R.drawable.noicon)
      }
    }
  }

  def onClick(view: View): Unit = {
    if (view.getId == R.id.button_play) {
      if (this.audioStreamer.getMediaPlayer.isPlaying) {
        this.audioStreamer.getMediaPlayer.pause
        this.button_play.setImageResource(R.drawable.button_play)
      }
      else {
        this.audioStreamer.getMediaPlayer.start
        this.button_play.setImageResource(R.drawable.button_pause)
      }
    }
    else if (view.getId == R.id.submission_view_on_ib) {
      var intent: Intent = new Intent("android.intent.action.VIEW", Uri.parse("https://inkbunny.net/submissionview.php?id=" + model.submission.getID))
      startActivity(intent)
    }
    else if (view.getId == R.id.author_icon) {
      Androbunny ! DoShowUserPage(this, model.submission.user)
    }
  }

  private var audioStreamer: StreamingMediaPlayer = null
  private var button_play: ImageButton = null
  private var mediaplayerlayout: LinearLayout = null
  private var image: ImageView = null
  private var gallery: Gallery = null
  private var writing: TextView = null
}

final object SubmissionActivityModel {
  private final val TAG: String = "SubmissionActivityModel"
}

final class SubmissionActivityModel(val submission: Submission) {
  private var currentFile: Int = 0
  var description: String = ""
  var details: String = ""
  var keywords: String = ""
  var statistics: String = ""

  def isEmpty: Boolean = {
    submission.files.length == 0
  }

  def hasWriting: Boolean = {
    (this.submission.writing != null) && (this.submission.writing ne "")
  }

  def writing: Spanned = {
    assert(hasWriting, Log.e(SubmissionActivityModel.TAG, "No writing available"))
    Html.fromHtml(submission.writing)
  }
  
  def getCurrentFile(): File = {
    assert(!isEmpty, Log.e(SubmissionActivityModel.TAG, "No current file exist, there are no files"))
    submission.files(currentFile)
  }
  
  def load: Unit = {
    description = ABTextProcessor.process(submission.description)
    details = "Type:\n" +
      this.submission.submission_type.name + "\n" +
      "Rating:\n" +
      this.submission.rating.name + "\n" +
      "Published:\n" +
      FriendlyDate.friendly(this.submission.create_datetime)
    var tmp = ""
    for (k <- this.submission.keywords) {
      tmp += k.name + "\n"
    }
    keywords = tmp
    statistics = "" +
      this.submission.favorites_count + " favorite" + (if ((this.submission.favorites_count > 1)) "s" else "") + "\n" +
      this.submission.comments_count + " comment" + (if ((this.submission.comments_count > 1)) "s" else "") + "\n"
  }
}
