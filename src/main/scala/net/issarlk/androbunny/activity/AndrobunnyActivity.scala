/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.net.URI
import java.util.Observable
import java.util.Observer
import org.apache.http.client.ClientProtocolException
import net.issarlk.androbunny.utils.ABToast
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import android.view.{View, Menu, MenuInflater, MenuItem}
import scala.Some
import net.issarlk.androbunny.inkbunny.{API, InkbunnyAPIException}
import net.issarlk.androbunny._

class AndrobunnyActivity extends Activity with Observer {
  def runOnUiThread(f: () => Unit): Unit = {
    runOnUiThread(new Runnable {
      def run = f()
    })
  }
  
  protected override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    if (AndrobunnyAppSingleton.androbunnyapp.upgrade_available && !AndrobunnyAppSingleton.androbunnyapp.refused_upgrade) {
      Log.d("upgrade", "show upgrade dialog")
      var builder: AlertDialog.Builder = new AlertDialog.Builder(this)
      builder.setMessage(this.getString(R.string.upgrade_prompt) + "\n" + AndrobunnyAppSingleton.androbunnyapp.upgrade_changes).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener {
        def onClick(dialog: DialogInterface, id: Int): Unit = {
          AndrobunnyAppSingleton.androbunnyapp.worker.execute(new Runnable {
            def run: Unit = {
              var stream: FileOutputStream = null
              try {
                stream = AndrobunnyActivity.this.openFileOutput("androbunny.apk", Context.MODE_WORLD_READABLE)
              }
              catch {
                case e: FileNotFoundException => {
                  val ex: FileNotFoundException = e
                  AndrobunnyActivity.this.runOnUiThread(new Runnable {
                    def run: Unit = {
                      ABToast.makeText("The upgrade couldn't be downloaded.\nError: " + ex.toString, Toast.LENGTH_LONG)
                    }
                  })
                  return
                }
              }
              try {
                ABIO.readUriIntoStream(URI.create("http://www.issarlk.net/androbunny.apk"), stream)
                stream.close
              }
              catch {
                case e: ClientProtocolException => {
                  val ex: ClientProtocolException = e
                  AndrobunnyActivity.this.runOnUiThread(new Runnable {
                    def run: Unit = {
                      ABToast.makeText("The upgrade couldn't be downloaded.\nError: " + ex.toString, Toast.LENGTH_LONG)
                    }
                  })
                  return
                }
                case e: IOException => {
                  val ex: IOException = e
                  AndrobunnyActivity.this.runOnUiThread(new Runnable {
                    def run: Unit = {
                      ABToast.makeText("The upgrade couldn't be downloaded.\nError: " + ex.toString, Toast.LENGTH_LONG)
                    }
                  })
                  return
                }
              }
              var intent: Intent = new Intent(Intent.ACTION_VIEW)
              intent.setDataAndType(Uri.fromFile(new File(AndrobunnyActivity.this.getFilesDir, "androbunny.apk")), "application/vnd.android.package-archive")
              startActivity(intent)
            }
          })
        }
      }).setNegativeButton("No", new DialogInterface.OnClickListener {
        def onClick(dialog: DialogInterface, id: Int): Unit = {
          dialog.cancel
          AndrobunnyAppSingleton.androbunnyapp.refused_upgrade = true
        }
      })
      var alert: AlertDialog = builder.create
      alert.show
    }
  }

  protected override def onStart: Unit = {
    super.onStart
    ABUser.addObserver(this)
  }

  protected override def onStop: Unit = {
    ABUser.deleteObserver(this)
    super.onStop
  }

  override def onPrepareOptionsMenu(menu: Menu): Boolean = {
    menu.findItem(R.id.search).setVisible(API.sid.get != null)
    menu.findItem(R.id.logout).setVisible(ABUser.isLoggedIn() && !ABUser.isGuest())
    menu.findItem(R.id.login).setVisible(!ABUser.isLoggedIn() || ABUser.isGuest())
    return super.onPrepareOptionsMenu(menu)
  }

  override def onCreateOptionsMenu(menu: Menu): Boolean = {
    var inflater: MenuInflater = getMenuInflater
    inflater.inflate(R.menu.main_menu, menu)
    return super.onCreateOptionsMenu(menu)
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.search =>
        SearchActivity.start(this)
      case R.id.settings =>
        PreferencesActivity.start(this)
      case R.id.about =>
        AboutActivity.start(this)
      case R.id.logout =>
        try {
          ABUser.logout
        }
        catch {
          case e: InkbunnyAPIException => {
            ABToast.makeText("Couldn't log back in as guest: " + e.toString, Toast.LENGTH_LONG).show
          }
        }
      case R.id.login =>
        LoginActivity.start(this)
    }
    return true
  }

  def update(observable: Observable, value: AnyRef): Unit = {
    if (observable eq ABUser) {
      ABUser.name match {
        case None => //Do nothing
        case Some(name) =>
          //Greet user
          this.runOnUiThread(() => {
            ABToast.makeText("Welcome " + name, Toast.LENGTH_LONG).show
            }
          )
      }
    }
  }

  class ViewWithOnClick(view : View) {
    def onClick(action : View => Any) = {
      view.setOnClickListener(new View.OnClickListener() {
        def onClick(v : View) { action(v) }
      })
    }
  }

  implicit def addOnClickToViews(view : View) =
    new ViewWithOnClick(view)

  def lastNonConfigurationInstance[T]: Option[T] =
    getLastNonConfigurationInstance match {
      case null =>
        None
      case value => Some[T](value.asInstanceOf[T])
    }
}

