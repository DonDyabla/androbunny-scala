/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import java.util.regex.Matcher
import java.util.regex.Pattern
import org.acra.ErrorReporter
import net.issarlk.androbunny.ThumbnailAdapter.ViewHolder
import net.issarlk.androbunny.inkbunny.InkbunnyAPIException
import net.issarlk.androbunny.inkbunny.Search
import net.issarlk.androbunny.utils.ABToast
import net.issarlk.androbunny.TR
import net.issarlk.androbunny.TypedResource._
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.Context
import android.content.res.Configuration
import android.database.DataSetObserver
import android.net.Uri
import android.os.Bundle
import android.widget.AdapterView
import android.widget.Toast
import android.app.ProgressDialog
import android.widget.AdapterView.OnItemClickListener
import android.widget.GridView
import android.view.View
import net.issarlk.androbunny._

final object ListActivity {
  private val TAG: String = "ListActivity"
  private val POOL_ID: Pattern = Pattern.compile("net.issarlk.androbunny.pool://([0-9]+)", Pattern.CASE_INSENSITIVE)

  def start(context: Context): Unit = {
    var intent: Intent = new Intent(context, classOf[ListActivity])
    context.startActivity(intent)
  }
}

final class ListActivity extends AndrobunnyActivity with OnItemClickListener {
  private var model: ListActivityModel = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    Androbunny.get_list_activity_model match {
      case None =>
        ABToast.makeText("No submissions provided!", Toast.LENGTH_LONG)
        this.finish
        return
      case Some(m) => model = m
    }
    ErrorReporter.getInstance.putCustomData("search", model.search.query)
    setupUI
    setupContent
  }

  override def onStart: Unit = {
    super.onStart
    adapter = new ThumbnailAdapter(this, model.search, true)
    gridView.setAdapter(adapter)
    adapter.registerDataSetObserver(nbOfSubmissionObserver)
    nbOfSubmissionObserver.onChanged
  }

  override def onStop: Unit = {
    adapter.unregisterDataSetObserver(nbOfSubmissionObserver)
    super.onStop
    gridView.setAdapter(null)
    adapter.cleanup
    model.search.freeMemory
  }

  override def onConfigurationChanged(newConfig: Configuration): Unit = {
    super.onConfigurationChanged(newConfig)
    setupUI
    setupContent
    gridView.setAdapter(adapter)
  }

  private def setupUI: Unit = {
    setContentView(R.layout.searchresults)
    gridView = this.findView(TR.thumbnail_view)
    gridView.setOnItemClickListener(this)
  }

  private def setupContent: Unit = {
    setTitle(model.title)
  }

  def onItemClick(parent: AdapterView[_], v: View, position: Int, id: Long): Unit = {
    var holder: ThumbnailAdapter.ViewHolder = v.getTag.asInstanceOf[ThumbnailAdapter.ViewHolder]
    Androbunny ! DoShowSubmission(this, holder.id)
  }

  private var adapter: ThumbnailAdapter = null
  private var gridView: GridView = null
  private final val nbOfSubmissionObserver: NbOfSubmissionObserver = new NbOfSubmissionObserver

  private final class NbOfSubmissionObserver extends DataSetObserver {
    override def onChanged: Unit = {
      Androbunny.runInBackground(
        task = () => {
          val number_of_submission: Int = ListActivity.this.model.search.getNbOfItems
          if (number_of_submission <= 0) {
            ListActivity.this.runOnUiThread(new Runnable {
              def run: Unit = {
                ABToast.makeText("No submission were found.", Toast.LENGTH_LONG).show
              }
            })
          }
        }
      )
    }
  }
}

final class ListActivityModel(val search: Search) {
  def title = search.description
  def load: Unit = {
    search.getNbOfItems
  }
}