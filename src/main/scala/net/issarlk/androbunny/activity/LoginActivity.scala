/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import java.util.Observable
import net.issarlk.androbunny.inkbunny.InkbunnyAPIException
import net.issarlk.androbunny.inkbunny.InkbunnyAPIInvalidLoginException
import net.issarlk.androbunny.utils.ABToast
import android.app.Activity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.DialogInterface.OnCancelListener
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import net.issarlk.androbunny.{ABUser, AndrobunnyAppSingleton, R, Preference}

object LoginActivity {
  def start(context: Activity): Unit = {
    var intent: Intent = new Intent(context, classOf[LoginActivity])
    context.startActivity(intent)
  }
}

class LoginActivity extends AndrobunnyActivity with View.OnClickListener with OnCancelListener {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setupUI
    if (savedInstanceState != null) {
      this.usernameEditText.setText(savedInstanceState.getString("username"))
      this.passwordEditText.setText(savedInstanceState.getString("password"))
    }
    else {
      var username: String = Preference.getString("username")
      if (!("guest" == username)) {
        this.usernameEditText.setText(username)
      }
    }
  }

  override def onResume: Unit = {
    super.onResume
    if (!ABUser.isLoggedIn()) {
      //User is not logged in, show progress bar until she is
      ABUser.addObserver(this)
      setProgressBarIndeterminateVisibility(true)
    }
  }

  override def onPause: Unit = {
    super.onPause
    ABUser.deleteObserver(this)
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    outState.putString("username", this.usernameEditText.getText.toString)
    outState.putString("password", this.passwordEditText.getText.toString)
    super.onSaveInstanceState(outState)
  }

  private def setupUI: Unit = {
    setContentView(R.layout.login)
    this.usernameEditText = findViewById(R.id.username).asInstanceOf[EditText]
    this.passwordEditText = findViewById(R.id.password).asInstanceOf[EditText]
    this.continue_layout = findViewById(R.id.login_continue_layout).asInstanceOf[LinearLayout]
    this.loginButton = findViewById(R.id.login).asInstanceOf[Button]
    this.loginAsGuestButton = findViewById(R.id.login_as_guest).asInstanceOf[Button]
    loginButton.setOnClickListener(this)
    loginAsGuestButton.setOnClickListener(this)
    if (ABUser.isLoggedIn()) {
      updateLoggedInUI(true)
    }
    else {
      updateLoggedInUI(false)
    }
  }

  private def updateLoggedInUI(status: Boolean): Unit = {
    var username: String = Preference.getString("username")
    if (!("guest" == username) && status == true) {
      val continueButton: Button = findViewById(R.id.continue_button).asInstanceOf[Button]
      continueButton.setText("Continue as " + Preference.getString("username"))
      val continue_text: TextView = findViewById(R.id.login_continue_text).asInstanceOf[TextView]
      continue_text.setText("Your are currently logged in as " + Preference.getString("username"))
      continueButton.setOnClickListener(this)
      continue_layout.setVisibility(View.VISIBLE)
    }
    else {
      continue_layout.setVisibility(View.GONE)
    }
  }

  override def update(observable: Observable, value: AnyRef): Unit = {
    super.update(observable, value)
    if (observable eq ABUser) {
      setProgressBarIndeterminateVisibility(false)
      if (ABUser.isLoggedIn()) {
        this.runOnUiThread(() => {
          updateLoggedInUI(true)
        })
      } else {
        this.runOnUiThread(() => {
            updateLoggedInUI(false)
          })
      }
    }
  }

  def onClick(v: View): Unit = {
    if (v.getId == R.id.login) {
      progressDialog = ProgressDialog.show(this, "", "Logging in, please wait...", true, true, this)
      this.loginTask = new LoginTask(usernameEditText.getText.toString, passwordEditText.getText.toString)
      AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(this.loginTask)
    }
    else if (v.getId == R.id.login_as_guest) {
      var username: String = Preference.getString("username")
      if (("guest" == username) && ABUser.isLoggedIn()) {
        this.finish
      }
      else {
        progressDialog = ProgressDialog.show(this, "", "Logging in, please wait...", true, true, this)
        this.loginTask = new LoginTask("guest", "")
        AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(this.loginTask)
      }
    }
    else if (v.getId == R.id.continue_button) {
      this.finish
    }
  }

  def onCancel(dialog: DialogInterface): Unit = {
    this.loginTask.stop = true
    dialog.dismiss
  }

  private var usernameEditText: EditText = null
  private var passwordEditText: EditText = null
  private var continue_layout: LinearLayout = null
  private var loginButton: Button = null
  private var loginAsGuestButton: Button = null
  private var progressDialog: ProgressDialog = null
  private var loginTask: LoginActivity#LoginTask = null

  private class LoginTask(val login: String, val password: String) extends Runnable {

    def run: Unit = {
      if (this.stop) return
      try {
        ABUser.login(this.login, this.password)
        if (this.stop) return
        LoginActivity.this.finish
      }
      catch {
        case e: InkbunnyAPIInvalidLoginException => {
          if (this.stop) return
          runOnUiThread(new Runnable {
            def run: Unit = {
              progressDialog.dismiss
              ABToast.makeText("Login unsuccessful. Did you activate API access in your account settings?", Toast.LENGTH_LONG).show
            }
          })
        }
        case e: InkbunnyAPIException => {
          if (this.stop) return
          val exception_text: String = e.getLocalizedMessage
          runOnUiThread(new Runnable {
            def run: Unit = {
              progressDialog.dismiss
              ABToast.makeText("An error occured. Please try again.\nError message: " + exception_text, Toast.LENGTH_LONG).show
            }
          })
        }
      }
      finally {
        runOnUiThread(new Runnable {
          def run: Unit = {
            progressDialog.dismiss
          }
        })
      }
    }

    var stop: Boolean = false
  }

}

