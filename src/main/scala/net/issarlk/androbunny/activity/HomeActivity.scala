/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import java.util.TreeMap
import java.util.Vector
import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import scala.Some
import net.issarlk.androbunny._
import inkbunny.{API, Search, Thumbnailable}

final object HomeActivity {
  def start(context: Activity): Unit = {
    var intent: Intent = new Intent(context, classOf[HomeActivity])
    context.startActivity(intent)
  }

  private[activity] final val TAG: String = "Home"
  final val SEARCH_POPULAR: Int = 0
  final val SEARCH_RECENT: Int = 1
  final val SEARCH_WRITINGS: Int = 2
  final val SEARCH_MUSICS: Int = 3
  final val SEARCH_NEW: Int = 4
}

final class HomeActivity extends AndrobunnyActivity with ThumbnailTable.OnItemClickListener with ThumbnailTable.OnSeeMoreListener {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setupUI
    lastNonConfigurationInstance[TreeMap[Integer, Search]] match {
      case Some(saved_searches) =>
        Log.d(HomeActivity.TAG, "Getting " + saved_searches.size + " saved searches.")
        searches.putAll(saved_searches)
      case None =>
        setupSearches
    }
  }

  override def onStart: Unit = {
    super.onStart
    setupSearches
    fillThumbnailTables
  }

  override def onStop: Unit = {
    super.onStop
    this.homeLinearLayout.removeAllViews
    import _root_.scala.collection.JavaConversions._
    for (t <- this.tables) {
      t.cleanup
    }
    import _root_.scala.collection.JavaConversions._
    for (s <- this.searches.values) {
      if (s ne this.dontFree) {
        s.freeMemory
      }
      this.dontFree = null
    }
    this.tables.clear
  }

  override def onRetainNonConfigurationInstance: AnyRef = {
    return this.searches
  }

  override def onConfigurationChanged(newConfig: Configuration): Unit = {
    super.onConfigurationChanged(newConfig)
    setupUI
    fillThumbnailTables
  }

  private def setupUI: Unit = {
    setContentView(R.layout.home)
    this.homeLinearLayout = findViewById(R.id.home_linearlayout).asInstanceOf[LinearLayout]
  }

  private def fillThumbnailTables: Unit = {
    this.homeLinearLayout.removeAllViews
    if (this.adapters != null) {
      import _root_.scala.collection.JavaConversions._
      for (a <- this.adapters) {
        a.cleanup
      }
    }
    this.adapters = new Vector[ThumbnailAdapter]
    if (this.tables != null) {
      import _root_.scala.collection.JavaConversions._
      for (tt <- this.tables) {
        tt.cleanup
      }
    }
    this.tables = new Vector[ThumbnailTable]
    var inflater: LayoutInflater = LayoutInflater.from(this)
    import _root_.scala.collection.JavaConversions._
    for (s <- this.searches.values) {
      var tmp: View = inflater.inflate(R.layout.home_search, null)
      var tmpl: ThumbnailTable = tmp.findViewById(R.id.section_table).asInstanceOf[ThumbnailTable]
      this.tables.add(tmpl)
      var description: TextView = tmp.findViewById(R.id.section_desc).asInstanceOf[TextView]
      description.setText(s.getDescription)
      var adapter: ThumbnailAdapter = new ThumbnailAdapter(this, s, java.lang.Boolean.TRUE, R.layout.homethumbnail)
      this.adapters.add(adapter)
      var display: Display = getWindowManager.getDefaultDisplay
      var width: Int = display.getWidth
      var nb_col: Int = (width / Androbunny.dp_to_pixel(100)).asInstanceOf[Int]
      tmpl.setNumberOfColumns(nb_col)
      var tmp_nb_rows: String = Preference.getString("home_nb_rows")
      try {
        tmpl.setNumberOfRows(Integer.parseInt(tmp_nb_rows))
      }
      catch {
        case e: NumberFormatException => {
          tmpl.setNumberOfRows(1)
        }
      }
      tmpl.setAdapter(adapter)
      tmpl.setOnItemClickListener(this)
      tmpl.setOnSeeMoreListener(this)
      homeLinearLayout.addView(tmp)
    }
  }

  def setupSearches: Unit = {
    try {
      if (Preference.getBoolean("show_popular")) {
        if (!this.searches.containsKey(HomeActivity.SEARCH_POPULAR)) {
          this.searches.put(HomeActivity.SEARCH_POPULAR, API.getPopular)
        }
      }
      else {
        var tmpS: Search = this.searches.get(HomeActivity.SEARCH_POPULAR)
        if (tmpS != null) {
          tmpS.cleanup
          this.searches.remove(HomeActivity.SEARCH_POPULAR)
        }
      }
      if (Preference.getBoolean("show_recent")) {
        if (!this.searches.containsKey(HomeActivity.SEARCH_RECENT)) {
          this.searches.put(HomeActivity.SEARCH_RECENT, API.getLatest)
        }
      }
      else {
        var tmpS: Search = this.searches.get(HomeActivity.SEARCH_RECENT)
        if (tmpS != null) {
          tmpS.cleanup
          this.searches.remove(HomeActivity.SEARCH_RECENT)
        }
      }
      if (Preference.getBoolean("show_writings")) {
        if (!this.searches.containsKey(HomeActivity.SEARCH_WRITINGS)) {
          this.searches.put(HomeActivity.SEARCH_WRITINGS, API.getLatestWritings)
        }
      }
      else {
        var tmpS: Search = this.searches.get(HomeActivity.SEARCH_WRITINGS)
        if (tmpS != null) {
          tmpS.cleanup
          this.searches.remove(HomeActivity.SEARCH_WRITINGS)
        }
      }
      if (Preference.getBoolean("show_musics")) {
        if (!this.searches.containsKey(HomeActivity.SEARCH_MUSICS)) {
          this.searches.put(HomeActivity.SEARCH_MUSICS, API.getLatestMusics)
        }
      }
      else {
        var tmpS: Search = this.searches.get(HomeActivity.SEARCH_MUSICS)
        if (tmpS != null) {
          tmpS.cleanup
          this.searches.remove(HomeActivity.SEARCH_MUSICS)
        }
      }
      if (Preference.getBoolean("show_new") && (!(Preference.getString("username") == "guest"))) {
        if (!this.searches.containsKey(HomeActivity.SEARCH_NEW)) {
          this.searches.put(HomeActivity.SEARCH_NEW, API.getNew)
        }
      }
      else {
        var tmpS: Search = this.searches.get(HomeActivity.SEARCH_NEW)
        if (tmpS != null) {
          tmpS.cleanup
          this.searches.remove(HomeActivity.SEARCH_NEW)
        }
      }
    }
    catch {
      case e: Exception => {
        Log.e(HomeActivity.TAG, e.toString)
      }
    }
  }

  def onItemClick(adapter: BaseAdapter, view: View, position: Int, id: Long): Unit = {
    val submission_id = adapter.getItem(position).asInstanceOf[Thumbnailable].getID
    Androbunny ! DoShowSubmission(this, submission_id)
  }

  def onSeeMore(search: Search): Unit = {
    this.dontFree = search
    Androbunny ! DoShowListPage(this, search)
  }

  final val searches: TreeMap[Integer, Search] = new TreeMap[Integer, Search]
  private var homeLinearLayout: LinearLayout = null
  private var tables: Vector[ThumbnailTable] = null
  private var adapters: Vector[ThumbnailAdapter] = null
  private var dontFree: Search = null
}

