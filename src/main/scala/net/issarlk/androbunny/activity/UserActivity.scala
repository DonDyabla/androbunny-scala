/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import org.acra.ErrorReporter
import net.issarlk.androbunny.TR
import net.issarlk.androbunny.TypedResource._
import net.issarlk.androbunny.ThumbnailTable.OnItemClickListener
import net.issarlk.androbunny.ThumbnailTable.OnSeeMoreListener
import net.issarlk.androbunny.inkbunny.Icon
import net.issarlk.androbunny.inkbunny.InkbunnyAPIException
import net.issarlk.androbunny.inkbunny.Search
import net.issarlk.androbunny.inkbunny.Thumbnailable
import net.issarlk.androbunny.inkbunny.User
import net.issarlk.androbunny.utils.ABToast
import android.os.Bundle
import android.text.Html
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import net.issarlk.androbunny._
import android.content.{Context, Intent}
import scala.collection.mutable.{MutableList, HashMap}


final object UserActivity {
  def start(context: Context): Unit = {
    var intent: Intent = new Intent(context, classOf[UserActivity])
    context.startActivity(intent)
  }

  private final val TAG: String = "UserActivity"
}

final class UserActivity extends AndrobunnyActivity with OnClickListener with OnItemClickListener with OnSeeMoreListener {
  private var model: UserActivityModel = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    Log.d(UserActivity.TAG, "onCreate")
    Androbunny.get_user_activity_model match {
      case None =>
        ABToast.makeText("No user provided!", Toast.LENGTH_LONG)
        this.finish
        return
      case Some(m) => model = m
    }
    ErrorReporter.getInstance.putCustomData("user", "" + model.user.id)
  }

  override def onStart: Unit = {
    super.onStart
    setupUI
    setupContent
    //Get the model's new searches
    model.onNewSearch = (search: Search) => {
      Log.d(UserActivity.TAG, "new search:" + search)
      addTable(search)
    }
    //Fill with already available searches
    setupTables
  }

  override def onStop: Unit = {
    super.onStop
    this.userSearches.removeAllViews
    for (t <- this.tables) {
      t.cleanup
    }
    this.tables.clear
  }

  private def setupUI: Unit = {
    setContentView(R.layout.user)
    gallery_button = this.findView(TR.gallery_button)
    gallery_button.setTag(UserActivitySearchTypeGallery)
    gallery_button.setOnClickListener(this)
    scraps_button = this.findView(TR.scraps_button)
    scraps_button.setTag(UserActivitySearchTypeScraps)
    scraps_button.setOnClickListener(this)
    favorites_button = this.findView(TR.favorites_button)
    favorites_button.setTag(UserActivitySearchTypeFavorites)
    favorites_button.setOnClickListener(this)
    userIcon = this.findView(TR.author_icon)
    userProfileView = this.findView(TR.user_profile)
    userSearches = this.findView(TR.user_searches_linearlayout)
  }

  private def setupContent: Unit = {
    userProfileView.setText(Html.fromHtml(model.profile, ABImageGetter, null))
    userProfileView.setVisibility(View.VISIBLE)
    setupAuthorIcon
  }

  private def setupAuthorIcon: Unit = {

    val ic: Icon = model.user.getLargestIcon
    if (ic != null) {
      this.userIcon.setImageResource(R.drawable.noicon)
      Androbunny.runInBackground(
        task = {
          Androbunny.createDownloadIntoImageViewTask(
            this.userIcon,
            ic.url,
            Androbunny.dp_to_pixel(90),
            Androbunny.dp_to_pixel(90),
            true,
            true,
            UnitBitmapFilter).run
        }
      )
    } else {
      this.userIcon.setImageResource(R.drawable.noicon)
    }
  }

  private def setupTables: Unit = {
    this.tables.clear
    for (s <- model.searches.values) {
      if (s.nb_of_submissions > 0) {
        Log.d(UserActivity.TAG, "Add search in table: " + s.description)
        this.addTable(s)
      }
    }
  }

  private def addTable(search: Search): Unit = {
    Log.d(UserActivity.TAG, "Add table for:" + search)
    val inflater: LayoutInflater = LayoutInflater.from(this)
    val tmp: View = inflater.inflate(R.layout.home_search, null)
    var tmpl = tmp.findView(TR.section_table)
    tables += tmpl
    val description = tmp.findView(TR.section_desc)
    description.setText(search.getDescription)
    val adapter: ThumbnailAdapter = new ThumbnailAdapter(this, search, java.lang.Boolean.TRUE, R.layout.homethumbnail)
    // Compute the number of columns displayable
    val display: Display = getWindowManager.getDefaultDisplay
    val width: Int = display.getWidth
    val nb_col: Int = (width / Androbunny.dp_to_pixel(100))
    tmpl.setNumberOfColumns(nb_col)
    var tmp_nb_rows: String = Preference.getString("home_nb_rows")
    try {
      tmpl.setNumberOfRows(Integer.parseInt(tmp_nb_rows))
    }
    catch {
      case e: NumberFormatException => {
        tmpl.setNumberOfRows(1)
      }
    }
    tmpl.setAdapter(adapter)
    tmpl.setOnItemClickListener(this)
    tmpl.setOnSeeMoreListener(this)
    val Scraps_r= ".* scraps".r
    val Gallery_r  = ".* gallery".r
    Log.d("Child", "Description: '" + search.description + "'")
    val tag = (search.description match {
      case Scraps_r()  => "_1"
      case Gallery_r() => "_0"
      case s => "_2" + s
    })
    Log.d("Child", "Set tag " + tag + " to " + tmp)
    tmp.setTag(tag)
    runOnUiThread(() => {
      // Insert new table alphabetically
      val n = UserActivity.this.userSearches.getChildCount
      Log.d("Child count", n.toString)
      for (i <- 0 until n) UserActivity.this.userSearches.getChildAt(i) match {
        case null =>
        case (v: View) =>
          Log.d("Child", i.toString + " " + v)
          val t = v.getTag()
          if (t != null) {
            val s = t.asInstanceOf[String]
            Log.d("Compare", "Compare " + s + " to " + tag + " " + s.compareTo(tag))
            if (s.compareTo(tag) > 0) {
              UserActivity.this.userSearches.addView(tmp, i)
              return
            }
          } else {
            Log.d("Child", "Child " + v + " has a null tag")
          }
      }
      UserActivity.this.userSearches.addView(tmp)
    })
  }

  def onClick(arg0: View): Unit = {
    var searchType : UserActivitySearchType = arg0.getTag.asInstanceOf[UserActivitySearchType]
    model.searches.get(searchType) match {
      case Some(s: Search) =>
        Androbunny ! DoShowListPage(this, s)
      case None =>
    }
  }

  def onItemClick(adapter: BaseAdapter, view: View, position: Int, id: Long): Unit = {
    val submission_id = adapter.getItem(position).asInstanceOf[Thumbnailable].getID
    Androbunny ! DoShowSubmission(this, submission_id)
  }

  def onSeeMore(search: Search): Unit = {
    Androbunny ! DoShowListPage(this, search)
  }


  private var userSearches: LinearLayout = null
  private val tables: MutableList[ThumbnailTable] = MutableList[ThumbnailTable]()
  private var userProfileView: TextView = null
  private var userIcon: ImageView = null
  private var gallery_button: Button = null
  private var scraps_button: Button = null
  private var favorites_button: Button = null
}

sealed trait UserActivitySearchType {
  def name: String
}
case object UserActivitySearchTypeGallery extends UserActivitySearchType { val name = "Gallery"}
case object UserActivitySearchTypeScraps extends UserActivitySearchType { val name = "Scps"}
case object UserActivitySearchTypeFavorites extends UserActivitySearchType { val name = "Favorites"}

final class UserActivityModel(val user: User) {
  var profile: String = null
  val searches: HashMap[UserActivitySearchType, Search] = HashMap[UserActivitySearchType, Search]()
  var onNewSearch: (Search) => Unit = { _ => }

  private final class Searcher(private val search: Search, private val onLoad: (Search) => Unit) extends Runnable {
   
    def run: Unit = {
      var nb: Int = search.getNbOfItems
      if (nb > 0) {
        Log.d("Searcher.run", "Found: " + nb + " items.")
        onLoad(search)
      }
    }
  }
  
  def load: Unit = {
    try {
      profile = user.getProfile
    }
    catch {
      case e: InkbunnyAPIException => {
        profile = "Couldn't read user profile. Error: " + e.toString
      }
    }
    def addSearch(searchType: UserActivitySearchType, search: Search): Unit = {
      Log.d("Searcher", "new search loaded")
      searches.put(searchType, search)
      onNewSearch(search)
    }
    val curriedAddSearch = Function.curried(addSearch _)
    
    AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(
      new Searcher(
        this.user.getGallery,
        curriedAddSearch(UserActivitySearchTypeGallery)
      )
    )
    AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(
      new Searcher(
        this.user.getScraps,
        curriedAddSearch(UserActivitySearchTypeScraps)
      )
    )
    AndrobunnyAppSingleton.androbunnyapp.urgentWorker.execute(
      new Searcher(
        this.user.getFavorites,
        curriedAddSearch(UserActivitySearchTypeFavorites)
      )
    )
  }
}