/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny.activity

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import net.issarlk.androbunny.{TR, R}
import net.issarlk.androbunny.TypedResource._

final object AboutActivity {
  def start(context: Activity): Unit = {
    var i: Intent = new Intent(context, classOf[AboutActivity])
    context.startActivity(i)
  }

  private[activity] final val TAG: String = "AboutActivity"
  private[activity] final val DIALOG_GPL: Int = 1
  private[activity] final val DIALOG_APACHE: Int = 2
}

final class AboutActivity extends AndrobunnyActivity {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.about)
    val text = this.findView(TR.about_text)
    text.setText(readRaw(R.raw.about))
    val gplButton = this.findView(TR.gpl_button)
    val apacheButton = this.findView(TR.apache_button)
    gplButton.onClick { view: View =>
      AboutActivity.this.showDialog(AboutActivity.DIALOG_GPL)
    }
    apacheButton.onClick { view: View =>
      AboutActivity.this.showDialog(AboutActivity.DIALOG_APACHE)
    }
  }

  protected override def onCreateDialog(id: Int): Dialog = {
    val builder: AlertDialog.Builder = new AlertDialog.Builder(this)
    builder.setCancelable(true)
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener {
      def onClick(dialog: DialogInterface, id: Int): Unit = {
        dialog.cancel
      }
    })
    builder.setInverseBackgroundForced(true)
    builder.setMessage(
      readRaw(
        id match {
          case AboutActivity.DIALOG_GPL =>
            R.raw.gpl
          case AboutActivity.DIALOG_APACHE =>
            R.raw.apache2
        }
      ).replaceAll("\n[ ]*", "\n")
    )
    return builder.create()
  }

  private def readRaw(raw: Int): String = {
    var inputStream: InputStream = getResources.openRawResource(raw)
    var byteArrayOutputStream: ByteArrayOutputStream = new ByteArrayOutputStream
    var i: Int = 0
    try {
      i = inputStream.read
      while (i != -1) {
        byteArrayOutputStream.write(i)
        i = inputStream.read
      }
      inputStream.close
    }
    catch {
      case e: IOException => {
        Log.e(AboutActivity.TAG, e.toString)
      }
    }
    return byteArrayOutputStream.toString
  }
}

