/*
Copyright 2011 Gilbert Roulot.

This file is part of Androbunny.

Androbunny is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Androbunny is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Androbunny.  If not, see <http://www.gnu.org/licenses/>.
	
*/
package net.issarlk.androbunny;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import net.issarlk.androbunny.R;
import net.issarlk.androbunny.Preference;
import net.issarlk.androbunny.inkbunny.Search;
import net.issarlk.androbunny.inkbunny.Submission;
import net.issarlk.androbunny.inkbunny.Thumbnail;
import net.issarlk.androbunny.inkbunny.Thumbnailable;
import net.issarlk.androbunny.inkbunny.ThumbnailableRequester;
import net.issarlk.androbunny.utils.Utils;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public final class ThumbnailAdapter extends BaseAdapter implements ThumbnailableRequester, Observer, Serializable, Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1554750365303844377L;
	private final static String TAG = "ThumbnailAdapter";
    /** The parent context */
    //Search
    public final Search search;
    //References to created views
    private Object[] views;
    //Whether to cache pictures on storage or not
    private Boolean diskCache;
    //Thumbnail layout to inflate
    private int thumbnailLayout;
    //Lightweight thumbnails
    private final ConcurrentLinkedQueue<Runnable> toRuns = new ConcurrentLinkedQueue<Runnable>(); 
    
    public ThumbnailAdapter(Context c, Search arg_search, Boolean argdiskcache) {
    	this(c, arg_search, argdiskcache, R.layout.thumbnail);
    }
        
	public ThumbnailAdapter(Context c, Search arg_search, Boolean argdiskcache, int argThumbnailLayout) {
    	this.search = arg_search;
    	this.diskCache = argdiskcache;  
    	this.thumbnailLayout = argThumbnailLayout;
    	this.views = new Object[10];
    	this.search.addObserver(this);
    }
	
	public void cleanup() {
		this.search.deleteObserver(this);
	}
    
	public void update(Observable arg0, Object arg1) {
		//The thumbnail list changed significantly
		//Cancel all pending image downloads
		this.cancelToRuns();
		//notify the view so all is refreshed
		Log.d(TAG, "List updated");
		AndrobunnyAppSingleton.androbunnyapp.handler.post(new Runnable() {
			public void run() {
				ThumbnailAdapter.this.notifyDataSetChanged();
			}
		});
	}
	
    /** Returns the amount of images we have defined. */
    public int getCount() {
    	if (this.search != null && this.search.isLoaded()) {
    		return this.search.getNbOfItems();
    	} else {
    		return 0;
    	}
    }

    public Object getItem(int position) {
    	Object[] results = this.search.results;
    	if (position < results.length) {
    		//Already have it
    		Object tmp = results[position];
    		if (tmp instanceof Submission) {
    			//Change it into a lightweight submission
    			results[position] = new LightweightThumbnailable(
    					(Submission) tmp, 
    					Androbunny.it().dp_to_pixel(90),
    					Androbunny.it().dp_to_pixel(90));
    		}
    		return results[position];
    	} else {
    		//Ask for it and wait...
    		GetItem tmp = new GetItem(position);
    		return tmp.getResult();
    	}
    }
    
    private class GetItem implements ThumbnailableRequester {
    	private int position;
    	private volatile boolean done = false;
    	private volatile Thumbnailable result;
    	private final ReentrantLock lock = new ReentrantLock();
    	private final Condition received = lock.newCondition();
    	
    	public GetItem(int argPosition) {
    		this.position = argPosition;
    	}
    	
    	public Thumbnailable getResult() {
    		//Request from source
    		ThumbnailAdapter.this.search.request(this, this.position);
    		//Wait for result
    		this.lock.lock();
    		try {
    			while(this.done == false) {
    				this.received.await();
    			}
    			return this.result;
    		} catch (InterruptedException e) {
				Log.e(TAG, "GetItem interrupted");
				return null;
			} finally {
    			this.lock.unlock();
    		}
    	}
    		
		public void receive(int position, Thumbnailable argThumbnailable) {
			this.lock.lock();
			try {
				if (argThumbnailable instanceof Submission) {
					//Change to lightweights
					Object[] results = ThumbnailAdapter.this.search.results;
					argThumbnailable = new LightweightThumbnailable(
							argThumbnailable, 
							Androbunny.it().dp_to_pixel(90),
							Androbunny.it().dp_to_pixel(90));
					results[position] = argThumbnailable;
				}
				this.result = argThumbnailable;
				this.done = true;
				this.received.signalAll();
			} finally {
				this.lock.unlock();
			}
		}
    }
    
    public long getItemId(int position) { 
    	return position; 
    }
    
	@SuppressWarnings("unchecked")
	public void receive(int position, Thumbnailable argThumbnailable) {		
		if (argThumbnailable instanceof Submission) {
			//Change to lightweight thumbnail
			argThumbnailable = new LightweightThumbnailable(argThumbnailable, Androbunny.it().dp_to_pixel(90), Androbunny.it().dp_to_pixel(90));
			Object[] results = ThumbnailAdapter.this.search.results;
			results[position] = argThumbnailable;
		}
			
		//Only update individual view
		Log.d(TAG, "Entering receive " + position);
		WeakReference<View> tmpWeakView = null;
		if (position < this.views.length) {
			tmpWeakView = (WeakReference<View>)this.views[position];			
		} else {
			return;
		}		
		View tmpView = tmpWeakView.get();
		if (tmpView != null) {
			ViewHolder holder = (ViewHolder) tmpView.getTag();
			if (holder.position == position) {
				//Log.d(TAG, "Position is good: " + position);
				if (Thread.currentThread() == AndrobunnyAppSingleton.androbunnyapp.uiThread) {
					//Log.d(TAG, "filling view");
					this.fillView(tmpView, position, argThumbnailable);					
				} else {
					//Log.d(TAG, "Posting a ViewFiller");
					AndrobunnyAppSingleton.androbunnyapp.handler.post(new ViewFiller(tmpView, position, argThumbnailable));
				}
			} else {
				Log.d(TAG, "Received wrong position for a view: " + position + " instead of " + holder.position);
			}
		} else {
			//Log.d(TAG, "tmpView null");
		}
	}
	
	private final class ViewFiller implements Runnable {
		private View view;
		private int position;
		private Thumbnailable thumbnailable;
		
		public ViewFiller(View v, int p, Thumbnailable t) {
			this.view = v;
			this.position = p;
			this.thumbnailable = t;
		}
		
		public void run() {
			ThumbnailAdapter.this.fillView(this.view, this.position, this.thumbnailable);
		}		
	}
    
	private void fillView(View v, int position, Thumbnailable thumbnailable) {
		ViewHolder holder;
        holder = (ViewHolder) v.getTag();
        if (thumbnailable.getID() != holder.id) {     
        	//Show generic submission picture
 	   		holder.image.setImageResource(R.drawable.nofile);
        	//Log.d(TAG, "Updating view with new content");
       		//The view doesn't already displays the correct thumbnail
       		//update it	
            //Fill author
            try {
            	holder.text.setText(AndrobunnyAppSingleton.androbunnyapp.getResources().getText(R.string.by_author) + " " + thumbnailable.getAuthor());
            } catch (Exception e) {
       			Log.e(TAG, e.toString());
       			holder.text.setText("error");
       		}    		
            //Fill icons if they are present
       		if (holder.iconRating != null) {
       			holder.iconRating.setImageResource(AndrobunnyAppSingleton.androbunnyapp.getResourceForRating(thumbnailable.getRating()));
       		}
       		if (holder.iconType != null) {
       			holder.iconType.setImageResource(AndrobunnyAppSingleton.androbunnyapp.getIconResourceForSubmissionType(thumbnailable.getSubmissionType()));
      		}
       		if (holder.iconDigital != null) {
       			holder.iconDigital.setImageResource(AndrobunnyAppSingleton.androbunnyapp.getIconResourceForDigitalSales(thumbnailable.getDigitalSales()));
       		}
       		if (holder.iconPrint != null) {
       			holder.iconPrint.setImageResource(AndrobunnyAppSingleton.androbunnyapp.getIconResourceForPrintSales(thumbnailable.getPrintSales()));
       		}
       		//display thumbnail
            int w = Androbunny.it().dp_to_pixel(90);
            //Get a thumbnail over the image size (90dp hardcoded)
            Thumbnail thumbnail = thumbnailable.getLargerThumbnail(Preference.it().useCustomThumbnails()?Thumbnailable.CUSTOM:Thumbnailable.ORIGINAL, w, w);
            Bitmap tmp;
            if (thumbnail == null) {
        		//No thumbnail
            	holder.image.setImageResource(AndrobunnyAppSingleton.androbunnyapp.getDefaultResourceForSubmissionType(thumbnailable.getSubmissionType()));
            } else {
         	   	tmp = AndrobunnyAppSingleton.androbunnyapp.bitmapCache.get(thumbnail.url, Androbunny.it().dp_to_pixel(90), Androbunny.it().dp_to_pixel(90));
         	   	if (tmp != null) {
         	   		//Bitmap was found in the cache. Show it
         	   		holder.image.setImageBitmap(Rating.getBitmapFilterFor(thumbnailable).filter(tmp));
         	   		//Log.d(TAG, "found in cache");
         	   		//holder.image.setImageResource(R.drawable.audio);
         	   	} else {
         	   		//Log.d(TAG, "not found in cache");
         	   		//Load from web or file cache      
         	   		this.toRuns.add(Androbunny.it().createDownloadIntoImageViewTask(holder.image, holder.id, thumbnail.url, Androbunny.it().dp_to_pixel(90), Androbunny.it().dp_to_pixel(90), this.diskCache, true, Rating.getBitmapFilterFor(thumbnailable)));
         	   		AndrobunnyAppSingleton.androbunnyapp.worker.execute(this);
         	   	}
         	   	/* Image should be scaled as width/height are set. */
         	   	holder.image.setScaleType(ImageView.ScaleType.FIT_CENTER);
         	   	holder.id = thumbnailable.getID();
            }
        } else {
        	//Log.d(TAG, "View already contains correct content");
        }
	}

    /** Returns a new ImageView to
     * be displayed, depending on
     * the position passed. */
    public View getView(int position, View convertView, ViewGroup parent) {
    	//Log.d(TAG, "getView(" + position + " ...");
    	ViewHolder holder;
    	View v;
    	if (convertView != null ) {
    		v = convertView;
    		holder = (ViewHolder)convertView.getTag();
    		//Remove this view from it's old position, unless it's for the same position
    		if (
    				holder.position != -1 &&
    				holder.position != position && 
    				holder.position < this.views.length &&
    				this.views[holder.position] == convertView) {
    			this.views[holder.position] = null;
    			//Stop asking for it
        		this.search.cancelRequest(this, holder.position);
        		//Clear position from holder
        		holder.position = -1;
    		}
    	} else {
    		//Build a new view
    		v = AndrobunnyAppSingleton.androbunnyapp.layoutInflater.inflate(this.thumbnailLayout, null);
    		// Creates a ViewHolder and store references to the children views
            // we will need now and later.
    		holder = new ViewHolder();
    		holder.text = (TextView) v.findViewById(R.id.by_author);
            holder.image = (ImageView) v.findViewById(R.id.thumbnail_image);
            //Store references to icons if they are present in the layout
    		holder.iconRating = (ImageView)v.findViewById(R.id.thumbnail_icon_rating);
    		holder.iconType = (ImageView)v.findViewById(R.id.thumbnail_icon_type);
    		holder.iconDigital = (ImageView)v.findViewById(R.id.thumbnail_icon_digital);
    		holder.iconPrint = (ImageView)v.findViewById(R.id.thumbnail_icon_print);   	
    		holder.position = -1;
    		v.setTag(holder);
    	}    		
    	Object[] results = this.search.results;
    	if (position < results.length) {
    		//We have the thumbnailable available, fill view   
    		//Log.d(TAG, "Pos " + position + " is in results (size " + results.length + ")");
    		this.fillView(v, position, (Thumbnailable) results[position]);
    	} else {
    		//Empty view content for now
    		holder.text.setText("");
    		holder.image.setImageResource(R.drawable.nofile);
    		if (holder.iconRating != null) {
    			holder.iconRating.setImageDrawable(null);
    		}
    		if (holder.iconType != null) {
    			holder.iconType.setImageDrawable(null);
    		}
    		if (holder.iconDigital != null) {
    			holder.iconDigital.setImageDrawable(null);
    		}
    		if (holder.iconPrint != null) {
    			holder.iconPrint.setImageDrawable(null);
    		}
    		if (position >= this.views.length) {
    			this.views = Utils.growArray(this.views, (int)((float)position * 1.2));
    		}
    		views[position] = new WeakReference<View>(v);
    		//Request thumbnailable
    		holder.position = position;
    		this.search.request(this, position);
    	}
        return v;
    }
    
	public static class ViewHolder {
        public TextView text;
        public ImageView image;
        public ImageView iconRating;
        public ImageView iconType;
        public ImageView iconDigital;
        public ImageView iconPrint;
        public int id;
        public int position;        
    }
	
	/*
	 * Runnable interface
	 */
	
	private void cancelToRuns() {
		this.toRuns.clear();
	}
	
	public void run() {
		//Run one of the toRun runnables
		Runnable toRun = this.toRuns.poll();
		if (toRun != null) {
			//Run it
			toRun.run();
			if (this.toRuns.peek() != null) {
				//Request another run
				AndrobunnyAppSingleton.androbunnyapp.worker.execute(this);
			}
		}
	}
}	
