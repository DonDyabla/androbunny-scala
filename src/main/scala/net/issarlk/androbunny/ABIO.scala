package net.issarlk.androbunny

import org.apache.http.client.methods.HttpGet
import java.net.URI
import org.apache.http.util.EntityUtils
import android.graphics.{BitmapFactory, Bitmap}
import org.apache.http.{HttpResponse, HttpEntity}
import org.apache.http.client.{ClientProtocolException, HttpClient, ResponseHandler}
import java.io._
import android.graphics.Bitmap.CompressFormat
import utils.{ABToast, AbHttpClient}
import java.security.{NoSuchAlgorithmException, MessageDigest}
import android.widget.{ImageView, Toast}
import ref.WeakReference

/**
 * Created by IntelliJ IDEA.
 * User: issarlk
 * Date: 12/24/11
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */

object ABIO {
  private val TAG: String = "ABIO"
  final var httpClient: HttpClient = null
  def setHTTPClient(arg: HttpClient) = httpClient = arg

  def it = this

  @throws(classOf[ClientProtocolException])
  @throws(classOf[IOException])
  def readUri(uri: URI): String = {
    Log.d(TAG, uri.toString)
    var request: HttpGet = new HttpGet(uri)
    var result: String = this.httpClient.execute(request, new StringResponseHandler)
    Log.d(TAG, "Got " + result.length + " from " + uri.toString)
    return result
  }

  def readUriIntoFile(uri: URI, target: File): Unit = {
    var request: HttpGet = new HttpGet(uri)
    this.httpClient.execute(request, new FileResponseHandler(target))
  }

  def readUriIntoStream(uri: URI, stream: OutputStream): Unit = {
    var request: HttpGet = new HttpGet(uri)
    this.httpClient.execute(request, new StreamResponseHandler(stream))
  }

  def readUriIntoBitmap(uri: URI, w: Int, h: Int): Bitmap = {
    var request: HttpGet = new HttpGet(uri)
    return this.httpClient.execute(request, new BitmapResponseHandler(w, h))
  }

  private final class StringResponseHandler extends ResponseHandler[String] {
    def handleResponse(response: HttpResponse): String = {
      var entity: HttpEntity = response.getEntity
      if (entity != null) {
        return EntityUtils.toString(entity)
      }
      else {
        return ""
      }
    }
  }

  private final class BitmapResponseHandler(val max_width: Int,  val max_height: Int)
    extends ResponseHandler[Bitmap] {

    def handleResponse(response: HttpResponse): Bitmap = {
      var entity: HttpEntity = response.getEntity
      if (entity != null) {
        var is: BufferedInputStream = new BufferedInputStream(entity.getContent)
        var bm: Bitmap = null
        try {
          var options: BitmapFactory.Options = new BitmapFactory.Options
          options.inJustDecodeBounds = true
          options.inInputShareable = false
          options.inPurgeable = true
          options.inPreferredConfig = Bitmap.Config.RGB_565
          is.mark(64000)
          BitmapFactory.decodeStream(is, null, options)
          is.reset
          if (options.outWidth == -1) {
            bm = BitmapFactory.decodeStream(is)
          }
          else {
            var cw: Int = options.outWidth
            var ch: Int = options.outHeight
            var inSampleSize: Int = 1
            while (cw / 2 >= this.max_width && ch / 2 >= this.max_height) {
              cw /= 2
              ch /= 2
              inSampleSize *= 2
            }
            Log.d(TAG, "inSampleSize " + inSampleSize + " to get " + this.max_width + "," + this.max_height + " out of " + options.outWidth + "," + options.outHeight)
            var resample: BitmapFactory.Options = new BitmapFactory.Options
            resample.inSampleSize = inSampleSize
            bm = BitmapFactory.decodeStream(is, null, resample)
          }
          if (bm == null) {
            Log.e(TAG, "Cant decode bitmap")
            return null
          }
          else {
            var realw: Int = bm.getWidth
            var realh: Int = bm.getHeight
            var whratio: Float = realw.asInstanceOf[Float] / realh.asInstanceOf[Float]
            if (this.max_width != 0 && (realw > this.max_width)) {
              realw = this.max_width
              realh = (realw.asInstanceOf[Float] / whratio).asInstanceOf[Int]
            }
            if (this.max_height != 0 && (realh > this.max_height)) {
              realh = this.max_height
              realw = (realh.asInstanceOf[Float] * whratio).asInstanceOf[Int]
            }
            val resized: Bitmap = Bitmap.createScaledBitmap(bm, realw, realh, true)
            bm.recycle
            return resized
          }
        }
        finally {
          is.close
        }
      }
      else {
        return null
      }
    }
  }

  private final class FileResponseHandler(val file: File) extends ResponseHandler[File] {

    def handleResponse(response: HttpResponse): File = {
      var entity: HttpEntity = response.getEntity
      if (entity != null) {
        var os: OutputStream = new FileOutputStream(this.file)
        entity.writeTo(os)
        os.close
        return this.file
      }
      else {
        return null
      }
    }
  }

  private final class StreamResponseHandler(val stream: OutputStream) extends ResponseHandler[OutputStream] {

    def handleResponse(response: HttpResponse): OutputStream = {
      var entity: HttpEntity = response.getEntity
      if (entity != null) {
        entity.writeTo(this.stream)
        return this.stream
      }
      else {
        return null
      }
    }
  }

  def downloadBitmap(url: URI, argw: Int, argh: Int, docache: Boolean, cacheScaled: Boolean): Bitmap = {
    var bm: Bitmap = AndrobunnyAppSingleton.androbunnyapp.bitmapCache.get(url, argw, argh)
    if (cacheScaled == true && bm != null) {
      return bm
    }
    var bytesOfMessage: Array[Byte] = url.toString.getBytes
    var md: MessageDigest = null
    try {
      md = MessageDigest.getInstance("MD5")
      var thedigest: Array[Byte] = md.digest(bytesOfMessage)
      var hexString: StringBuffer = new StringBuffer
      var i: Int = 0
      while (i < thedigest.length) {
        {
          hexString.append(Integer.toHexString(0xFF & thedigest(i)))
        }
        ({
          i += 1; i
        })
      }
      var cachedir: File = AndrobunnyAppSingleton.androbunnyapp.getCacheDir
      var cachefile: File = null
      if (cacheScaled) {
        cachefile = new File(cachedir, hexString.toString + "_" + argw + "_" + argh)
      }
      else {
        cachefile = new File(cachedir, hexString.toString)
      }
      if (cachefile.exists) {
        Log.d(TAG, "Read from disk cache: " + url.toString)
        var fis: FileInputStream = new FileInputStream(cachefile)
        bm = BitmapFactory.decodeStream(fis)
        fis.close
        if (bm == null) {
          cachefile.delete
        }
        if (!cacheScaled) {
          var realw: Int = bm.getWidth
          var realh: Int = bm.getHeight
          var whratio: Float = realw.asInstanceOf[Float] / realh.asInstanceOf[Float]
          if (argw != 0 && (realw >= argw)) {
            realw = argw
            realh = (realw.asInstanceOf[Float] / whratio).asInstanceOf[Int]
          }
          if (argh != 0 && (realh >= argh)) {
            realh = argh
            realw = (realh.asInstanceOf[Float] * whratio).asInstanceOf[Int]
          }
          var resized: Bitmap = Bitmap.createScaledBitmap(bm, realw, realh, true)
          bm.recycle
          bm = resized
        }
      }
      else {
        Log.d(TAG, "Download: " + url.toString)
        bm = ABIO.it.readUriIntoBitmap(url, argw, argh)
        if (docache) {
          var fo: FileOutputStream = new FileOutputStream(cachefile)
          bm.compress(CompressFormat.PNG, 100, fo)
          fo.close
        }
      }
      AndrobunnyAppSingleton.androbunnyapp.bitmapCache.put(url, argw, argh, bm)
    }
    catch {
      case e: NoSuchAlgorithmException => {
        Log.e(TAG, e.toString)
      }
      case e: ClientProtocolException => {
        Log.e(TAG, e.toString)
      }
      case e: FileNotFoundException => {
        Log.e(TAG, e.toString)
      }
    }
    return bm
  }

  final class DownloadIntoImageViewTask(
      iv: ImageView,
      val id: Int,
      val uri: URI,
      val w: Int,
      val h: Int,
      val doCache: Boolean,
      val cacheScaled: Boolean,
      val bitmapFilter: BitmapFilter) extends Runnable {
    val wiv = new WeakReference[ImageView](iv)

    def cancel: Unit = {
      this.canceled = true
    }

    def setImage(argImageView: ImageView, argBitmap: Bitmap): Unit = {
      this.imageView = argImageView
      this.resultBitmap = argBitmap
      var holder: ThumbnailAdapter.ViewHolder = this.imageView.getTag.asInstanceOf[ThumbnailAdapter.ViewHolder]
      if (holder != null && holder.id != this.id) {
        return
      }
      AndrobunnyAppSingleton.androbunnyapp.handler.post(new Runnable {
        def run: Unit = {
          DownloadIntoImageViewTask.this.imageView.setImageBitmap(DownloadIntoImageViewTask.this.resultBitmap)
        }
      })
    }

    def run: Unit = {
      if (canceled) return
      wiv.get match {
        case None =>
          //do nothing
        case Some(iv) =>
          var holder: ThumbnailAdapter.ViewHolder = iv.getTag.asInstanceOf[ThumbnailAdapter.ViewHolder]
          if (holder != null && holder.id != this.id) {
            return
          }
          try {
            var bm: Bitmap = AndrobunnyAppSingleton.androbunnyapp.bitmapCache.get(this.uri, this.w, this.h)
            if (bm != null) {
              this.setImage(iv, this.bitmapFilter.filter(bm))
              return
            }
            var bytesOfMessage: Array[Byte] = uri.toString.getBytes
            var md: MessageDigest = MessageDigest.getInstance("MD5")
            var thedigest: Array[Byte] = md.digest(bytesOfMessage)
            var hexString: StringBuffer = new StringBuffer
            var i: Int = 0
            while (i < thedigest.length) {
              {
                hexString.append(Integer.toHexString(0xFF & thedigest(i)))
              }
              ({
                i += 1; i
              })
            }
            var cachedir: File = AndrobunnyAppSingleton.androbunnyapp.getCacheDir
            var cachefile: File = null
            if (this.cacheScaled) {
              cachefile = new File(cachedir, hexString.toString + "_" + this.w + "_" + this.h)
            }
            else {
              cachefile = new File(cachedir, hexString.toString)
            }
            if (cachefile.exists) {
              var fis: FileInputStream = new FileInputStream(cachefile)
              bm = BitmapFactory.decodeStream(fis)
              fis.close
              if (bm == null) {
                cachefile.delete
              }
            }
            else {
              bm = ABIO.it.readUriIntoBitmap(uri, this.w, this.h)
              if (this.doCache) {
                var fo: FileOutputStream = new FileOutputStream(cachefile)
                bm.compress(CompressFormat.PNG, 100, fo)
                fo.close
              }
            }
            AndrobunnyAppSingleton.androbunnyapp.bitmapCache.put(this.uri, this.w, this.h, bm)
            setImage(iv, this.bitmapFilter.filter(bm))
          }
          catch {
            case e: IOException => {
              ABToast.makeText("A network error occured. Please try again.", Toast.LENGTH_SHORT).show
            }
            case e: Exception => {
              Log.e(TAG, e.toString)
            }
          }
          return
        }
    }

    private var canceled: Boolean = false
    private var resultBitmap: Bitmap = null
    private var imageView: ImageView = null
  }
}